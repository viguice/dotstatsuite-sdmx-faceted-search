import debug from '../debug';
import { getCache } from '../facets';

export default async ctx => {
  const { solr, config } = ctx();
  const cachedFacets = await getCache(solr, config);
  debug.info('new cache loaded');
  return ctx({ cachedFacets });
};
