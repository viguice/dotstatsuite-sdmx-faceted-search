import express from 'express';
import helmet from 'helmet';
import debug from '../../debug';
import http from 'http';
import bodyParser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import requestConnector from './connectors/request';
import directConnector from './connectors/direct';
import errors from './middlewares/errors';
import tenant from './middlewares/tenant';

const expressPino = require('express-pino-logger')({
  logger: debug,
  serializers: {
    req: req => ({
      tenant: req.raw.tenant?.id,
      ...req,
    }),
  },
});

const getUrl = server => `http://${server.address().address}:${server.address().port}`;

const init = ctx => {
  const {
    config,
    services: { api, admin, healthcheck },
  } = ctx();
  const {
    server: { host, port },
  } = config;
  const app = express();
  const httpServer = http.createServer(app);

  const promise = new Promise(resolve => {
    app
      .use(compression())
      .use(cors())
      .use(bodyParser.json({ limit: '5000kb' }))
      .use(helmet.hidePoweredBy())
      .use(helmet.hsts())
      .use('/ping', (req, res) => res.json({ ping: 'pong' }))
      .use('/healthcheck', directConnector('healthcheck', healthcheck))
      .use(tenant(ctx))
      .use(expressPino)
      .use('/api', requestConnector(api))
      .use('/admin', requestConnector(admin))
      .use(errors);

    httpServer.listen(port, host, () => {
      httpServer.url = getUrl(httpServer);
      debug.info(`server started on ${httpServer.url}`);
      resolve(ctx({ http: httpServer }));
    });
  });

  return promise;
};

export default init;
