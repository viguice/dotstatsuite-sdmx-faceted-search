import axios from 'axios'
import express from 'express'
import http from 'http'
import { keys, prop, includes, pluck } from 'ramda'
import urljoin from 'url-join'
import { createContext } from 'jeto'
import initHttp from '../init/http'
import initServices from '../services'
import initSolr from '../solr'
import initIndexer from '../init/indexer'
import { manageSchema, tearContext, initConfig } from './utils'
import initMongo from '../init/mongo'
import initConfigManager from '../init/configManager'
import initReporter from '../init/reporter'
import initParams from '../init/loadParams'
import { encode } from '../utils'

let makeRequest
let CTX
let TENANT
const tc = () => CTX

const makeRequestFactory = (http, { apiKey }) => ({ method = 'post', url, data }) =>
  axios({ method, url: urljoin(http.url, url), data, headers: { 'x-tenant': TENANT.id, 'x-api-key': apiKey } }).then(
    prop('data'),
  )

const tenantId = `test`
const tenants = nsi => ({
  [tenantId]: {
    id: tenantId,
    spaces: {
      'qa-reset': {
        url: nsi.url,
      },
    },
    datasources: {
      DS1: {
        indexed: true,
        dataSpaceId: 'qa-reset',
        dataqueries: [
          {
            version: '1.0',
            categorySchemeId: 'CS1',
            agencyId: 'AG1',
          },
          {
            version: '1.0',
            categorySchemeId: 'CS2',
            agencyId: 'AG2',
          },
        ],
      },
      DS2: {
        indexed: true,
        dataSpaceId: 'qa-reset',
        dataqueries: [
          {
            version: '1.0',
            categorySchemeId: 'CS1',
            agencyId: 'AG1',
          },
        ],
      },
      DS3: {
        indexed: false,
        dataSpaceId: 'qa-reset',
        dataqueries: [
          {
            version: '1.0',
            categorySchemeId: 'CS1',
            agencyId: 'AG1',
          },
        ],
      },
    },
  },
})

const params = {
  defaultLocale: 'en',
  excludedCategorySchemeFacets: ['TO_BE_REJECTED'],
}

const nsiServer = jest.fn()
const getUrl = server => `http://${server.address().address}:${server.address().port}`
const getRef = url => {
  const matches = url.match(/dataflow\/([\w.]+)\/(\w+)\/([\d.]+)/)
  return { agencyId: matches[1], dataflowId: matches[2], version: matches[3] }
}

const initNSI = () => {
  const host = '0.0.0.0'
  const app = express()
  const httpServer = http.createServer(app)
  app.use((req, res) => {
    nsiServer(req.url)

    if (/categoryscheme\/AG1\/CS1/.test(req.url)) {
      return res.json(require('./categoryScheme.js')({ url: httpServer.url, agencyId: 'AG1', version: '2.3.4' }))
    }

    if (/categoryscheme\/AG2\/CS2/.test(req.url)) {
      return res.json(require('./categoryScheme.js')({ url: httpServer.url, agencyId: 'AG2', version: '2.4' }))
    }

    if (req.url === '/external/dataflow/AG1/DF2/2.3.4/?references=all&detail=referencepartial') {
      return res.json(require('./mocks/sdmx/dataflow_external.js')({ ...getRef(req.url), order: 13 }))
    }

    if (req.url === '/external/dataflow/AG2/DF2/2.4/?references=all&detail=referencepartial') {
      return res.json(
        require('./mocks/sdmx/dataflow_external.js')({ ...getRef(req.url), order: { fr: '2', en: 130.7 } }),
      )
    }

    if (/dataflow\/AG[12]\/DF3/.test(req.url))
      return res.json(require('./mocks/sdmx/dataflow_without_cc')({ ...getRef(req.url) }))
    if (req.url === '/data/AG1,DF3,1.0/all') return res.set('content-range', 'values 0-999/10000').json({})
    if (req.url === '/data/AG2,DF3,1.0/all') return res.status(404).json({})

    if (/dataflow\/AG[12]\/DF4/.test(req.url))
      return res.json(require('./mocks/sdmx/dataflow_citizens')(getRef(req.url)))

    res.json(require('./mocks/sdmx/dataflow_sample_abs')(getRef(req.url)))
  })

  return new Promise(resolve => {
    httpServer.listen(null, host, () => {
      httpServer.url = getUrl(httpServer)
      resolve(httpServer)
    })
  })
}

const initContext = () => async () => {
  const nsi = await initNSI()
  try {
    CTX = await initConfig(createContext, params, { tenants: tenants(nsi) })
      .then(ctx => {
        TENANT = ctx().TENANT
        return ctx({ nsi })
      })
      .then(initReporter)
      .then(initMongo)
      .then(initConfigManager)
      .then(initSolr)
      .then(async ctx => {
        await manageSchema(TENANT, ctx().solr)
        const solrClient = ctx().solr.getClient(TENANT)
        await solrClient.deleteAll()
        return ctx
      })
      .then(ctx => {
        return ctx
      })
      .then(initParams)
      .then(initIndexer)
      .then(initServices)
      .then(initHttp)
    makeRequest = makeRequestFactory(CTX().http, CTX().config)
  } catch (e) {
    console.error(e) // eslint-disable-line
    throw e
  }
}

beforeAll(initContext())
//afterAll(tearContext(tc))

describe('Server | services | admin', () => {
  it('should index datasources', async () => {
    const res = await makeRequest({ method: 'POST', url: '/admin/dataflows?mode=sync' })

    expect(res.DS1.dataflows.transpiled).toEqual(4)
    expect(res.DS1.dataflows.indexed).toEqual(4)
    expect(res.DS1.dataflows.rejected).toEqual(1)
    expect(res.DS1.dataflows.loaded).toEqual(7)

    //expect(nsiServer).toHaveBeenNthCalledWith(1, '/categoryscheme/AG1/CS1/1.0/?references=dataflow')
    //expect(nsiServer).toHaveBeenNthCalledWith(2, '/dataflow/AG1/DF1/2.3.4/?references=all&detail=referencepartial')
    //expect(nsiServer).toHaveBeenNthCalledWith(3, '/external/dataflow/AG1/DF2/2.3.4/?references=all&detail=referencepartial')
    //expect(nsiServer).toHaveBeenNthCalledWith(4, '/dataflow/AG1/DF2/2.3.4/?references=all&detail=referencepartial')
    //expect(nsiServer).toHaveBeenNthCalledWith(5, '/dataflow/AG1/DF3/1.0/?references=all&detail=referencepartial')
    //expect(nsiServer).toHaveBeenNthCalledWith(6, '/data/AG1,DF3,1.0/all')
    //expect(nsiServer).toHaveBeenNthCalledWith(7, '/dataflow/AG1/DF4/1.0/?references=all&detail=referencepartial')
    //expect(nsiServer).toHaveBeenNthCalledWith(8, '/categoryscheme/AG2/CS2/1.0/?references=dataflow')
    //expect(nsiServer).toHaveBeenNthCalledWith(9, '/dataflow/AG2/DF1/2.4/?references=all&detail=referencepartial')
    //expect(nsiServer).toHaveBeenNthCalledWith(10, '/external/dataflow/AG2/DF2/2.4/?references=all&detail=referencepartial')
    //expect(nsiServer).toHaveBeenNthCalledWith(11, '/dataflow/AG2/DF2/2.4/?references=all&detail=referencepartial')
    //expect(nsiServer).toHaveBeenNthCalledWith(12, '/dataflow/AG2/DF3/1.0/?references=all&detail=referencepartial')
    //expect(nsiServer).toHaveBeenNthCalledWith(13, '/data/AG2,DF3,1.0/all')

    expect(nsiServer).toHaveBeenCalledTimes(21)
  })

  it('should query', async () => {
    const search = `datasourceId:"dS1" dataflowId:"DF1"`
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.numFound).toEqual(1)
  })

  it('should query without case sensitive or exact match on dataflowId field', async () => {
    const res1 = await makeRequest({ method: 'POST', url: '/api/search', data: { search: 'DF1' } })
    expect(res1.numFound).toEqual(2)

    const res2 = await makeRequest({ method: 'POST', url: '/api/search', data: { search: 'df1' } })
    expect(res2.numFound).toEqual(2)

    //const res3 = await makeRequest({ method: 'POST', url: '/api/search', data: { search: 'df_C' } })
    //expect(res3.numFound).toEqual(2)
  })

  it('should query on non alphanumeric name values', async () => {
    const search = `"Consumer Price Index (CPI) 17th Series é-._#& تكرر"`
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.numFound).toEqual(2)
  })

  it('should query on non alphanumeric dimension names', async () => {
    const search = `"12345-Balance"`
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    // console.log(JSON.stringify(res, null, 4))
    expect(res.numFound).toEqual(4)
  })

  it('should highlight only dataflowId', async () => {
    const search = 'datasourceId:"dS1" dataflowId:"DF2"'
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.highlighting['DS1:DF2'].dataflowId).toEqual(['<mark>DF2</mark>'])
    expect(keys(res.highlighting['DS1:DF2'])).toEqual(['dataflowId'])
  })

  it('should get right versions', async () => {
    const search = `datasourceId:"DS1" dataflowId:"DF2"`
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.dataflows[0].version).toEqual('2.4')
  })

  it('should search on facet (category) and match leaf', async () => {
    const facetName = encode('ABS 123(Top)ics وحدة')
    let search = `datasourceId:"DS1" dataflowId:"DF1" ${facetName}:ECONOMY`
    let res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.numFound).toEqual(1)

    search = `datasourceId:"DS1" dataflowId:"DF1" ECONOMY`
    res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.numFound).toEqual(1)
    expect(res.highlighting['DS1:DF1']['ABS 123(Top)ics وحدة']).toBeDefined()
  })

  it('should search on facet (category) and match intermediate categorie(s)', async () => {
    const facetName = encode('ABS 123(Top)ics وحدة')
    let search = `datasourceId:"DS1" dataflowId:"DF1" ${facetName}:"Price Indexes and Inflation"`
    let res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.numFound).toEqual(1)

    search = `datasourceId:"DS1" dataflowId:"DF1" "Price Indexes and Inflation"`
    res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.numFound).toEqual(1)
  })

  it('should search on facet (codelist)', async () => {
    const search = `datasourceId:"DS1" dataflowId:"DF1" DarWin`
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.numFound).toEqual(1)
  })

  it('should filter on facet', async () => {
    const search = ``
    const facets = {
      datasourceId: ['DS1'],
      Topic: ['0|Government#GOV#'],
    }
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search, facets } })
    expect(res.numFound).toEqual(1)
  })

  it('should filter on facet but get empty result', async () => {
    const search = ``
    const facets = {
      datasourceId: ['DS1'],
      Topic: ['X|Government#GOV#'],
    }
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search, facets } })
    expect(res.numFound).toEqual(0)
  })

  it('should search on excluded category scheme facet', async () => {
    const search = 'Rejected:ECONOMY'
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.numFound).toEqual(0)
  })

  it('should get externalUrl', async () => {
    const search = 'dataflowId:DF2'
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.numFound).toEqual(2)
    expect(res.dataflows[0].externalUrl).toEqual(
      `${CTX().nsi.url}/external/dataflow/AG2/DF2/2.4/?references=all&detail=referencepartial`,
    )
  })

  it('should handle partial tree (data availability)', async () => {
    const search = 'datasourceId:"dS1" dataflowId:"DF4"'
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    const values = pluck('val', res.facets['Field'].buckets)
    expect(includes('0|Access#ACC#', values)).toBeFalsy()
    expect(includes('1|Access#ACC#|Affordability#AFF#', values)).toBeTruthy()
  })

  it('should get config', async () => {
    const res = await makeRequest({ method: 'GET', url: '/admin/config' })
    // console.log('=========', JSON.stringify(res, null, 4))
    expect(res.fields['lorder'].type).toEqual('ATTR')
    expect(res.dataflows.schema.find(x => x.name === 'lorder')).toBeDefined()
  })

  it('should sort on sname_en_s asc, lastUpdated_s desc', async () => {
    const sort = 'sname asc, lastUpdated desc'
    const res1 = await makeRequest({ method: 'POST', url: '/api/search', data: { sort } })
    const id1s = res1.dataflows.map(v => v.dataflowId)

    const res2 = await makeRequest({ method: 'POST', url: '/api/search', data: { sortMode: 'name' } })
    const id2s = res2.dataflows.map(v => v.dataflowId)

    expect(id1s).toEqual(id2s)
  })

  it('should sort on  lastUpdated_s desc', async () => {
    const sort = 'lastUpdated desc'
    const res1 = await makeRequest({ method: 'POST', url: '/api/search', data: { sort } })
    const id1s = res1.dataflows.map(v => v.dataflowId)

    const res2 = await makeRequest({ method: 'POST', url: '/api/search', data: { sortMode: 'date' } })
    const id2s = res2.dataflows.map(v => v.dataflowId)

    expect(id1s).toEqual(id2s)
  })

  it('should sort on score desc', async () => {
    const sort = 'score desc'
    const res1 = await makeRequest({ method: 'POST', url: '/api/search', data: { sort } })
    const id1s = res1.dataflows.map(v => v.dataflowId)

    const res2 = await makeRequest({ method: 'POST', url: '/api/search', data: { sortMode: 'score' } })
    const id2s = res2.dataflows.map(v => v.dataflowId)

    expect(id1s).toEqual(id2s)
  })

  it('should get gorder and lorder', async () => {
    const search = 'datasourceId:"dS1"'
    const sort = 'score desc'
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { sort, search } })
    const ids = res.dataflows.map(v => v.dataflowId)
    expect(ids).toEqual(['DF2', 'DF4', 'DF1', 'DF3'])
  })

  it('should search on dimensions names that have values', async () => {
    const search = 'education'
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.highlighting['DS1:DF4'].dimensions).toBeDefined()
  })

  it('should search with fl', async () => {
    const sort = 'score desc'
    const fl = ['id', 'Reference area', 'Topic']
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { sort, fl } })
    const fields = new Set()
    for (const doc of res.dataflows) for (const name in doc) fields.add(name)
    expect(fields.size).toEqual(1)
    expect(fields.has('id')).toBeTruthy()

    const facets = new Set()
    for (const name in res.facets) facets.add(name)
    expect(facets.size).toEqual(2)
    expect(facets.has('Topic')).toBeTruthy()
    expect(facets.has('Reference area')).toBeTruthy()
  })
})
