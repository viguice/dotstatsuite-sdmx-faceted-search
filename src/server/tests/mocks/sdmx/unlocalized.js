module.exports = {
  data: {
    dataflows: [
      {
        id: 'DF1',
        version: '1.0',
        agencyID: 'TEST',
        isFinal: true,
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TEST:DS1(1.0)',
      },
      {
        id: 'DF2',
        version: '1.0',
        agencyID: 'TEST',
        isFinal: true,
        names: {
          fr: 'DF2 NOM',
        },
        descriptions:{
          en: 'ENGLISH DESCRIPTION',
          it: 'ITALIAN DESCRIPTION',
          fr: 'FRENCH DESCRIPTION',
        },
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TEST:DS1(1.0)',
      },

    ],
    categorySchemes: [
      {
        id: 'CATEGORY_SCHEME',
        version: '1.0',
        agencyID: 'TEST',
        isFinal: true,
        name: 'CATEGORY_SCHEME NAME',
        names: {
          en: 'CATEGORY_SCHEME NAME',
          fr: 'CATEGORY_SCHEME NOM',
        },
        annotations: [
          {
            type: 'CategoryScheme_node_order',
            text: '0',
            texts: {
              en: '0',
            },
          },
        ],
        isPartial: false,
        categories: [
          {
            id: 'CAT1',
            name: 'CAT1 NAME',
            names: {
              en: 'CAT1 NAME',
              fr: 'CAT1 NOM',
            },
            annotations: [
              {
                type: 'CategoryScheme_node_order',
                text: '0',
                texts: {
                  en: '0',
                },
              },
            ],
          },
        ],
      },
    ],
    conceptSchemes: [
      {
        id: 'CS1',
        version: '1.0',
        agencyID: 'TEST',
        isFinal: true,
        names: {
          en: 'CS1 NAME',
          fr: 'CS1 NOM',
        },
        isPartial: false,
        concepts: [
          {
            id: 'CONCEPT1',
            names: {
              en: 'CONCEPT1 NAME',
              fr: 'CONCEPT1 NOM',
            },
          },
        ],
      },
    ],
    codelists: [
      {
        id: 'CL1',
        version: '1.0',
        agencyID: 'TEST',
        isFinal: true,
        names: {
          en: 'CL1 NAME',
          fr: 'CL1 NOM',
        },
        isPartial: true,
        codes: [
          {
            id: 'CODE1',
            name: 'CODE1 NAME',
            names: {
              en: 'CODE1 NAME',
              fr: 'CODE1 NOM',
            },
          },
          {
            id: 'CODE2',
            names: {
              en: 'CODE2 NAME',
            },
          },

        ],
      },
    ],
    dataStructures: [
      {
        id: 'DS1',
        version: '1.0',
        agencyID: 'TEST',
        isFinal: true,
        names: {
          en: 'DS1 NAME',
          fr: 'DS1 NOM',
        },
        dataStructureComponents: {
          attributeList: {
            id: 'AttributeDescriptor',
            attributes: [
              {
                id: 'ATTR1',
                conceptIdentity:
                  'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=TEST:CS1(1.0).CONCEPT1',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST:CL1(1.0)',
                },
              },
            ],
          },
          dimensionList: {
            id: 'DimensionDescriptor',
            dimensions: [
              {
                id: 'DIM1',
                position: 0,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=TEST:CS1(1.0).CONCEPT1',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST:CL1(1.0)',
                },
              },
            ],
            timeDimensions: [
              {
                id: 'TIME_PERIOD',
                position: 3,
                type: 'TimeDimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=TEST:CS1(1.0).CONCEPT1',
                localRepresentation: {
                  textFormat: {
                    textType: 'ObservationalTimePeriod',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
              },
            ],
          },
          measureList: {
            id: 'MeasureDescriptor',
            primaryMeasure: {
              id: 'OBS_VALUE',
              conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=TEST:CS1(1.0).CONCEPT1',
              localRepresentation: {
                textFormat: {
                  textType: 'Double',
                  isSequence: false,
                  isMultiLingual: false,
                },
              },
            },
          },
        },
      },
    ],
    contentConstraints: [
      {
        id: 'CR1',
        version: '1.0',
        agencyID: 'TEST',
        isFinal: false,
        type: 'Actual',
        constraintAttachment: {
          dataflows: ['urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=TEST:DF1(1.0)'],
        },
        cubeRegions: [
          {
            isIncluded: true,
            keyValues: [
              {
                id: 'DIM1',
                values: ['CODE1', 'CODE2'],
              },
              {
                id: 'TIME_PERIOD',
                timeRange: {
                  startPeriod: {
                    period: '1990-01-01T00:00:00Z',
                    isInclusive: true,
                  },
                  endPeriod: {
                    period: '2017-12-31T23:59:59Z',
                    isInclusive: true,
                  },
                },
              },
            ],
          },
        ],
      },
    ],
  },
  meta: {
    schema:
      'https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/structure-message/tools/schemas/1.0/sdmx-json-structure-schema.json',
    contentLanguages: ['en'],
    id: 'IDREF12276',
    prepared: '2020-05-26T12:18:22.4628466Z',
    test: false,
    sender: {
      id: 'Unknown',
    },
    receiver: [
      {
        id: 'Unknown',
      },
    ],
  },
};
