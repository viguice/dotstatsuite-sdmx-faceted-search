/*
 * GET http://nsi-stable-siscc.redpelicans.com/rest/dataflow/OECD.CFE/DF_TOURISM_ENT_EMP/5.0?references=all&detail=referencepartial
 * Accept: application/vnd.sdmx.structure+json;version=1.0;urn=true
 */

module.exports = ({
  id,
  agencyID,
  dataflowId = 'DF_TOURISM_ENT_EMP',
  agencyId = 'OECD.CFE',
  version = '5.0',
  csId = 'OECDCS1',
  order,
} = {}) => {
  id = id ?? dataflowId
  agencyID = agencyID ?? agencyId
  const res = {
    data: {
      dataflows: [
        {
          id,
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.CFE:DF_TOURISM_ENT_EMP(5.0)',
            },
            {
              rel: 'structure',
              urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_KEY_TOURISM(5.0)',
            },
          ],
          version,
          agencyID,
          isFinal: true,
          name: 'Enterprises and employment in tourism',
          names: {
            en: 'Enterprises and employment in tourism',
            fr: 'Enterprises and employment in tourism (fr)',
          },
          descriptions: {
            en: 'Description of Enterprises and employment in tourism',
            fr: 'Description of Enterprises and employment in tourism (fr)',
          },
          annotations: [
            {
              type: 'NonProductionDataflow',
              text: 'true',
              texts: {
                en: 'true',
              },
            },
            {
              title: 'ACTIVITY',
              type: 'LAYOUT_ROW',
            },
            {
              title: 'TIME_PERIOD',
              type: 'LAYOUT_COLUMN',
            },
            {
              title: 'REF_AREA,MEASURE,UNIT_MEASURE',
              type: 'LAYOUT_ROW_SECTION',
            },
            {
              title: 'UNIT_MULT=0,CONF_STATUS=F',
              type: 'NOT_DISPLAYED',
            },
            {
              title: 'REF_AREA=AU',
              type: 'DEFAULT',
            },
          ],
          structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_KEY_TOURISM(5.0)',
        },
      ],
      categorySchemes: [
        {
          id: csId,
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=OECD:OECDCS1(1.0)',
            },
          ],
          version: '1.0',
          agencyID: 'OECD',
          isFinal: true,
          name: 'Topic',
          names: {
            en: 'Topic',
          },
          description: 'The main OECD category scheme. First cut.',
          descriptions: {
            en: 'The main OECD category scheme. First cut.',
          },
          isPartial: true,
          categories: [
            {
              id: 'IND',
              name: 'Industry',
              names: {
                en: 'Industry',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).IND',
                },
              ],
              categories: [
                {
                  id: 'IND_TOUR',
                  name: 'Tourism',
                  names: {
                    en: 'Tourism',
                  },
                  links: [
                    {
                      rel: 'self',
                      urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).IND.IND_TOUR',
                    },
                  ],
                },
              ],
            },
          ],
        },
      ],
      categorisations: [
        {
          id: 'CAT_TOURISM_ENT_EMP',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Categorisation=OECD:CAT_TOURISM_ENT_EMP(1.0)',
            },
          ],
          version: '1.0',
          agencyID: 'OECD',
          isFinal: true,
          name: 'Categorise: DATAFLOW OECD.CFE:DF_TOURISM_ENT_EMP(5.0)',
          names: {
            en: 'Categorise: DATAFLOW OECD.CFE:DF_TOURISM_ENT_EMP(5.0)',
          },
          source: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.CFE:DF_TOURISM_ENT_EMP(5.0)',
          target: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).IND.IND_TOUR',
        },
      ],
      conceptSchemes: [
        {
          id: 'CS_TOURISM',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=OECD.CFE:CS_TOURISM(5.0)',
            },
          ],
          version: '5.0',
          agencyID: 'OECD.CFE',
          isFinal: true,
          name: 'Concept scheme for tourism',
          names: {
            en: 'Concept scheme for tourism',
          },
          isPartial: true,
          concepts: [
            {
              id: 'REF_AREA',
              name: 'Reference area',
              names: {
                en: 'Reference area',
                fr: 'Reference area (fr)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).REF_AREA',
                },
              ],
            },
            {
              id: 'MEASURE',
              name: 'Measure',
              names: {
                en: 'Measure',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).MEASURE',
                },
              ],
            },
            {
              id: 'ACTIVITY',
              name: 'Activity',
              names: {
                en: 'Activity',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).ACTIVITY',
                },
              ],
            },
            {
              id: 'UNIT_MEASURE',
              name: 'Unit of measure',
              names: {
                en: 'Unit of measure',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).UNIT_MEASURE',
                },
              ],
            },
            {
              id: 'FREQ',
              name: 'Frequency of observation',
              names: {
                en: 'Frequency of observation',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).FREQ',
                },
              ],
            },
            {
              id: 'TIME_PERIOD',
              name: 'Time period',
              names: {
                en: 'Time period',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).TIME_PERIOD',
                },
              ],
            },
            {
              id: 'OBS_VALUE',
              name: 'Observation value',
              names: {
                en: 'Observation value',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).OBS_VALUE',
                },
              ],
            },
            {
              id: 'UNIT_MULT',
              name: 'Unit multiplier',
              names: {
                en: 'Unit multiplier',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).UNIT_MULT',
                },
              ],
            },
            {
              id: 'OBS_STATUS',
              name: 'Observation status',
              names: {
                en: 'Observation status',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).OBS_STATUS',
                },
              ],
            },
            {
              id: 'CONF_STATUS',
              name: 'Confidentiality status',
              names: {
                en: 'Confidentiality status',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).CONF_STATUS',
                },
              ],
            },
            {
              id: 'DECIMALS',
              name: 'Decimals',
              names: {
                en: 'Decimals',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).DECIMALS',
                },
              ],
            },
          ],
        },
      ],
      codelists: [
        {
          id: 'CL_AREA',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.CFE:CL_AREA(1.2)',
            },
          ],
          version: '1.2',
          agencyID: 'OECD.CFE',
          isFinal: true,
          name: 'Reference area',
          names: {
            en: 'Reference area',
          },
          isPartial: false,
          codes: [
            {
              id: '_T',
              name: 'Total',
              names: {
                en: 'Total',
                fr: 'Total (fr)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2)._T',
                },
              ],
            },
            {
              id: 'AD',
              name: 'Andorra',
              names: {
                en: 'Andorra',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AD',
                },
              ],
            },
            {
              id: 'AE',
              name: 'United Arab Emirates',
              names: {
                en: 'United Arab Emirates',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AE',
                },
              ],
            },
            {
              id: 'AF',
              name: 'Afghanistan',
              names: {
                en: 'Afghanistan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AF',
                },
              ],
            },
            {
              id: 'AG',
              name: 'Antigua and Barbuda',
              names: {
                en: 'Antigua and Barbuda',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AG',
                },
              ],
            },
            {
              id: 'AI',
              name: 'Anguilla',
              names: {
                en: 'Anguilla',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AI',
                },
              ],
            },
            {
              id: 'AL',
              name: 'Albania',
              names: {
                en: 'Albania',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AL',
                },
              ],
            },
            {
              id: 'AM',
              name: 'Armenia',
              names: {
                en: 'Armenia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AM',
                },
              ],
            },
            {
              id: 'AO',
              name: 'Angola',
              names: {
                en: 'Angola',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AO',
                },
              ],
            },
            {
              id: 'AQ',
              name: 'Antarctica',
              names: {
                en: 'Antarctica',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AQ',
                },
              ],
            },
            {
              id: 'AR',
              name: 'Argentina',
              names: {
                en: 'Argentina',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AR',
                },
              ],
            },
            {
              id: 'AS',
              name: 'American Samoa',
              names: {
                en: 'American Samoa',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AS',
                },
              ],
            },
            {
              id: 'AT',
              name: 'Austria',
              names: {
                en: 'Austria',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AT',
                },
              ],
            },
            {
              id: 'AU',
              name: 'Australia',
              names: {
                en: 'Australia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AU',
                },
              ],
            },
            {
              id: 'AW',
              name: 'Aruba',
              names: {
                en: 'Aruba',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AW',
                },
              ],
            },
            {
              id: 'AX',
              name: 'Aland Islands',
              names: {
                en: 'Aland Islands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AX',
                },
              ],
            },
            {
              id: 'AZ',
              name: 'Azerbaijan',
              names: {
                en: 'Azerbaijan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AZ',
                },
              ],
            },
            {
              id: 'BA',
              name: 'Bosnia and Herzegovina',
              names: {
                en: 'Bosnia and Herzegovina',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BA',
                },
              ],
            },
            {
              id: 'BB',
              name: 'Barbados',
              names: {
                en: 'Barbados',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BB',
                },
              ],
            },
            {
              id: 'BD',
              name: 'Bangladesh',
              names: {
                en: 'Bangladesh',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BD',
                },
              ],
            },
            {
              id: 'BE',
              name: 'Belgium',
              names: {
                en: 'Belgium',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BE',
                },
              ],
            },
            {
              id: 'BE1',
              name: 'Region of Brussels-Capital',
              names: {
                en: 'Region of Brussels-Capital',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BE1',
                },
              ],
              parent: 'BE',
            },
            {
              id: 'BE2',
              name: 'Flanders',
              names: {
                en: 'Flanders',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BE2',
                },
              ],
              parent: 'BE',
            },
            {
              id: 'BE3',
              name: 'Wallonia',
              names: {
                en: 'Wallonia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BE3',
                },
              ],
              parent: 'BE',
            },
            {
              id: 'BF',
              name: 'Burkina Faso',
              names: {
                en: 'Burkina Faso',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BF',
                },
              ],
            },
            {
              id: 'BG',
              name: 'Bulgaria',
              names: {
                en: 'Bulgaria',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BG',
                },
              ],
            },
            {
              id: 'BH',
              name: 'Bahrain',
              names: {
                en: 'Bahrain',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BH',
                },
              ],
            },
            {
              id: 'BI',
              name: 'Burundi',
              names: {
                en: 'Burundi',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BI',
                },
              ],
            },
            {
              id: 'BJ',
              name: 'Benin',
              names: {
                en: 'Benin',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BJ',
                },
              ],
            },
            {
              id: 'BL',
              name: 'Saint Barthelemy',
              names: {
                en: 'Saint Barthelemy',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BL',
                },
              ],
            },
            {
              id: 'BM',
              name: 'Bermuda',
              names: {
                en: 'Bermuda',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BM',
                },
              ],
            },
            {
              id: 'BN',
              name: 'Brunei Darussalam',
              names: {
                en: 'Brunei Darussalam',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BN',
                },
              ],
            },
            {
              id: 'BO',
              name: 'Bolivia (Plurinational State of)',
              names: {
                en: 'Bolivia (Plurinational State of)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BO',
                },
              ],
            },
            {
              id: 'BQ',
              name: 'Bonaire, Sint Eustatius and Saba',
              names: {
                en: 'Bonaire, Sint Eustatius and Saba',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BQ',
                },
              ],
            },
            {
              id: 'BR',
              name: 'Brazil',
              names: {
                en: 'Brazil',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BR',
                },
              ],
            },
            {
              id: 'BS',
              name: 'Bahamas',
              names: {
                en: 'Bahamas',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BS',
                },
              ],
            },
            {
              id: 'BT',
              name: 'Bhutan',
              names: {
                en: 'Bhutan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BT',
                },
              ],
            },
            {
              id: 'BV',
              name: 'Bouvet Island',
              names: {
                en: 'Bouvet Island',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BV',
                },
              ],
            },
            {
              id: 'BW',
              name: 'Botswana',
              names: {
                en: 'Botswana',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BW',
                },
              ],
            },
            {
              id: 'BY',
              name: 'Belarus',
              names: {
                en: 'Belarus',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BY',
                },
              ],
            },
            {
              id: 'BZ',
              name: 'Belize',
              names: {
                en: 'Belize',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).BZ',
                },
              ],
            },
            {
              id: 'CA',
              name: 'Canada',
              names: {
                en: 'Canada',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CA',
                },
              ],
            },
            {
              id: 'CC',
              name: 'Cocos (Keeling) Islands',
              names: {
                en: 'Cocos (Keeling) Islands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CC',
                },
              ],
            },
            {
              id: 'CD',
              name: 'Congo, Democratic Republic of the',
              names: {
                en: 'Congo, Democratic Republic of the',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CD',
                },
              ],
            },
            {
              id: 'CF',
              name: 'Central African Republic',
              names: {
                en: 'Central African Republic',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CF',
                },
              ],
            },
            {
              id: 'CG',
              name: 'Congo',
              names: {
                en: 'Congo',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CG',
                },
              ],
            },
            {
              id: 'CH',
              name: 'Switzerland',
              names: {
                en: 'Switzerland',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CH',
                },
              ],
            },
            {
              id: 'CI',
              name: "Cote d'Ivoire",
              names: {
                en: "Cote d'Ivoire",
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CI',
                },
              ],
            },
            {
              id: 'CK',
              name: 'Cook Islands',
              names: {
                en: 'Cook Islands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CK',
                },
              ],
            },
            {
              id: 'CL',
              name: 'Chile',
              names: {
                en: 'Chile',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CL',
                },
              ],
            },
            {
              id: 'CM',
              name: 'Cameroon',
              names: {
                en: 'Cameroon',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CM',
                },
              ],
            },
            {
              id: 'CN',
              name: 'China',
              names: {
                en: 'China',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CN',
                },
              ],
            },
            {
              id: 'CO',
              name: 'Colombia',
              names: {
                en: 'Colombia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CO',
                },
              ],
            },
            {
              id: 'CR',
              name: 'Costa Rica',
              names: {
                en: 'Costa Rica',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CR',
                },
              ],
            },
            {
              id: 'CU',
              name: 'Cuba',
              names: {
                en: 'Cuba',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CU',
                },
              ],
            },
            {
              id: 'CV',
              name: 'Cabo Verde',
              names: {
                en: 'Cabo Verde',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CV',
                },
              ],
            },
            {
              id: 'CW',
              name: 'Curacao',
              names: {
                en: 'Curacao',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CW',
                },
              ],
            },
            {
              id: 'CX',
              name: 'Christmas Island',
              names: {
                en: 'Christmas Island',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CX',
                },
              ],
            },
            {
              id: 'CY',
              name: 'Cyprus',
              names: {
                en: 'Cyprus',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CY',
                },
              ],
            },
            {
              id: 'CZ',
              name: 'Czechia',
              names: {
                en: 'Czechia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CZ',
                },
              ],
            },
            {
              id: 'DE',
              name: 'Germany',
              names: {
                en: 'Germany',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).DE',
                },
              ],
            },
            {
              id: 'DJ',
              name: 'Djibouti',
              names: {
                en: 'Djibouti',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).DJ',
                },
              ],
            },
            {
              id: 'DK',
              name: 'Denmark',
              names: {
                en: 'Denmark',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).DK',
                },
              ],
            },
            {
              id: 'DM',
              name: 'Dominica',
              names: {
                en: 'Dominica',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).DM',
                },
              ],
            },
            {
              id: 'DO',
              name: 'Dominican Republic',
              names: {
                en: 'Dominican Republic',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).DO',
                },
              ],
            },
            {
              id: 'DZ',
              name: 'Algeria',
              names: {
                en: 'Algeria',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).DZ',
                },
              ],
            },
            {
              id: 'EC',
              name: 'Ecuador',
              names: {
                en: 'Ecuador',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).EC',
                },
              ],
            },
            {
              id: 'EE',
              name: 'Estonia',
              names: {
                en: 'Estonia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).EE',
                },
              ],
            },
            {
              id: 'EG',
              name: 'Egypt',
              names: {
                en: 'Egypt',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).EG',
                },
              ],
            },
            {
              id: 'EH',
              name: 'Western Sahara',
              names: {
                en: 'Western Sahara',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).EH',
                },
              ],
            },
            {
              id: 'ER',
              name: 'Eritrea',
              names: {
                en: 'Eritrea',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).ER',
                },
              ],
            },
            {
              id: 'ES',
              name: 'Spain',
              names: {
                en: 'Spain',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).ES',
                },
              ],
            },
            {
              id: 'ET',
              name: 'Ethiopia',
              names: {
                en: 'Ethiopia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).ET',
                },
              ],
            },
            {
              id: 'FI',
              name: 'Finland',
              names: {
                en: 'Finland',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).FI',
                },
              ],
            },
            {
              id: 'FJ',
              name: 'Fiji',
              names: {
                en: 'Fiji',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).FJ',
                },
              ],
            },
            {
              id: 'FK',
              name: 'Falkland Islands (Malvinas)',
              names: {
                en: 'Falkland Islands (Malvinas)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).FK',
                },
              ],
            },
            {
              id: 'FM',
              name: 'Micronesia (Federated States of)',
              names: {
                en: 'Micronesia (Federated States of)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).FM',
                },
              ],
            },
            {
              id: 'FO',
              name: 'Faroe Islands',
              names: {
                en: 'Faroe Islands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).FO',
                },
              ],
            },
            {
              id: 'FR',
              name: 'France',
              names: {
                en: 'France',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).FR',
                },
              ],
            },
            {
              id: 'GA',
              name: 'Gabon',
              names: {
                en: 'Gabon',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GA',
                },
              ],
            },
            {
              id: 'GB',
              name: 'United Kingdom of Great Britain and Northern Ireland',
              names: {
                en: 'United Kingdom of Great Britain and Northern Ireland',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GB',
                },
              ],
            },
            {
              id: 'GD',
              name: 'Grenada',
              names: {
                en: 'Grenada',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GD',
                },
              ],
            },
            {
              id: 'GE',
              name: 'Georgia',
              names: {
                en: 'Georgia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GE',
                },
              ],
            },
            {
              id: 'GF',
              name: 'French Guiana',
              names: {
                en: 'French Guiana',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GF',
                },
              ],
            },
            {
              id: 'GG',
              name: 'Guernsey',
              names: {
                en: 'Guernsey',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GG',
                },
              ],
            },
            {
              id: 'GH',
              name: 'Ghana',
              names: {
                en: 'Ghana',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GH',
                },
              ],
            },
            {
              id: 'GI',
              name: 'Gibraltar',
              names: {
                en: 'Gibraltar',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GI',
                },
              ],
            },
            {
              id: 'GL',
              name: 'Greenland',
              names: {
                en: 'Greenland',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GL',
                },
              ],
            },
            {
              id: 'GM',
              name: 'Gambia',
              names: {
                en: 'Gambia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GM',
                },
              ],
            },
            {
              id: 'GN',
              name: 'Guinea',
              names: {
                en: 'Guinea',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GN',
                },
              ],
            },
            {
              id: 'GP',
              name: 'Guadeloupe',
              names: {
                en: 'Guadeloupe',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GP',
                },
              ],
            },
            {
              id: 'GQ',
              name: 'Equatorial Guinea',
              names: {
                en: 'Equatorial Guinea',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GQ',
                },
              ],
            },
            {
              id: 'GR',
              name: 'Greece',
              names: {
                en: 'Greece',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GR',
                },
              ],
            },
            {
              id: 'GS',
              name: 'South Georgia and the South Sandwich Islands',
              names: {
                en: 'South Georgia and the South Sandwich Islands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GS',
                },
              ],
            },
            {
              id: 'GT',
              name: 'Guatemala',
              names: {
                en: 'Guatemala',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GT',
                },
              ],
            },
            {
              id: 'GU',
              name: 'Guam',
              names: {
                en: 'Guam',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GU',
                },
              ],
            },
            {
              id: 'GW',
              name: 'Guinea-Bissau',
              names: {
                en: 'Guinea-Bissau',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GW',
                },
              ],
            },
            {
              id: 'GY',
              name: 'Guyana',
              names: {
                en: 'Guyana',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).GY',
                },
              ],
            },
            {
              id: 'HK',
              name: 'Hong Kong',
              names: {
                en: 'Hong Kong',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).HK',
                },
              ],
            },
            {
              id: 'HM',
              name: 'Heard Island and McDonald Islands',
              names: {
                en: 'Heard Island and McDonald Islands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).HM',
                },
              ],
            },
            {
              id: 'HN',
              name: 'Honduras',
              names: {
                en: 'Honduras',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).HN',
                },
              ],
            },
            {
              id: 'HR',
              name: 'Croatia',
              names: {
                en: 'Croatia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).HR',
                },
              ],
            },
            {
              id: 'HT',
              name: 'Haiti',
              names: {
                en: 'Haiti',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).HT',
                },
              ],
            },
            {
              id: 'HU',
              name: 'Hungary',
              names: {
                en: 'Hungary',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).HU',
                },
              ],
            },
            {
              id: 'ID',
              name: 'Indonesia',
              names: {
                en: 'Indonesia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).ID',
                },
              ],
            },
            {
              id: 'IE',
              name: 'Ireland',
              names: {
                en: 'Ireland',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).IE',
                },
              ],
            },
            {
              id: 'IL',
              name: 'Israel',
              names: {
                en: 'Israel',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).IL',
                },
              ],
            },
            {
              id: 'IM',
              name: 'Isle of Man',
              names: {
                en: 'Isle of Man',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).IM',
                },
              ],
            },
            {
              id: 'IN',
              name: 'India',
              names: {
                en: 'India',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).IN',
                },
              ],
            },
            {
              id: 'IO',
              name: 'British Indian Ocean Territory',
              names: {
                en: 'British Indian Ocean Territory',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).IO',
                },
              ],
            },
            {
              id: 'IQ',
              name: 'Iraq',
              names: {
                en: 'Iraq',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).IQ',
                },
              ],
            },
            {
              id: 'IR',
              name: 'Iran (Islamic Republic of)',
              names: {
                en: 'Iran (Islamic Republic of)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).IR',
                },
              ],
            },
            {
              id: 'IS',
              name: 'Iceland',
              names: {
                en: 'Iceland',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).IS',
                },
              ],
            },
            {
              id: 'IT',
              name: 'Italy',
              names: {
                en: 'Italy',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).IT',
                },
              ],
            },
            {
              id: 'JE',
              name: 'Jersey',
              names: {
                en: 'Jersey',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).JE',
                },
              ],
            },
            {
              id: 'JM',
              name: 'Jamaica',
              names: {
                en: 'Jamaica',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).JM',
                },
              ],
            },
            {
              id: 'JO',
              name: 'Jordan',
              names: {
                en: 'Jordan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).JO',
                },
              ],
            },
            {
              id: 'JP',
              name: 'Japan',
              names: {
                en: 'Japan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).JP',
                },
              ],
            },
            {
              id: 'KE',
              name: 'Kenya',
              names: {
                en: 'Kenya',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).KE',
                },
              ],
            },
            {
              id: 'KG',
              name: 'Kyrgyzstan',
              names: {
                en: 'Kyrgyzstan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).KG',
                },
              ],
            },
            {
              id: 'KH',
              name: 'Cambodia',
              names: {
                en: 'Cambodia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).KH',
                },
              ],
            },
            {
              id: 'KI',
              name: 'Kiribati',
              names: {
                en: 'Kiribati',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).KI',
                },
              ],
            },
            {
              id: 'KM',
              name: 'Comoros',
              names: {
                en: 'Comoros',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).KM',
                },
              ],
            },
            {
              id: 'KN',
              name: 'Saint Kitts and Nevis',
              names: {
                en: 'Saint Kitts and Nevis',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).KN',
                },
              ],
            },
            {
              id: 'KP',
              name: "Korea (Democratic People's Republic of)",
              names: {
                en: "Korea (Democratic People's Republic of)",
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).KP',
                },
              ],
            },
            {
              id: 'KR',
              name: 'Korea, Republic of',
              names: {
                en: 'Korea, Republic of',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).KR',
                },
              ],
            },
            {
              id: 'KW',
              name: 'Kuwait',
              names: {
                en: 'Kuwait',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).KW',
                },
              ],
            },
            {
              id: 'KY',
              name: 'Cayman Islands',
              names: {
                en: 'Cayman Islands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).KY',
                },
              ],
            },
            {
              id: 'KZ',
              name: 'Kazakhstan',
              names: {
                en: 'Kazakhstan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).KZ',
                },
              ],
            },
            {
              id: 'LA',
              name: "Lao People's Democratic Republic",
              names: {
                en: "Lao People's Democratic Republic",
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).LA',
                },
              ],
            },
            {
              id: 'LB',
              name: 'Lebanon',
              names: {
                en: 'Lebanon',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).LB',
                },
              ],
            },
            {
              id: 'LC',
              name: 'Saint Lucia',
              names: {
                en: 'Saint Lucia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).LC',
                },
              ],
            },
            {
              id: 'LI',
              name: 'Liechtenstein',
              names: {
                en: 'Liechtenstein',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).LI',
                },
              ],
            },
            {
              id: 'LK',
              name: 'Sri Lanka',
              names: {
                en: 'Sri Lanka',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).LK',
                },
              ],
            },
            {
              id: 'LR',
              name: 'Liberia',
              names: {
                en: 'Liberia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).LR',
                },
              ],
            },
            {
              id: 'LS',
              name: 'Lesotho',
              names: {
                en: 'Lesotho',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).LS',
                },
              ],
            },
            {
              id: 'LT',
              name: 'Lithuania',
              names: {
                en: 'Lithuania',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).LT',
                },
              ],
            },
            {
              id: 'LU',
              name: 'Luxembourg',
              names: {
                en: 'Luxembourg',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).LU',
                },
              ],
            },
            {
              id: 'LV',
              name: 'Latvia',
              names: {
                en: 'Latvia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).LV',
                },
              ],
            },
            {
              id: 'LY',
              name: 'Libya',
              names: {
                en: 'Libya',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).LY',
                },
              ],
            },
            {
              id: 'MA',
              name: 'Morocco',
              names: {
                en: 'Morocco',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MA',
                },
              ],
            },
            {
              id: 'MC',
              name: 'Monaco',
              names: {
                en: 'Monaco',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MC',
                },
              ],
            },
            {
              id: 'MD',
              name: 'Moldova, Republic of',
              names: {
                en: 'Moldova, Republic of',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MD',
                },
              ],
            },
            {
              id: 'ME',
              name: 'Montenegro',
              names: {
                en: 'Montenegro',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).ME',
                },
              ],
            },
            {
              id: 'MF',
              name: 'Saint Martin (French part)',
              names: {
                en: 'Saint Martin (French part)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MF',
                },
              ],
            },
            {
              id: 'MG',
              name: 'Madagascar',
              names: {
                en: 'Madagascar',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MG',
                },
              ],
            },
            {
              id: 'MH',
              name: 'Marshall Islands',
              names: {
                en: 'Marshall Islands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MH',
                },
              ],
            },
            {
              id: 'MK',
              name: 'North Macedonia',
              names: {
                en: 'North Macedonia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MK',
                },
              ],
            },
            {
              id: 'ML',
              name: 'Mali',
              names: {
                en: 'Mali',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).ML',
                },
              ],
            },
            {
              id: 'MM',
              name: 'Myanmar',
              names: {
                en: 'Myanmar',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MM',
                },
              ],
            },
            {
              id: 'MN',
              name: 'Mongolia',
              names: {
                en: 'Mongolia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MN',
                },
              ],
            },
            {
              id: 'MO',
              name: 'Macao',
              names: {
                en: 'Macao',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MO',
                },
              ],
            },
            {
              id: 'MP',
              name: 'Northern Mariana Islands',
              names: {
                en: 'Northern Mariana Islands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MP',
                },
              ],
            },
            {
              id: 'MQ',
              name: 'Martinique',
              names: {
                en: 'Martinique',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MQ',
                },
              ],
            },
            {
              id: 'MR',
              name: 'Mauritania',
              names: {
                en: 'Mauritania',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MR',
                },
              ],
            },
            {
              id: 'MS',
              name: 'Montserrat',
              names: {
                en: 'Montserrat',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MS',
                },
              ],
            },
            {
              id: 'MT',
              name: 'Malta',
              names: {
                en: 'Malta',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MT',
                },
              ],
            },
            {
              id: 'MU',
              name: 'Mauritius',
              names: {
                en: 'Mauritius',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MU',
                },
              ],
            },
            {
              id: 'MV',
              name: 'Maldives',
              names: {
                en: 'Maldives',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MV',
                },
              ],
            },
            {
              id: 'MW',
              name: 'Malawi',
              names: {
                en: 'Malawi',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MW',
                },
              ],
            },
            {
              id: 'MX',
              name: 'Mexico',
              names: {
                en: 'Mexico',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MX',
                },
              ],
            },
            {
              id: 'MY',
              name: 'Malaysia',
              names: {
                en: 'Malaysia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MY',
                },
              ],
            },
            {
              id: 'MZ',
              name: 'Mozambique',
              names: {
                en: 'Mozambique',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).MZ',
                },
              ],
            },
            {
              id: 'NA',
              name: 'Namibia',
              names: {
                en: 'Namibia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).NA',
                },
              ],
            },
            {
              id: 'NC',
              name: 'New Caledonia',
              names: {
                en: 'New Caledonia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).NC',
                },
              ],
            },
            {
              id: 'NE',
              name: 'Niger',
              names: {
                en: 'Niger',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).NE',
                },
              ],
            },
            {
              id: 'NF',
              name: 'Norfolk Island',
              names: {
                en: 'Norfolk Island',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).NF',
                },
              ],
            },
            {
              id: 'NG',
              name: 'Nigeria',
              names: {
                en: 'Nigeria',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).NG',
                },
              ],
            },
            {
              id: 'NI',
              name: 'Nicaragua',
              names: {
                en: 'Nicaragua',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).NI',
                },
              ],
            },
            {
              id: 'NL',
              name: 'Netherlands',
              names: {
                en: 'Netherlands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).NL',
                },
              ],
            },
            {
              id: 'NO',
              name: 'Norway',
              names: {
                en: 'Norway',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).NO',
                },
              ],
            },
            {
              id: 'NP',
              name: 'Nepal',
              names: {
                en: 'Nepal',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).NP',
                },
              ],
            },
            {
              id: 'NR',
              name: 'Nauru',
              names: {
                en: 'Nauru',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).NR',
                },
              ],
            },
            {
              id: 'NU',
              name: 'Niue',
              names: {
                en: 'Niue',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).NU',
                },
              ],
            },
            {
              id: 'NZ',
              name: 'New Zealand',
              names: {
                en: 'New Zealand',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).NZ',
                },
              ],
            },
            {
              id: 'OM',
              name: 'Oman',
              names: {
                en: 'Oman',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).OM',
                },
              ],
            },
            {
              id: 'PA',
              name: 'Panama',
              names: {
                en: 'Panama',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PA',
                },
              ],
            },
            {
              id: 'PE',
              name: 'Peru',
              names: {
                en: 'Peru',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PE',
                },
              ],
            },
            {
              id: 'PF',
              name: 'French Polynesia',
              names: {
                en: 'French Polynesia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PF',
                },
              ],
            },
            {
              id: 'PG',
              name: 'Papua New Guinea',
              names: {
                en: 'Papua New Guinea',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PG',
                },
              ],
            },
            {
              id: 'PH',
              name: 'Philippines',
              names: {
                en: 'Philippines',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PH',
                },
              ],
            },
            {
              id: 'PK',
              name: 'Pakistan',
              names: {
                en: 'Pakistan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PK',
                },
              ],
            },
            {
              id: 'PL',
              name: 'Poland',
              names: {
                en: 'Poland',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PL',
                },
              ],
            },
            {
              id: 'PM',
              name: 'Saint Pierre and Miquelon',
              names: {
                en: 'Saint Pierre and Miquelon',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PM',
                },
              ],
            },
            {
              id: 'PN',
              name: 'Pitcairn',
              names: {
                en: 'Pitcairn',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PN',
                },
              ],
            },
            {
              id: 'PR',
              name: 'Puerto Rico',
              names: {
                en: 'Puerto Rico',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PR',
                },
              ],
            },
            {
              id: 'PS',
              name: 'Palestine, State of',
              names: {
                en: 'Palestine, State of',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PS',
                },
              ],
            },
            {
              id: 'PT',
              name: 'Portugal',
              names: {
                en: 'Portugal',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PT',
                },
              ],
            },
            {
              id: 'PW',
              name: 'Palau',
              names: {
                en: 'Palau',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PW',
                },
              ],
            },
            {
              id: 'PY',
              name: 'Paraguay',
              names: {
                en: 'Paraguay',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).PY',
                },
              ],
            },
            {
              id: 'QA',
              name: 'Qatar',
              names: {
                en: 'Qatar',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).QA',
                },
              ],
            },
            {
              id: 'RE',
              name: 'Reunion',
              names: {
                en: 'Reunion',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).RE',
                },
              ],
            },
            {
              id: 'RO',
              name: 'Romania',
              names: {
                en: 'Romania',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).RO',
                },
              ],
            },
            {
              id: 'RS',
              name: 'Serbia',
              names: {
                en: 'Serbia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).RS',
                },
              ],
            },
            {
              id: 'RU',
              name: 'Russian Federation',
              names: {
                en: 'Russian Federation',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).RU',
                },
              ],
            },
            {
              id: 'RW',
              name: 'Rwanda',
              names: {
                en: 'Rwanda',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).RW',
                },
              ],
            },
            {
              id: 'SA',
              name: 'Saudi Arabia',
              names: {
                en: 'Saudi Arabia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SA',
                },
              ],
            },
            {
              id: 'SB',
              name: 'Solomon Islands',
              names: {
                en: 'Solomon Islands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SB',
                },
              ],
            },
            {
              id: 'SC',
              name: 'Seychelles',
              names: {
                en: 'Seychelles',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SC',
                },
              ],
            },
            {
              id: 'SD',
              name: 'Sudan',
              names: {
                en: 'Sudan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SD',
                },
              ],
            },
            {
              id: 'SE',
              name: 'Sweden',
              names: {
                en: 'Sweden',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SE',
                },
              ],
            },
            {
              id: 'SG',
              name: 'Singapore',
              names: {
                en: 'Singapore',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SG',
                },
              ],
            },
            {
              id: 'SH',
              name: 'Saint Helena, Ascension and Tristan da Cunha',
              names: {
                en: 'Saint Helena, Ascension and Tristan da Cunha',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SH',
                },
              ],
            },
            {
              id: 'SI',
              name: 'Slovenia',
              names: {
                en: 'Slovenia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SI',
                },
              ],
            },
            {
              id: 'SJ',
              name: 'Svalbard and Jan Mayen',
              names: {
                en: 'Svalbard and Jan Mayen',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SJ',
                },
              ],
            },
            {
              id: 'SK',
              name: 'Slovakia',
              names: {
                en: 'Slovakia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SK',
                },
              ],
            },
            {
              id: 'SL',
              name: 'Sierra Leone',
              names: {
                en: 'Sierra Leone',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SL',
                },
              ],
            },
            {
              id: 'SM',
              name: 'San Marino',
              names: {
                en: 'San Marino',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SM',
                },
              ],
            },
            {
              id: 'SN',
              name: 'Senegal',
              names: {
                en: 'Senegal',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SN',
                },
              ],
            },
            {
              id: 'SO',
              name: 'Somalia',
              names: {
                en: 'Somalia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SO',
                },
              ],
            },
            {
              id: 'SR',
              name: 'Suriname',
              names: {
                en: 'Suriname',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SR',
                },
              ],
            },
            {
              id: 'SS',
              name: 'South Sudan',
              names: {
                en: 'South Sudan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SS',
                },
              ],
            },
            {
              id: 'ST',
              name: 'Sao Tome and Principe',
              names: {
                en: 'Sao Tome and Principe',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).ST',
                },
              ],
            },
            {
              id: 'SV',
              name: 'El Salvador',
              names: {
                en: 'El Salvador',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SV',
                },
              ],
            },
            {
              id: 'SX',
              name: 'Sint Maarten (Dutch part)',
              names: {
                en: 'Sint Maarten (Dutch part)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SX',
                },
              ],
            },
            {
              id: 'SY',
              name: 'Syrian Arab Republic',
              names: {
                en: 'Syrian Arab Republic',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SY',
                },
              ],
            },
            {
              id: 'SZ',
              name: 'Eswatini',
              names: {
                en: 'Eswatini',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).SZ',
                },
              ],
            },
            {
              id: 'TC',
              name: 'Turks and Caicos Islands',
              names: {
                en: 'Turks and Caicos Islands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TC',
                },
              ],
            },
            {
              id: 'TD',
              name: 'Chad',
              names: {
                en: 'Chad',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TD',
                },
              ],
            },
            {
              id: 'TF',
              name: 'French Southern Territories',
              names: {
                en: 'French Southern Territories',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TF',
                },
              ],
            },
            {
              id: 'TG',
              name: 'Togo',
              names: {
                en: 'Togo',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TG',
                },
              ],
            },
            {
              id: 'TH',
              name: 'Thailand',
              names: {
                en: 'Thailand',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TH',
                },
              ],
            },
            {
              id: 'TJ',
              name: 'Tajikistan',
              names: {
                en: 'Tajikistan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TJ',
                },
              ],
            },
            {
              id: 'TK',
              name: 'Tokelau',
              names: {
                en: 'Tokelau',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TK',
                },
              ],
            },
            {
              id: 'TL',
              name: 'Timor-Leste',
              names: {
                en: 'Timor-Leste',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TL',
                },
              ],
            },
            {
              id: 'TM',
              name: 'Turkmenistan',
              names: {
                en: 'Turkmenistan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TM',
                },
              ],
            },
            {
              id: 'TN',
              name: 'Tunisia',
              names: {
                en: 'Tunisia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TN',
                },
              ],
            },
            {
              id: 'TO',
              name: 'Tonga',
              names: {
                en: 'Tonga',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TO',
                },
              ],
            },
            {
              id: 'TR',
              name: 'Turkey',
              names: {
                en: 'Turkey',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TR',
                },
              ],
            },
            {
              id: 'TT',
              name: 'Trinidad and Tobago',
              names: {
                en: 'Trinidad and Tobago',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TT',
                },
              ],
            },
            {
              id: 'TV',
              name: 'Tuvalu',
              names: {
                en: 'Tuvalu',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TV',
                },
              ],
            },
            {
              id: 'TW',
              name: 'Taiwan, Province of China',
              names: {
                en: 'Taiwan, Province of China',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TW',
                },
              ],
            },
            {
              id: 'TZ',
              name: 'Tanzania, United Republic of',
              names: {
                en: 'Tanzania, United Republic of',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).TZ',
                },
              ],
            },
            {
              id: 'UA',
              name: 'Ukraine',
              names: {
                en: 'Ukraine',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).UA',
                },
              ],
            },
            {
              id: 'UG',
              name: 'Uganda',
              names: {
                en: 'Uganda',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).UG',
                },
              ],
            },
            {
              id: 'UM',
              name: 'United States Minor Outlying Islands',
              names: {
                en: 'United States Minor Outlying Islands',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).UM',
                },
              ],
            },
            {
              id: 'US',
              name: 'United States of America',
              names: {
                en: 'United States of America',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).US',
                },
              ],
            },
            {
              id: 'UY',
              name: 'Uruguay',
              names: {
                en: 'Uruguay',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).UY',
                },
              ],
            },
            {
              id: 'UZ',
              name: 'Uzbekistan',
              names: {
                en: 'Uzbekistan',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).UZ',
                },
              ],
            },
            {
              id: 'VA',
              name: 'Holy See',
              names: {
                en: 'Holy See',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).VA',
                },
              ],
            },
            {
              id: 'VC',
              name: 'Saint Vincent and the Grenadines',
              names: {
                en: 'Saint Vincent and the Grenadines',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).VC',
                },
              ],
            },
            {
              id: 'VE',
              name: 'Venezuela (Bolivarian Republic of)',
              names: {
                en: 'Venezuela (Bolivarian Republic of)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).VE',
                },
              ],
            },
            {
              id: 'VG',
              name: 'Virgin Islands (British)',
              names: {
                en: 'Virgin Islands (British)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).VG',
                },
              ],
            },
            {
              id: 'VI',
              name: 'Virgin Islands (U.S.)',
              names: {
                en: 'Virgin Islands (U.S.)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).VI',
                },
              ],
            },
            {
              id: 'VN',
              name: 'Viet Nam',
              names: {
                en: 'Viet Nam',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).VN',
                },
              ],
            },
            {
              id: 'VU',
              name: 'Vanuatu',
              names: {
                en: 'Vanuatu',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).VU',
                },
              ],
            },
            {
              id: 'WF',
              name: 'Wallis and Futuna',
              names: {
                en: 'Wallis and Futuna',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).WF',
                },
              ],
            },
            {
              id: 'WS',
              name: 'Samoa',
              names: {
                en: 'Samoa',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).WS',
                },
              ],
            },
            {
              id: 'YE',
              name: 'Yemen',
              names: {
                en: 'Yemen',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).YE',
                },
              ],
            },
            {
              id: 'YT',
              name: 'Mayotte',
              names: {
                en: 'Mayotte',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).YT',
                },
              ],
            },
            {
              id: 'ZA',
              name: 'South Africa',
              names: {
                en: 'South Africa',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).ZA',
                },
              ],
            },
            {
              id: 'ZM',
              name: 'Zambia',
              names: {
                en: 'Zambia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).ZM',
                },
              ],
            },
            {
              id: 'ZW',
              name: 'Zimbabwe',
              names: {
                en: 'Zimbabwe',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).ZW',
                },
              ],
            },
            {
              id: 'CH_LI',
              name: 'Switzerland and Liechtenstein',
              names: {
                en: 'Switzerland and Liechtenstein',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).CH_LI',
                },
              ],
            },
            {
              id: 'US_CA',
              name: 'United States and Canada',
              names: {
                en: 'United States and Canada',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).US_CA',
                },
              ],
            },
            {
              id: 'AGG_AMER',
              name: 'America (Continent)',
              names: {
                en: 'America (Continent)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AGG_AMER',
                },
              ],
            },
            {
              id: 'AGG_AMERC',
              name: 'Central America',
              names: {
                en: 'Central America',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AGG_AMERC',
                },
              ],
            },
            {
              id: 'AGG_AMERN',
              name: 'North America',
              names: {
                en: 'North America',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AGG_AMERN',
                },
              ],
            },
            {
              id: 'AGG_AMERS',
              name: 'South America',
              names: {
                en: 'South America',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AGG_AMERS',
                },
              ],
            },
            {
              id: 'AGG_EUROPE',
              name: 'Europe',
              names: {
                en: 'Europe',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AGG_EUROPE',
                },
              ],
            },
            {
              id: 'AGG_NORD',
              name: 'Nordic countries',
              names: {
                en: 'Nordic countries',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AGG_NORD',
                },
              ],
            },
            {
              id: 'AGG_EUROPE_NS',
              name: 'Europe not specified',
              names: {
                en: 'Europe not specified',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AGG_EUROPE_NS',
                },
              ],
            },
            {
              id: 'AGG_EUROPE_O',
              name: 'Other Europe',
              names: {
                en: 'Other Europe',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AGG_EUROPE_O',
                },
              ],
            },
            {
              id: 'AGG_ASIAT',
              name: 'Asia',
              names: {
                en: 'Asia',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2).AGG_ASIAT',
                },
              ],
            },
            {
              id: '_Z',
              name: 'Not applicable',
              names: {
                en: 'Not applicable',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_AREA(1.2)._Z',
                },
              ],
            },
          ],
        },
        {
          id: 'CL_TOURISM_ACTIVITY',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.CFE:CL_TOURISM_ACTIVITY(1.0)',
            },
          ],
          version: '1.0',
          agencyID: 'OECD.CFE',
          isFinal: true,
          name: 'Activity',
          names: {
            en: 'Activity',
          },
          isPartial: false,
          codes: [
            {
              id: 'AGG_TOUR',
              name: 'Tourism',
              names: {
                en: 'Tourism',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).AGG_TOUR',
                },
              ],
            },
            {
              id: 'IND',
              name: 'Tourism industries',
              names: {
                en: 'Tourism industries',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).IND',
                },
              ],
              parent: 'AGG_TOUR',
            },
            {
              id: 'ACCOM_SVCS',
              name: 'Accommodation services for visitors',
              names: {
                en: 'Accommodation services for visitors',
              },
              description: '5510+5520+5590+6810+6820',
              descriptions: {
                en: '5510+5520+5590+6810+6820',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).ACCOM_SVCS',
                },
              ],
              parent: 'IND',
            },
            {
              id: 'ACCOM_SVCS_HOTELS',
              name: 'Hotels and similar establishments',
              names: {
                en: 'Hotels and similar establishments',
              },
              description: '5510',
              descriptions: {
                en: '5510',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).ACCOM_SVCS_HOTELS',
                },
              ],
              parent: 'ACCOM_SVCS',
            },
            {
              id: 'FOOD_BEV',
              name: 'Food and beverage serving industries',
              names: {
                en: 'Food and beverage serving industries',
              },
              description: '5610+5629+5630',
              descriptions: {
                en: '5610+5629+5630',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).FOOD_BEV',
                },
              ],
              parent: 'IND',
            },
            {
              id: 'TRANSPORT',
              name: 'Passenger transport',
              names: {
                en: 'Passenger transport',
              },
              description: '5110+4911+4922+5011+5021',
              descriptions: {
                en: '5110+4911+4922+5011+5021',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).TRANSPORT',
                },
              ],
              parent: 'IND',
            },
            {
              id: 'TRANSPORT_AIR',
              name: 'Air passenger transport',
              names: {
                en: 'Air passenger transport',
              },
              description: '5110',
              descriptions: {
                en: '5110',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).TRANSPORT_AIR',
                },
              ],
              parent: 'TRANSPORT',
            },
            {
              id: 'TRANSPORT_RAIL',
              name: 'Railways passenger transport',
              names: {
                en: 'Railways passenger transport',
              },
              description: '4911',
              descriptions: {
                en: '4911',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).TRANSPORT_RAIL',
                },
              ],
              parent: 'TRANSPORT',
            },
            {
              id: 'TRANSPORT_ROAD',
              name: 'Road passenger transport',
              names: {
                en: 'Road passenger transport',
              },
              description: '4922',
              descriptions: {
                en: '4922',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).TRANSPORT_ROAD',
                },
              ],
              parent: 'TRANSPORT',
            },
            {
              id: 'TRANSPORT_WATER',
              name: 'Water passenger transport',
              names: {
                en: 'Water passenger transport',
              },
              description: '5011+5021',
              descriptions: {
                en: '5011+5021',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).TRANSPORT_WATER',
                },
              ],
              parent: 'TRANSPORT',
            },
            {
              id: 'SUPPORT_SVC',
              name: 'Passenger transport supporting services',
              names: {
                en: 'Passenger transport supporting services',
              },
              description: 'Not specified - depends on country',
              descriptions: {
                en: 'Not specified - depends on country',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).SUPPORT_SVC',
                },
              ],
              parent: 'IND',
            },
            {
              id: 'EQUIP_RENTAL',
              name: 'Transport equipment rental',
              names: {
                en: 'Transport equipment rental',
              },
              description: '7710',
              descriptions: {
                en: '7710',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).EQUIP_RENTAL',
                },
              ],
              parent: 'IND',
            },
            {
              id: 'TRVL_AGENT',
              name: 'Travel agencies and other reservation services industry',
              names: {
                en: 'Travel agencies and other reservation services industry',
              },
              description: '7911+7912+7990',
              descriptions: {
                en: '7911+7912+7990',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).TRVL_AGENT',
                },
              ],
              parent: 'IND',
            },
            {
              id: 'CULTURE',
              name: 'Cultural industry',
              names: {
                en: 'Cultural industry',
              },
              description: '9000+9102+9103',
              descriptions: {
                en: '9000+9102+9103',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).CULTURE',
                },
              ],
              parent: 'IND',
            },
            {
              id: 'SPORTS_REC',
              name: 'Sports and recreation industry',
              names: {
                en: 'Sports and recreation industry',
              },
              description: '7721+9200+9311+9319+9321+9329',
              descriptions: {
                en: '7721+9200+9311+9319+9321+9329',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).SPORTS_REC',
                },
              ],
              parent: 'IND',
            },
            {
              id: 'RETAIL',
              name: 'Retail trade of country-specific tourism charasteristic goods',
              names: {
                en: 'Retail trade of country-specific tourism charasteristic goods',
              },
              description: 'Not specified - depends on country',
              descriptions: {
                en: 'Not specified - depends on country',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).RETAIL',
                },
              ],
              parent: 'IND',
            },
            {
              id: 'OTHER_COUNTRY_SPEC',
              name: 'Other country-specific tourism industries',
              names: {
                en: 'Other country-specific tourism industries',
              },
              description: 'Not specified - depends on country',
              descriptions: {
                en: 'Not specified - depends on country',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0).OTHER_COUNTRY_SPEC',
                },
              ],
              parent: 'IND',
            },
            {
              id: '_O',
              name: 'Other industries',
              names: {
                en: 'Other industries',
              },
              description: 'Not specified - depends on country',
              descriptions: {
                en: 'Not specified - depends on country',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_ACTIVITY(1.0)._O',
                },
              ],
              parent: 'AGG_TOUR',
            },
          ],
        },
        {
          id: 'CL_TOURISM_MEASURE',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.CFE:CL_TOURISM_MEASURE(2.0)',
            },
          ],
          version: '2.0',
          agencyID: 'OECD.CFE',
          isFinal: true,
          name: 'Measure',
          names: {
            en: 'Measure',
          },
          isPartial: true,
          codes: [
            {
              id: 'ENT',
              name: 'Enterprises',
              names: {
                en: 'Enterprises',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_MEASURE(2.0).ENT',
                },
              ],
            },
            {
              id: 'EMP',
              name: 'Employment',
              names: {
                en: 'Employment',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_TOURISM_MEASURE(2.0).EMP',
                },
              ],
            },
          ],
        },
        {
          id: 'CL_UNIT_MEASURE',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.CFE:CL_UNIT_MEASURE(1.1)',
            },
          ],
          version: '1.1',
          agencyID: 'OECD.CFE',
          isFinal: true,
          name: 'Unit of measure',
          names: {
            en: 'Unit of measure',
          },
          isPartial: true,
          codes: [
            {
              id: 'NUMBER',
              name: 'Number',
              names: {
                en: 'Number',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_UNIT_MEASURE(1.1).NUMBER',
                },
              ],
            },
            {
              id: 'PERSONS',
              name: 'Persons',
              names: {
                en: 'Persons',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.CFE:CL_UNIT_MEASURE(1.1).PERSONS',
                },
              ],
            },
          ],
        },
        {
          id: 'CL_CONF_STATUS',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_CONF_STATUS(1.2)',
            },
          ],
          version: '1.2',
          agencyID: 'SDMX',
          isFinal: true,
          name: 'Confidentiality Status',
          names: {
            en: 'Confidentiality Status',
          },
          isPartial: false,
          codes: [
            {
              id: 'F',
              name: 'Free (free for publication)',
              names: {
                en: 'Free (free for publication)',
              },
              description:
                'Used for observations without any special sensitivity considerations and which can thus be freely shared. Usually, source organisations provide information and guidance on general requirements for re-dissemination  (like mentioning the source) either on their websites or in their paper publications. In some institutional environments the term "unclassified" is used in a sense that still denotes implied restrictions in the circulation of information. If this is the case, the organisations concerned may probably consider that "free" (value F) is not the appropriate tag for this kind of "unclassified" category and that "Not for publication, restricted for internal use only" (value N) may be more appropriate.',
              descriptions: {
                en:
                  'Used for observations without any special sensitivity considerations and which can thus be freely shared. Usually, source organisations provide information and guidance on general requirements for re-dissemination  (like mentioning the source) either on their websites or in their paper publications. In some institutional environments the term "unclassified" is used in a sense that still denotes implied restrictions in the circulation of information. If this is the case, the organisations concerned may probably consider that "free" (value F) is not the appropriate tag for this kind of "unclassified" category and that "Not for publication, restricted for internal use only" (value N) may be more appropriate.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_CONF_STATUS(1.2).F',
                },
              ],
            },
            {
              id: 'N',
              name: 'Not for publication, restricted for internal use only',
              names: {
                en: 'Not for publication, restricted for internal use only',
              },
              description:
                'Used to denote observations that are restricted for internal use only within organisations. This code may be accompanied with an additional observation-level attribute: CONF_REDIST which defines the secondary recipient(s) to whom the sender allows the primary recipient to forward confidential data.',
              descriptions: {
                en:
                  'Used to denote observations that are restricted for internal use only within organisations. This code may be accompanied with an additional observation-level attribute: CONF_REDIST which defines the secondary recipient(s) to whom the sender allows the primary recipient to forward confidential data.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_CONF_STATUS(1.2).N',
                },
              ],
            },
            {
              id: 'C',
              name: 'Confidential statistical information',
              names: {
                en: 'Confidential statistical information',
              },
              description:
                'Confidential statistical information (primary confidentiality) due to identifiable respondents. Measures also should be taken to prevent not only direct access, but also indirect deduction or calculation by other users and parties, probably by considering and treating additional observations as "confidential" (secondary confidentiality management).',
              descriptions: {
                en:
                  'Confidential statistical information (primary confidentiality) due to identifiable respondents. Measures also should be taken to prevent not only direct access, but also indirect deduction or calculation by other users and parties, probably by considering and treating additional observations as "confidential" (secondary confidentiality management).',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_CONF_STATUS(1.2).C',
                },
              ],
            },
            {
              id: 'D',
              name: 'Secondary confidentiality set by the sender, not for publication',
              names: {
                en: 'Secondary confidentiality set by the sender, not for publication',
              },
              description:
                'Used by the sender of the data to flag (beyond the confidential statistical information) additional observations in the dataset so that the receiver knows that he/she should suppress these observations in subsequent stages of processing (especially dissemination) in order to prevent third parties to indirectly deduct the observations that are genuinely flagged with "C".',
              descriptions: {
                en:
                  'Used by the sender of the data to flag (beyond the confidential statistical information) additional observations in the dataset so that the receiver knows that he/she should suppress these observations in subsequent stages of processing (especially dissemination) in order to prevent third parties to indirectly deduct the observations that are genuinely flagged with "C".',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_CONF_STATUS(1.2).D',
                },
              ],
            },
            {
              id: 'S',
              name: 'Secondary confidentiality set and managed by the receiver, not for publication',
              names: {
                en: 'Secondary confidentiality set and managed by the receiver, not for publication',
              },
              description:
                'If senders do not manage the secondary confidentiality in their data and/or there are also other countries\' data involved (with the intention to eventually compile a regional-wide aggregate that is going to be published), the value "S" is used by the receiver to flag additional suppressed observations (within sender\'s data and/or within the datasets of other senders) in subsequent stages of processing (especially, dissemination) in order to prevent third parties to indirectly deduct the observations that were genuinely flagged with "C" by the sender.',
              descriptions: {
                en:
                  'If senders do not manage the secondary confidentiality in their data and/or there are also other countries\' data involved (with the intention to eventually compile a regional-wide aggregate that is going to be published), the value "S" is used by the receiver to flag additional suppressed observations (within sender\'s data and/or within the datasets of other senders) in subsequent stages of processing (especially, dissemination) in order to prevent third parties to indirectly deduct the observations that were genuinely flagged with "C" by the sender.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_CONF_STATUS(1.2).S',
                },
              ],
            },
            {
              id: 'A',
              name: 'Primary confidentiality due to small counts',
              names: {
                en: 'Primary confidentiality due to small counts',
              },
              description:
                'A cell is flagged as confidential if less than m units ("too few units") contribute to the total of that cell. The limits of what constitutes "small counts" can vary across statistical domains, countries, etc.',
              descriptions: {
                en:
                  'A cell is flagged as confidential if less than m units ("too few units") contribute to the total of that cell. The limits of what constitutes "small counts" can vary across statistical domains, countries, etc.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_CONF_STATUS(1.2).A',
                },
              ],
            },
            {
              id: 'O',
              name: 'Primary confidentiality due to dominance by one unit',
              names: {
                en: 'Primary confidentiality due to dominance by one unit',
              },
              description:
                'Used when one unit accounts for more than x % of the total of a cell. The value of x can vary across statistical domains or countries, be influenced by legislation, etc.',
              descriptions: {
                en:
                  'Used when one unit accounts for more than x % of the total of a cell. The value of x can vary across statistical domains or countries, be influenced by legislation, etc.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_CONF_STATUS(1.2).O',
                },
              ],
            },
            {
              id: 'T',
              name: 'Primary confidentiality due to dominance by two units',
              names: {
                en: 'Primary confidentiality due to dominance by two units',
              },
              description:
                'Used when two units account for more than x % of the total of a cell. The value of x can vary across statistical domains or countries, be influenced by legislation, etc.',
              descriptions: {
                en:
                  'Used when two units account for more than x % of the total of a cell. The value of x can vary across statistical domains or countries, be influenced by legislation, etc.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_CONF_STATUS(1.2).T',
                },
              ],
            },
            {
              id: 'G',
              name: 'Primary confidentiality due to dominance by one or two units',
              names: {
                en: 'Primary confidentiality due to dominance by one or two units',
              },
              description:
                'Used when one or two units account(s) for more than x % of the total of a cell. The value of x can vary across statistical domains or countries, be influenced by legislation, etc.',
              descriptions: {
                en:
                  'Used when one or two units account(s) for more than x % of the total of a cell. The value of x can vary across statistical domains or countries, be influenced by legislation, etc.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_CONF_STATUS(1.2).G',
                },
              ],
            },
            {
              id: 'M',
              name:
                'Primary confidentiality due to data declared confidential based on other measures of concentration',
              names: {
                en:
                  'Primary confidentiality due to data declared confidential based on other measures of concentration',
              },
              description:
                'Cells declared confidential using mathematical definitions of sensitive cells, e.g. p-percent, p/q or (n,k) rules.',
              descriptions: {
                en:
                  'Cells declared confidential using mathematical definitions of sensitive cells, e.g. p-percent, p/q or (n,k) rules.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_CONF_STATUS(1.2).M',
                },
              ],
            },
          ],
        },
        {
          id: 'CL_DECIMALS',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_DECIMALS(1.0)',
            },
          ],
          version: '1.0',
          agencyID: 'SDMX',
          isFinal: true,
          name: 'Decimals',
          names: {
            en: 'Decimals',
          },
          description:
            'Cette liste de codes fournit une liste de valeurs indiquant le nombre de chiffres décimaux utilisés dans les données. Cette liste de codes a été publiée en 2009. De plus amples informations sur cette liste de codes et sur les listes de codes SDMX en général (par exemple, la liste des codes génériques pour exprimer des concepts généraux comme "Total", "Inconnu", etc. ; les syntaxes pour la création d\'autres codes ; les lignes directrices générales pour la création de listes de codes SDMX) peuvent être consultées à l\'adresse suivante : https://sdmx.org/?page_id=4345.',
          descriptions: {
            en:
              'Cette liste de codes fournit une liste de valeurs indiquant le nombre de chiffres décimaux utilisés dans les données. Cette liste de codes a été publiée en 2009. De plus amples informations sur cette liste de codes et sur les listes de codes SDMX en général (par exemple, la liste des codes génériques pour exprimer des concepts généraux comme "Total", "Inconnu", etc. ; les syntaxes pour la création d\'autres codes ; les lignes directrices générales pour la création de listes de codes SDMX) peuvent être consultées à l\'adresse suivante : https://sdmx.org/?page_id=4345.',
          },
          isPartial: false,
          codes: [
            {
              id: '0',
              name: 'Zero',
              names: {
                en: 'Zero',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).0',
                },
              ],
            },
            {
              id: '1',
              name: 'One',
              names: {
                en: 'One',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).1',
                },
              ],
            },
            {
              id: '2',
              name: 'Two',
              names: {
                en: 'Two',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).2',
                },
              ],
            },
            {
              id: '3',
              name: 'Three',
              names: {
                en: 'Three',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).3',
                },
              ],
            },
            {
              id: '4',
              name: 'Four',
              names: {
                en: 'Four',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).4',
                },
              ],
            },
            {
              id: '5',
              name: 'Five',
              names: {
                en: 'Five',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).5',
                },
              ],
            },
            {
              id: '6',
              name: 'Six',
              names: {
                en: 'Six',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).6',
                },
              ],
            },
            {
              id: '7',
              name: 'Seven',
              names: {
                en: 'Seven',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).7',
                },
              ],
            },
            {
              id: '8',
              name: 'Eight',
              names: {
                en: 'Eight',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).8',
                },
              ],
            },
            {
              id: '9',
              name: 'Nine',
              names: {
                en: 'Nine',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).9',
                },
              ],
            },
            {
              id: '10',
              name: 'Ten',
              names: {
                en: 'Ten',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).10',
                },
              ],
            },
            {
              id: '11',
              name: 'Eleven',
              names: {
                en: 'Eleven',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).11',
                },
              ],
            },
            {
              id: '12',
              name: 'Twelve',
              names: {
                en: 'Twelve',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).12',
                },
              ],
            },
            {
              id: '13',
              name: 'Thirteen',
              names: {
                en: 'Thirteen',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).13',
                },
              ],
            },
            {
              id: '14',
              name: 'Fourteen',
              names: {
                en: 'Fourteen',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).14',
                },
              ],
            },
            {
              id: '15',
              name: 'Fifteen',
              names: {
                en: 'Fifteen',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_DECIMALS(1.0).15',
                },
              ],
            },
          ],
        },
        {
          id: 'CL_FREQ',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_FREQ(2.0)',
            },
          ],
          version: '2.0',
          agencyID: 'SDMX',
          isFinal: true,
          name: 'Frequency',
          names: {
            en: 'Frequency',
          },
          description:
            'This code list provides a set of values indicating the "frequency" of the data (e.g. weekly, monthly, quarterly). The concept “frequency” may refer to various stages in the production process, e.g. data collection or data dissemination. For example, a time series could be disseminated at annual frequency but the underlying data are compiled monthly. The code list is applicable for all different uses of "frequency". This code list was formally adopted on 4 December 2013. More information about and supporting material for this code list and SDMX code lists in general (e.g. list of generic codes for expressing general concepts like "Total", "Unknown", etc.; syntaxes for the creation of further codes; general guidelines for the creation of SDMX code lists) can be found at this address: https://sdmx.org/?page_id=4345.',
          descriptions: {
            en:
              'This code list provides a set of values indicating the "frequency" of the data (e.g. weekly, monthly, quarterly). The concept “frequency” may refer to various stages in the production process, e.g. data collection or data dissemination. For example, a time series could be disseminated at annual frequency but the underlying data are compiled monthly. The code list is applicable for all different uses of "frequency". This code list was formally adopted on 4 December 2013. More information about and supporting material for this code list and SDMX code lists in general (e.g. list of generic codes for expressing general concepts like "Total", "Unknown", etc.; syntaxes for the creation of further codes; general guidelines for the creation of SDMX code lists) can be found at this address: https://sdmx.org/?page_id=4345.',
          },
          isPartial: false,
          codes: [
            {
              id: 'A',
              name: 'Annual',
              names: {
                en: 'Annual',
              },
              description: 'To be used for data collected or disseminated every year.',
              descriptions: {
                en: 'To be used for data collected or disseminated every year.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_FREQ(2.0).A',
                },
              ],
            },
            {
              id: 'S',
              name: 'Half-yearly, semester',
              names: {
                en: 'Half-yearly, semester',
              },
              description: 'To be used for data collected or disseminated every semester.',
              descriptions: {
                en: 'To be used for data collected or disseminated every semester.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_FREQ(2.0).S',
                },
              ],
            },
            {
              id: 'Q',
              name: 'Quarterly',
              names: {
                en: 'Quarterly',
              },
              description: 'To be used for data collected or disseminated every quarter.',
              descriptions: {
                en: 'To be used for data collected or disseminated every quarter.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_FREQ(2.0).Q',
                },
              ],
            },
            {
              id: 'M',
              name: 'Monthly',
              names: {
                en: 'Monthly',
              },
              description: 'To be used for data collected or disseminated every month.',
              descriptions: {
                en: 'To be used for data collected or disseminated every month.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_FREQ(2.0).M',
                },
              ],
            },
            {
              id: 'W',
              name: 'Weekly',
              names: {
                en: 'Weekly',
              },
              description: 'To be used for data collected or disseminated every week.',
              descriptions: {
                en: 'To be used for data collected or disseminated every week.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_FREQ(2.0).W',
                },
              ],
            },
            {
              id: 'D',
              name: 'Daily',
              names: {
                en: 'Daily',
              },
              description: 'To be used for data collected or disseminated every day.',
              descriptions: {
                en: 'To be used for data collected or disseminated every day.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_FREQ(2.0).D',
                },
              ],
            },
            {
              id: 'H',
              name: 'Hourly',
              names: {
                en: 'Hourly',
              },
              description: 'To be used for data collected or disseminated every hour.',
              descriptions: {
                en: 'To be used for data collected or disseminated every hour.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_FREQ(2.0).H',
                },
              ],
            },
            {
              id: 'B',
              name: 'Daily – businessweek',
              names: {
                en: 'Daily – businessweek',
              },
              description:
                'Similar to "daily", however there are no observations for Saturdays and Sundays (so, neither "missing values" nor "numeric values" should be provided for Saturday and Sunday). This treatment  ("business") is one way to deal with such cases, but it is not the only option. Such a time series could alternatively be considered daily ("D"), thus, with missing values in the weekend.',
              descriptions: {
                en:
                  'Similar to "daily", however there are no observations for Saturdays and Sundays (so, neither "missing values" nor "numeric values" should be provided for Saturday and Sunday). This treatment  ("business") is one way to deal with such cases, but it is not the only option. Such a time series could alternatively be considered daily ("D"), thus, with missing values in the weekend.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_FREQ(2.0).B',
                },
              ],
            },
            {
              id: 'N',
              name: 'Minutely',
              names: {
                en: 'Minutely',
              },
              description:
                'While N denotes "minutely", usually, there may be no observations every minute (for several series the frequency is usually "irregular" within a day/days). And though observations may be sparse (not collected or disseminated every minute), missing values do not need to be given for the minutes when no observations exist: in any case the time stamp determines when an observation is observed.',
              descriptions: {
                en:
                  'While N denotes "minutely", usually, there may be no observations every minute (for several series the frequency is usually "irregular" within a day/days). And though observations may be sparse (not collected or disseminated every minute), missing values do not need to be given for the minutes when no observations exist: in any case the time stamp determines when an observation is observed.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_FREQ(2.0).N',
                },
              ],
            },
          ],
        },
        {
          id: 'CL_OBS_STATUS',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_OBS_STATUS(2.2)',
            },
          ],
          version: '2.2',
          agencyID: 'SDMX',
          isFinal: true,
          name: 'Observation status',
          names: {
            en: 'Observation status',
          },
          isPartial: false,
          codes: [
            {
              id: 'A',
              name: 'Normal value',
              names: {
                en: 'Normal value',
              },
              description:
                'To be used as default value if no value is provided or when no special coded qualification is assumed. Usually, it can be assumed that the source agency assigns sufficient confidence to the provided observation and/or the value is not expected to be dramatically revised.',
              descriptions: {
                en:
                  'To be used as default value if no value is provided or when no special coded qualification is assumed. Usually, it can be assumed that the source agency assigns sufficient confidence to the provided observation and/or the value is not expected to be dramatically revised.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).A',
                },
              ],
            },
            {
              id: 'B',
              name: 'Time series break',
              names: {
                en: 'Time series break',
              },
              description:
                'Observations are characterised as such when different content exists or a different methodology has been applied to this observation as compared with the preceding one (the one given for the previous period).',
              descriptions: {
                en:
                  'Observations are characterised as such when different content exists or a different methodology has been applied to this observation as compared with the preceding one (the one given for the previous period).',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).B',
                },
              ],
            },
            {
              id: 'D',
              name: 'Definition differs',
              names: {
                en: 'Definition differs',
              },
              description:
                'Used to indicate slight deviations from the established methodology (footnote-type information); these divergences do not imply a break in time series.',
              descriptions: {
                en:
                  'Used to indicate slight deviations from the established methodology (footnote-type information); these divergences do not imply a break in time series.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).D',
                },
              ],
            },
            {
              id: 'E',
              name: 'Estimated value',
              names: {
                en: 'Estimated value',
              },
              description:
                'Observation obtained through an estimation methodology (e.g. to produce back-casts) or based on the use of a limited amount of data or ad hoc sampling and through additional calculations (e.g. to produce a value at an early stage of the production stage while not all data are available). It may also be used in case of experimental data (e.g. in the context of a pilot ahead of a full scale production process) or in case of data of (anticipated/assessed) low quality. If needed, additional information can be provided through free text using the COMMENT_OBS attribute at the observation level or at a higher level. This code is to be used when the estimation is done by a sender agency. When the imputation is carried out by a receiver agency in order to replace or fill gaps in reported data series, the flag to use is I "Value imputed by a receiving agency".',
              descriptions: {
                en:
                  'Observation obtained through an estimation methodology (e.g. to produce back-casts) or based on the use of a limited amount of data or ad hoc sampling and through additional calculations (e.g. to produce a value at an early stage of the production stage while not all data are available). It may also be used in case of experimental data (e.g. in the context of a pilot ahead of a full scale production process) or in case of data of (anticipated/assessed) low quality. If needed, additional information can be provided through free text using the COMMENT_OBS attribute at the observation level or at a higher level. This code is to be used when the estimation is done by a sender agency. When the imputation is carried out by a receiver agency in order to replace or fill gaps in reported data series, the flag to use is I "Value imputed by a receiving agency".',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).E',
                },
              ],
            },
            {
              id: 'F',
              name: 'Forecast value',
              names: {
                en: 'Forecast value',
              },
              description:
                'Value deemed to assess the magnitude which a quantity will assume at some future point of time (as distinct from "estimated value" which attempts to assess the magnitude of an already existent quantity).',
              descriptions: {
                en:
                  'Value deemed to assess the magnitude which a quantity will assume at some future point of time (as distinct from "estimated value" which attempts to assess the magnitude of an already existent quantity).',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).F',
                },
              ],
            },
            {
              id: 'G',
              name: 'Experimental value',
              names: {
                en: 'Experimental value',
              },
              description:
                'Data collected on the basis of definitions or (alternative) collection methods under development. Data not of guaranteed quality as normally expected from provider.',
              descriptions: {
                en:
                  'Data collected on the basis of definitions or (alternative) collection methods under development. Data not of guaranteed quality as normally expected from provider.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).G',
                },
              ],
            },
            {
              id: 'I',
              name: 'Value imputed by a receiving agency',
              names: {
                en: 'Value imputed by a receiving agency',
              },
              description:
                'Observation imputed by a receiving agency to replace or fill gaps in reported data series. This code is intended to cover all cases where a receiving agency publishes data about a sending agency that do not come from an official source in the sender agency\'s reporting framework. When the estimation is done by the sender agency, the flag to use is E "Estimated value".',
              descriptions: {
                en:
                  'Observation imputed by a receiving agency to replace or fill gaps in reported data series. This code is intended to cover all cases where a receiving agency publishes data about a sending agency that do not come from an official source in the sender agency\'s reporting framework. When the estimation is done by the sender agency, the flag to use is E "Estimated value".',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).I',
                },
              ],
            },
            {
              id: 'K',
              name: 'Data included in another category',
              names: {
                en: 'Data included in another category',
              },
              description:
                'This code is used when data for a given category are missing and are included in another category, sub-total or total. Generally where code "K" is used there should be a corresponding code "W - Includes data from another category" assigned to the over-covered category. Implementers and data reporters should use the COMMENT_OBS observation-level attribute to specify under which category the data are included.',
              descriptions: {
                en:
                  'This code is used when data for a given category are missing and are included in another category, sub-total or total. Generally where code "K" is used there should be a corresponding code "W - Includes data from another category" assigned to the over-covered category. Implementers and data reporters should use the COMMENT_OBS observation-level attribute to specify under which category the data are included.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).K',
                },
              ],
            },
            {
              id: 'W',
              name: 'Includes data from another category',
              names: {
                en: 'Includes data from another category',
              },
              description:
                'This code is used when data include another category, or go beyond the scope of the data collection and are therefore over-covered. Generally, where code "W" is used there should be a corresponding code "K - Data included in another category" assigned to the category which is under-covered. Implementers and data reporters should use the COMMENT_OBS observation-level attribute to specify which additional data are included.',
              descriptions: {
                en:
                  'This code is used when data include another category, or go beyond the scope of the data collection and are therefore over-covered. Generally, where code "W" is used there should be a corresponding code "K - Data included in another category" assigned to the category which is under-covered. Implementers and data reporters should use the COMMENT_OBS observation-level attribute to specify which additional data are included.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).W',
                },
              ],
            },
            {
              id: 'O',
              name: 'Missing value',
              names: {
                en: 'Missing value',
              },
              description:
                'This code is to be used when no breakdown is made between the reasons why data are missing. Data can be missing due to many reasons: data cannot exist, data exist but are not collected (e.g. because they are below a certain threshold or subject to a derogation clause), data are unreliable, etc.',
              descriptions: {
                en:
                  'This code is to be used when no breakdown is made between the reasons why data are missing. Data can be missing due to many reasons: data cannot exist, data exist but are not collected (e.g. because they are below a certain threshold or subject to a derogation clause), data are unreliable, etc.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).O',
                },
              ],
            },
            {
              id: 'M',
              name: 'Missing value; data cannot exist',
              names: {
                en: 'Missing value; data cannot exist',
              },
              description:
                "Used to denote empty cells resulting from the impossibility to collect a statistical value (e.g. a particular education level or type of institution may be not applicable to a given country's education system).",
              descriptions: {
                en:
                  "Used to denote empty cells resulting from the impossibility to collect a statistical value (e.g. a particular education level or type of institution may be not applicable to a given country's education system).",
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).M',
                },
              ],
            },
            {
              id: 'P',
              name: 'Provisional value',
              names: {
                en: 'Provisional value',
              },
              description:
                'An observation is characterised as "provisional" when the source agency - while it bases its calculations on its standard production methodology - considers that the data, almost certainly, are expected to be revised.',
              descriptions: {
                en:
                  'An observation is characterised as "provisional" when the source agency - while it bases its calculations on its standard production methodology - considers that the data, almost certainly, are expected to be revised.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).P',
                },
              ],
            },
            {
              id: 'S',
              name: 'Strike and other special events',
              names: {
                en: 'Strike and other special events',
              },
              description: 'Special circumstances (e.g. strike) affecting the observation or causing a missing value.',
              descriptions: {
                en: 'Special circumstances (e.g. strike) affecting the observation or causing a missing value.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).S',
                },
              ],
            },
            {
              id: 'L',
              name: 'Missing value; data exist but were not collected',
              names: {
                en: 'Missing value; data exist but were not collected',
              },
              description:
                'Used, for example, when some data are not reported/disseminated because they are below a certain threshold.',
              descriptions: {
                en:
                  'Used, for example, when some data are not reported/disseminated because they are below a certain threshold.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).L',
                },
              ],
            },
            {
              id: 'H',
              name: 'Missing value; holiday or weekend',
              names: {
                en: 'Missing value; holiday or weekend',
              },
              description: 'Used in some daily data flows.',
              descriptions: {
                en: 'Used in some daily data flows.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).H',
                },
              ],
            },
            {
              id: 'Q',
              name: 'Missing value; suppressed',
              names: {
                en: 'Missing value; suppressed',
              },
              description:
                'Used, for example, when data are suppressed due to statistical confidentiality considerations.',
              descriptions: {
                en: 'Used, for example, when data are suppressed due to statistical confidentiality considerations.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).Q',
                },
              ],
            },
            {
              id: 'J',
              name: 'Derogation',
              names: {
                en: 'Derogation',
              },
              description:
                "Clause in an agreement (e.g. legal act, gentlemen's agreement) stating that some provisions in the agreement are not to be implemented by designated parties; these derogations may affect the observation or cause a missing value. In general, derogations are limited in time.",
              descriptions: {
                en:
                  "Clause in an agreement (e.g. legal act, gentlemen's agreement) stating that some provisions in the agreement are not to be implemented by designated parties; these derogations may affect the observation or cause a missing value. In general, derogations are limited in time.",
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).J',
                },
              ],
            },
            {
              id: 'N',
              name: 'Not significant',
              names: {
                en: 'Not significant',
              },
              description:
                'Used to indicate a value which is not a "real" zero (e.g. a result of 0.0004 rounded to zero).',
              descriptions: {
                en: 'Used to indicate a value which is not a "real" zero (e.g. a result of 0.0004 rounded to zero).',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).N',
                },
              ],
            },
            {
              id: 'U',
              name: 'Low reliability',
              names: {
                en: 'Low reliability',
              },
              description:
                'This indicates existing observations, but for which the user should also be aware of the low quality assigned.',
              descriptions: {
                en:
                  'This indicates existing observations, but for which the user should also be aware of the low quality assigned.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).U',
                },
              ],
            },
            {
              id: 'V',
              name: 'Unvalidated value',
              names: {
                en: 'Unvalidated value',
              },
              description: 'Observation as received from the respondent without further evaluation of data quality.',
              descriptions: {
                en: 'Observation as received from the respondent without further evaluation of data quality.',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.2).V',
                },
              ],
            },
          ],
        },
        {
          id: 'CL_UNIT_MULT',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_UNIT_MULT(1.2)',
            },
          ],
          version: '1.2',
          agencyID: 'SDMX',
          isFinal: true,
          name: 'Unit Multiplier',
          names: {
            en: 'Unit Multiplier',
          },
          isPartial: false,
          codes: [
            {
              id: '0',
              name: 'Units',
              names: {
                en: 'Units',
              },
              description: 'In scientific notation, expressed as ten raised to the power of zero (10^0)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of zero (10^0)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).0',
                },
              ],
            },
            {
              id: '1',
              name: 'Tens',
              names: {
                en: 'Tens',
              },
              description: 'In scientific notation, expressed as ten raised to the power of one (10^1)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of one (10^1)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).1',
                },
              ],
            },
            {
              id: '2',
              name: 'Hundreds',
              names: {
                en: 'Hundreds',
              },
              description: 'In scientific notation, expressed as ten raised to the power of two (10^2)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of two (10^2)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).2',
                },
              ],
            },
            {
              id: '3',
              name: 'Thousands',
              names: {
                en: 'Thousands',
              },
              description: 'In scientific notation, expressed as ten raised to the power of three (10^3)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of three (10^3)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).3',
                },
              ],
            },
            {
              id: '4',
              name: 'Tens of thousands',
              names: {
                en: 'Tens of thousands',
              },
              description: 'In scientific notation, expressed as ten raised to the power of four (10^4)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of four (10^4)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).4',
                },
              ],
            },
            {
              id: '5',
              name: 'Hundreds of thousands',
              names: {
                en: 'Hundreds of thousands',
              },
              description: 'In scientific notation, expressed as ten raised to the power of five(10^5)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of five(10^5)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).5',
                },
              ],
            },
            {
              id: '6',
              name: 'Millions',
              names: {
                en: 'Millions',
              },
              description: 'In scientific notation, expressed as ten raised to the power of six (10^6)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of six (10^6)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).6',
                },
              ],
            },
            {
              id: '7',
              name: 'Tens of millions',
              names: {
                en: 'Tens of millions',
              },
              description: 'In scientific notation, expressed as ten raised to the power of seven (10^7)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of seven (10^7)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).7',
                },
              ],
            },
            {
              id: '8',
              name: 'Hundreds of millions',
              names: {
                en: 'Hundreds of millions',
              },
              description: 'In scientific notation, expressed as ten raised to the power of eight (10^8)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of eight (10^8)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).8',
                },
              ],
            },
            {
              id: '9',
              name: 'Billions',
              names: {
                en: 'Billions',
              },
              description: 'In scientific notation, expressed as ten raised to the power of nine (10^9)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of nine (10^9)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).9',
                },
              ],
            },
            {
              id: '10',
              name: 'Tens of billions',
              names: {
                en: 'Tens of billions',
              },
              description: 'In scientific notation, expressed as ten raised to the power of ten (10^10)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of ten (10^10)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).10',
                },
              ],
            },
            {
              id: '11',
              name: 'Hundreds of billions',
              names: {
                en: 'Hundreds of billions',
              },
              description: 'In scientific notation, expressed as ten raised to the power of eleven (10^11)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of eleven (10^11)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).11',
                },
              ],
            },
            {
              id: '12',
              name: 'Trillions',
              names: {
                en: 'Trillions',
              },
              description: 'In scientific notation, expressed as ten raised to the power of twelve (10^12)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of twelve (10^12)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).12',
                },
              ],
            },
            {
              id: '13',
              name: 'Tens of trillions',
              names: {
                en: 'Tens of trillions',
              },
              description: 'In scientific notation, expressed as ten raised to the power of thirteen (10^13)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of thirteen (10^13)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).13',
                },
              ],
            },
            {
              id: '14',
              name: 'Hundreds of trillions',
              names: {
                en: 'Hundreds of trillions',
              },
              description: 'In scientific notation, expressed as ten raised to the power of fourteen (10^14)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of fourteen (10^14)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).14',
                },
              ],
            },
            {
              id: '15',
              name: 'Quadrillions',
              names: {
                en: 'Quadrillions',
              },
              description: 'In scientific notation, expressed as ten raised to the power of fifteen (10^15)',
              descriptions: {
                en: 'In scientific notation, expressed as ten raised to the power of fifteen (10^15)',
              },
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.2).15',
                },
              ],
            },
          ],
        },
      ],
      dataStructures: [
        {
          id: 'DSD_KEY_TOURISM',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_KEY_TOURISM(5.0)',
            },
          ],
          version: '5.0',
          agencyID: 'OECD.CFE',
          isFinal: true,
          name: 'DSD for tourism',
          names: {
            en: 'DSD for tourism',
          },
          dataStructureComponents: {
            attributeList: {
              id: 'AttributeDescriptor',
              links: [
                {
                  rel: 'self',
                  urn:
                    'urn:sdmx:org.sdmx.infomodel.datastructure.AttributeDescriptor=OECD.CFE:DSD_KEY_TOURISM(5.0).AttributeDescriptor',
                },
              ],
              attributes: [
                {
                  id: 'UNIT_MULT',
                  links: [
                    {
                      rel: 'self',
                      urn:
                        'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=OECD.CFE:DSD_KEY_TOURISM(5.0).UNIT_MULT',
                    },
                  ],
                  conceptIdentity:
                    'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).UNIT_MULT',
                  localRepresentation: {
                    enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_UNIT_MULT(1.2)',
                  },
                  assignmentStatus: 'Conditional',
                  attributeRelationship: {
                    primaryMeasure: 'OBS_VALUE',
                  },
                },
                {
                  id: 'OBS_STATUS',
                  links: [
                    {
                      rel: 'self',
                      urn:
                        'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=OECD.CFE:DSD_KEY_TOURISM(5.0).OBS_STATUS',
                    },
                  ],
                  conceptIdentity:
                    'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).OBS_STATUS',
                  localRepresentation: {
                    enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_OBS_STATUS(2.2)',
                  },
                  assignmentStatus: 'Conditional',
                  attributeRelationship: {
                    primaryMeasure: 'OBS_VALUE',
                  },
                },
                {
                  id: 'CONF_STATUS',
                  links: [
                    {
                      rel: 'self',
                      urn:
                        'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=OECD.CFE:DSD_KEY_TOURISM(5.0).CONF_STATUS',
                    },
                  ],
                  conceptIdentity:
                    'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).CONF_STATUS',
                  localRepresentation: {
                    enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_CONF_STATUS(1.2)',
                  },
                  assignmentStatus: 'Conditional',
                  attributeRelationship: {
                    primaryMeasure: 'OBS_VALUE',
                  },
                },
                {
                  id: 'DECIMALS',
                  links: [
                    {
                      rel: 'self',
                      urn:
                        'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=OECD.CFE:DSD_KEY_TOURISM(5.0).DECIMALS',
                    },
                  ],
                  conceptIdentity:
                    'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).DECIMALS',
                  localRepresentation: {
                    enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_DECIMALS(1.0)',
                  },
                  assignmentStatus: 'Conditional',
                  attributeRelationship: {
                    primaryMeasure: 'OBS_VALUE',
                  },
                },
              ],
            },
            dimensionList: {
              id: 'DimensionDescriptor',
              links: [
                {
                  rel: 'self',
                  urn:
                    'urn:sdmx:org.sdmx.infomodel.datastructure.DimensionDescriptor=OECD.CFE:DSD_KEY_TOURISM(5.0).DimensionDescriptor',
                },
              ],
              dimensions: [
                {
                  id: 'REF_AREA',
                  links: [
                    {
                      rel: 'self',
                      urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.CFE:DSD_KEY_TOURISM(5.0).REF_AREA',
                    },
                  ],
                  position: 0,
                  type: 'Dimension',
                  conceptIdentity:
                    'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).REF_AREA',
                  localRepresentation: {
                    enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.CFE:CL_AREA(1.2)',
                  },
                },
                {
                  id: 'MEASURE',
                  links: [
                    {
                      rel: 'self',
                      urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.CFE:DSD_KEY_TOURISM(5.0).MEASURE',
                    },
                  ],
                  position: 1,
                  type: 'Dimension',
                  conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).MEASURE',
                  localRepresentation: {
                    enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.CFE:CL_TOURISM_MEASURE(2.0)',
                  },
                },
                {
                  id: 'ACTIVITY',
                  links: [
                    {
                      rel: 'self',
                      urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.CFE:DSD_KEY_TOURISM(5.0).ACTIVITY',
                    },
                  ],
                  position: 2,
                  type: 'Dimension',
                  conceptIdentity:
                    'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).ACTIVITY',
                  localRepresentation: {
                    enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.CFE:CL_TOURISM_ACTIVITY(1.0)',
                  },
                },
                {
                  id: 'UNIT_MEASURE',
                  links: [
                    {
                      rel: 'self',
                      urn:
                        'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.CFE:DSD_KEY_TOURISM(5.0).UNIT_MEASURE',
                    },
                  ],
                  position: 3,
                  type: 'Dimension',
                  conceptIdentity:
                    'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).UNIT_MEASURE',
                  localRepresentation: {
                    enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.CFE:CL_UNIT_MEASURE(1.1)',
                  },
                },
                {
                  id: 'FREQ',
                  links: [
                    {
                      rel: 'self',
                      urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.CFE:DSD_KEY_TOURISM(5.0).FREQ',
                    },
                  ],
                  position: 4,
                  type: 'Dimension',
                  conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).FREQ',
                  localRepresentation: {
                    enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_FREQ(2.0)',
                  },
                },
              ],
              timeDimensions: [
                {
                  id: 'TIME_PERIOD',
                  links: [
                    {
                      rel: 'self',
                      urn:
                        'urn:sdmx:org.sdmx.infomodel.datastructure.TimeDimension=OECD.CFE:DSD_KEY_TOURISM(5.0).TIME_PERIOD',
                    },
                  ],
                  position: 5,
                  type: 'TimeDimension',
                  conceptIdentity:
                    'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).TIME_PERIOD',
                  localRepresentation: {
                    textFormat: {
                      textType: 'ObservationalTimePeriod',
                      isSequence: false,
                      isMultiLingual: false,
                    },
                  },
                },
              ],
            },
            measureList: {
              id: 'MeasureDescriptor',
              links: [
                {
                  rel: 'self',
                  urn:
                    'urn:sdmx:org.sdmx.infomodel.datastructure.MeasureDescriptor=OECD.CFE:DSD_KEY_TOURISM(5.0).MeasureDescriptor',
                },
              ],
              primaryMeasure: {
                id: 'OBS_VALUE',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.PrimaryMeasure=OECD.CFE:DSD_KEY_TOURISM(5.0).OBS_VALUE',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.CFE:CS_TOURISM(5.0).OBS_VALUE',
              },
            },
          },
        },
      ],
      contentConstraints: [
        {
          id: 'CR_A_DF_TOURISM_ENT_EMP',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.registry.ContentConstraint=OECD.CFE:CR_A_DF_TOURISM_ENT_EMP(5.0)',
            },
          ],
          version: '5.0',
          agencyID: 'OECD.CFE',
          isFinal: false,
          name: 'Availability (A) for DF_TOURISM_ENT_EMP',
          names: {
            en: 'Availability (A) for DF_TOURISM_ENT_EMP',
          },
          annotations: [
            {
              title: 'A',
              type: 'ReleaseVersion',
            },
          ],
          type: 'Actual',
          constraintAttachment: {
            dataflows: ['urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.CFE:DF_TOURISM_ENT_EMP(5.0)'],
          },
          cubeRegions: [
            {
              isIncluded: true,
              keyValues: [
                {
                  id: 'REF_AREA',
                  values: [
                    'AR',
                    'AT',
                    'AU',
                    'BE',
                    'BE1',
                    'BE2',
                    'BE3',
                    'BG',
                    'BR',
                    'CA',
                    'CH',
                    'CL',
                    'CN',
                    'CO',
                    'CR',
                    'CZ',
                    'DE',
                    'DK',
                    'EE',
                    'EG',
                    'ES',
                    'FI',
                    'FR',
                    'GB',
                    'GR',
                    'HR',
                    'HU',
                    'ID',
                    'IE',
                    'IL',
                    'IN',
                    'IS',
                    'IT',
                    'JP',
                    'KR',
                    'KZ',
                    'LT',
                    'LU',
                    'LV',
                    'MA',
                    'MT',
                    'MX',
                    'NL',
                    'NO',
                    'NZ',
                    'PE',
                    'PH',
                    'PL',
                    'PT',
                    'RO',
                    'RS',
                    'RU',
                    'SE',
                    'SI',
                    'SK',
                    'TR',
                    'US',
                    'ZA',
                  ],
                },
                {
                  id: 'MEASURE',
                  values: ['EMP', 'ENT'],
                },
                {
                  id: 'ACTIVITY',
                  values: [
                    '_O',
                    'ACCOM_SVCS',
                    'ACCOM_SVCS_HOTELS',
                    'AGG_TOUR',
                    'CULTURE',
                    'EQUIP_RENTAL',
                    'FOOD_BEV',
                    'IND',
                    'OTHER_COUNTRY_SPEC',
                    'RETAIL',
                    'SPORTS_REC',
                    'SUPPORT_SVC',
                    'TRANSPORT',
                    'TRANSPORT_AIR',
                    'TRANSPORT_RAIL',
                    'TRANSPORT_ROAD',
                    'TRANSPORT_WATER',
                    'TRVL_AGENT',
                  ],
                },
                {
                  id: 'UNIT_MEASURE',
                  values: ['NUMBER', 'PERSONS'],
                },
                {
                  id: 'FREQ',
                  values: ['A'],
                },
                {
                  id: 'TIME_PERIOD',
                  timeRange: {
                    startPeriod: {
                      period: '2008-01-01T00:00:00Z',
                      isInclusive: true,
                    },
                    endPeriod: {
                      period: '2018-12-31T23:59:59Z',
                      isInclusive: true,
                    },
                  },
                },
              ],
            },
          ],
        },
        {
          id: 'CR_DF_TOURISM_ENT_EMP',
          links: [
            {
              rel: 'self',
              urn: 'urn:sdmx:org.sdmx.infomodel.registry.ContentConstraint=OECD.CFE:CR_DF_TOURISM_ENT_EMP(1.0)',
            },
          ],
          version: '1.0',
          agencyID: 'OECD.CFE',
          isFinal: false,
          name: 'OECD.CFE:CR_DF_TOURISM_ENT_EMP(1.0)',
          names: {
            en: 'OECD.CFE:CR_DF_TOURISM_ENT_EMP(1.0)',
          },
          type: 'Allowed',
          constraintAttachment: {
            dataflows: ['urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.CFE:DF_TOURISM_ENT_EMP(5.0)'],
          },
          cubeRegions: [
            {
              isIncluded: true,
              keyValues: [
                {
                  id: 'MEASURE',
                  values: ['EMP', 'ENT'],
                },
                {
                  id: 'UNIT_MEASURE',
                  values: ['NUMBER', 'PERSONS'],
                },
                {
                  id: 'ACTIVITY',
                  values: [
                    '_O',
                    'ACCOM_SVCS',
                    'ACCOM_SVCS_HOTELS',
                    'AGG_TOUR',
                    'CULTURE',
                    'EQUIP_RENTAL',
                    'FOOD_BEV',
                    'IND',
                    'OTHER_COUNTRY_SPEC',
                    'RETAIL',
                    'SPORTS_REC',
                    'SUPPORT_SVC',
                    'TRANSPORT',
                    'TRANSPORT_AIR',
                    'TRANSPORT_RAIL',
                    'TRANSPORT_ROAD',
                    'TRANSPORT_WATER',
                    'TRVL_AGENT',
                  ],
                },
              ],
            },
          ],
        },
      ],
    },
    meta: {
      schema:
        'https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/structure-message/tools/schemas/1.0/sdmx-json-structure-schema.json',
      contentLanguages: ['en'],
      id: 'IDREF4951',
      prepared: '2020-10-15T15:32:07.5785637Z',
      test: false,
      sender: {
        id: 'Unknown',
      },
      receiver: [
        {
          id: 'Unknown',
        },
      ],
    },
  }
  if (order || order === 0) {
    if (typeof order === 'number') res.data.dataflows[0].annotations.push({ type: 'SEARCH_WEIGHT', title: `${order}` })
    else res.data.dataflows[0].annotations.push({ type: 'SEARCH_WEIGHT', texts: order })
  }

  return res
}
