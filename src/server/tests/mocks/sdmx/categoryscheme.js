/*
 * GET http://nsi-stable-siscc.redpelicans.com/rest/categoryscheme/OECD/OECDCS1/1.0/?references=dataflow
 * Accept: application/vnd.sdmx.structure+json;version=1.0;urn=true
 */

module.exports = {
  data: {
    dataflows: [
      {
        id: 'DF_SDG_ALL_SDG_A871_SEX_AGE_RT',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ILO:DF_SDG_ALL_SDG_A871_SEX_AGE_RT(1.0)',
          },
          {
            href: 'https://ilo.org/sdmx-test/rest/dataflow/ILO/DF_SDG_ALL_SDG_A871_SEX_AGE_RT/1.0',
            rel: 'external',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ILO:DF_SDG_ALL_SDG_A871_SEX_AGE_RT(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: true,
        isFinal: true,
        name: 'SDG indicator 8.7.1 - Proportion of children engaged in economic activity',
        names: {
          en: 'SDG indicator 8.7.1 - Proportion of children engaged in economic activity',
        },
        description:
          'Estimates on economic activity among children aged 5-17 refer to: (a) children 5 and 11 years old who, during the reference week, did at least one hour of economic activity, (b) children 12 and 14 years old who, during the reference week, did at least 14 hours of economic activity, (c) children 15 and 17 years old who, during the reference week, did at least 43 hours of economic activity.',
        descriptions: {
          en:
            'Estimates on economic activity among children aged 5-17 refer to: (a) children 5 and 11 years old who, during the reference week, did at least one hour of economic activity, (b) children 12 and 14 years old who, during the reference week, did at least 14 hours of economic activity, (c) children 15 and 17 years old who, during the reference week, did at least 43 hours of economic activity.',
        },
      },
      {
        id: 'DF_AIR_EMISSIONS',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD:DF_AIR_EMISSIONS(2.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:AIR_EMISSIONS(1.1)',
          },
        ],
        version: '2.1',
        agencyID: 'OECD',
        isFinal: true,
        name: 'Emissions of air pollutants',
        names: {
          en: 'Emissions of air pollutants',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'VAR=TOT',
            type: 'DEFAULT',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:AIR_EMISSIONS(1.1)',
      },
      {
        id: 'DF_KEI',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD:DF_KEI(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:DSD_KEI(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'Key Short-Term Economic Indicators',
        names: {
          en: 'Key Short-Term Economic Indicators',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'FREQUENCY=A,LOCATION=AUS,TIME_PERIOD_START=2015',
            type: 'DEFAULT',
          },
          {
            title: 'SUBJECT',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'FREQ',
            type: 'NOT_DISPLAYED',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:DSD_KEI(1.0)',
      },
      {
        id: 'DF_KEI_21760',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD:DF_KEI_21760(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:DSD_KEI(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'Harmonised Unemployment Rate',
        names: {
          en: 'Harmonised Unemployment Rate',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'FREQUENCY=A,SUBJECT=LRHUTTTT,TIME_PERIOD_START=2015',
            type: 'DEFAULT',
          },
          {
            title: 'LOCATION',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'FREQ',
            type: 'NOT_DISPLAYED',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:DSD_KEI(1.0)',
      },
      {
        id: 'DF_MONTHLY',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD:DF_MONTHLY(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:DSD_MONTHLY(3.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: false,
        name: 'Monthly Data Test',
        names: {
          en: 'Monthly Data Test',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:DSD_MONTHLY(3.0)',
      },
      {
        id: 'DF_UOM',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD:DF_UOM(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:DSD_UOM(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD',
        isFinal: false,
        name: 'National Accounts (UoM)',
        names: {
          en: 'National Accounts (UoM)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            id: 'UNIT_MEASURE_CONCEPTS',
            title: 'UNIT_MEASURE,PRICE_BASE,ADJUSTMENT,TRANSFORMATION,UNIT_MULT,BASE_YEAR',
            type: 'UNIT_MEASURE_CONCEPTS',
          },
          {
            id: 'DEFAULT',
            title: 'FREQ=A,TRANSFORMATION=NT,ADJUSTMENT=SA+NSA,PRICE_BASE=V+Q,MEASURE=GDP',
            type: 'DEFAULT',
          },
          {
            id: 'LAYOUT_ROW',
            title: 'REF_AREA,UNIT_MEASURE',
            type: 'LAYOUT_ROW',
          },
          {
            id: 'LAYOUT_COLUMN',
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            id: 'LAYOUT_ROW_SECTION',
            title: 'PRICE_BASE,ADJUSTMENT,TRANSFORMATION',
            type: 'LAYOUT_ROW_SECTION',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:DSD_UOM(3.0)',
      },
      {
        id: 'TEST_ANN2',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD:TEST_ANN2(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:DSD_ANN_TEST(2.0)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD',
        isFinal: true,
        name: 'Test of annotations',
        names: {
          en: 'Test of annotations',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            id: 'ANN_DIMENSION_ROW',
            title: 'DIMENSION_ROW',
            type: 'LAYOUT_ROW',
          },
          {
            id: 'ANN_DIMENSION_COLUMN',
            title: 'DIMENSION_COLUMN,DIMENSION_COLUMN2',
            type: 'LAYOUT_COLUMN',
          },
          {
            id: 'ANN_DIMENSION_SECTION',
            title: 'DIMENSION_SECTION,DIMENSION_SECTION2,DIMENSION_SECTION3',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            id: 'ANN_DIMENSION_HIDDEN',
            title: 'DIMENSION_HIDDEN,DIMENSION_HIDDEN2',
            type: 'NOT_DISPLAYED',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:DSD_ANN_TEST(2.0)',
      },
      {
        id: 'DF_DOMESTIC_TOURISM',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.CFE:DF_DOMESTIC_TOURISM(5.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_TOURISM_TRIPS(5.0)',
          },
        ],
        version: '5.0',
        agencyID: 'OECD.CFE',
        isFinal: true,
        name: 'Domestic tourism',
        names: {
          en: 'Domestic tourism',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'MEASURE,VISITOR_TYPE,ACCOMMODATION_TYPE',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD,SOURCE',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REPORTING_COUNTRY',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'SOURCE=TDS',
            type: 'DEFAULT',
          },
          {
            title: 'UNIT_MULT=0,ACTIVITY,TOURISM_FORM,COUNTERPART_COUNTRY,COUNTERPART_COUN',
            type: 'NOT_DISPLAYED',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_TOURISM_TRIPS(5.0)',
      },
      {
        id: 'DF_INBOUND_TOURISM',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.CFE:DF_INBOUND_TOURISM(5.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_TOURISM_TRIPS(5.0)',
          },
        ],
        version: '5.0',
        agencyID: 'OECD.CFE',
        isFinal: true,
        name: 'Inbound tourism',
        names: {
          en: 'Inbound tourism',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'TIME_PERIOD,SOURCE',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REPORTING_COUNTRY',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MULT=0,ACTIVITY,TOURISM_FORM',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'COUNTERPART_COUNTRY,VISITOR_TYPE,ACCOMMODATION_TYPE',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'SOURCE=TSS',
            type: 'DEFAULT',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_TOURISM_TRIPS(5.0)',
      },
      {
        id: 'DF_OUTBOUND_TOURISM',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.CFE:DF_OUTBOUND_TOURISM(5.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_TOURISM_TRIPS(5.0)',
          },
        ],
        version: '5.0',
        agencyID: 'OECD.CFE',
        isFinal: true,
        name: 'Outbound tourism',
        names: {
          en: 'Outbound tourism',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'COUNTERPART_COUNTRY,VISITOR_TYPE',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REPORTING_COUNTRY',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MULT=0,ACTIVITY,TOURISM_FORM',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'REPORTING_COUNTRY=AU',
            type: 'DEFAULT',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_TOURISM_TRIPS(5.0)',
      },
      {
        id: 'DF_TOURISM_EMP',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.CFE:DF_TOURISM_EMP(5.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_KEY_TOURISM(5.0)',
          },
        ],
        version: '5.0',
        agencyID: 'OECD.CFE',
        isFinal: true,
        name: 'Employment in tourism',
        names: {
          en: 'Employment in tourism',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'ACTIVITY',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REF_AREA,UNIT_MEASURE',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MULT=0,CONF_STATUS=F',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'REF_AREA=AU',
            type: 'DEFAULT',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_KEY_TOURISM(5.0)',
      },
      {
        id: 'DF_TOURISM_ENT',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.CFE:DF_TOURISM_ENT(5.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_KEY_TOURISM(5.0)',
          },
        ],
        version: '5.0',
        agencyID: 'OECD.CFE',
        isFinal: true,
        name: 'Enterprises in tourism',
        names: {
          en: 'Enterprises in tourism',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'ACTIVITY',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REF_AREA',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MULT=0,CONF_STATUS=F',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'REF_AREA=AU',
            type: 'DEFAULT',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_KEY_TOURISM(5.0)',
      },
      {
        id: 'DF_TOURISM_ENT_EMP',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.CFE:DF_TOURISM_ENT_EMP(5.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_KEY_TOURISM(5.0)',
          },
        ],
        version: '5.0',
        agencyID: 'OECD.CFE',
        isFinal: true,
        name: 'Enterprises and employment in tourism',
        names: {
          en: 'Enterprises and employment in tourism',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'ACTIVITY',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REF_AREA,MEASURE,UNIT_MEASURE',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MULT=0,CONF_STATUS=F',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'REF_AREA=AU',
            type: 'DEFAULT',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_KEY_TOURISM(5.0)',
      },
      {
        id: 'DF_TOURISM_KEY_IND_PC',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.CFE:DF_TOURISM_KEY_IND_PC(5.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_KEY_TOURISM(5.0)',
          },
        ],
        version: '5.0',
        agencyID: 'OECD.CFE',
        isFinal: true,
        name: 'Key tourism indicators',
        names: {
          en: 'Key tourism indicators',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'REF_AREA',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'MEASURE,UNIT_MEASURE',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MULT=0,ACTIVITY,CONF_STATUS=F',
            type: 'NOT_DISPLAYED',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.CFE:DSD_KEY_TOURISM(5.0)',
      },
      {
        id: 'DF_EO_N',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.ECO:DF_EO_N(1.2)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.ECO:DSD_EO_N(1.2)',
          },
        ],
        version: '1.2',
        agencyID: 'OECD.ECO',
        isFinal: false,
        name: 'Economic Outlook No 106 (new structure)',
        names: {
          en: 'Economic Outlook No 106 (new structure)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'MEASURE,UNIT_MEASURE',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REF_AREA,VALUATION',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MULT=0',
            type: 'NOT_DISPLAYED',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.ECO:DSD_EO_N(1.2)',
      },
      {
        id: 'DF_EO_N',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.ECO:DF_EO_N(1.3)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.ECO:DSD_EO_N(1.3)',
          },
        ],
        version: '1.3',
        agencyID: 'OECD.ECO',
        isFinal: false,
        name: 'Economic Outlook No 106 (new structure)',
        names: {
          en: 'Economic Outlook No 106 (new structure)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'MEASURE,UNIT_MEASURE',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REF_AREA,VALUATION',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MULT=0',
            type: 'NOT_DISPLAYED',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.ECO:DSD_EO_N(1.3)',
      },
      {
        id: 'DF_EO_O',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.ECO:DF_EO_O(1.2)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.ECO:DSD_EO_O(1.2)',
          },
        ],
        version: '1.2',
        agencyID: 'OECD.ECO',
        isFinal: false,
        name: 'Economic Outlook No 106 (old structure)',
        names: {
          en: 'Economic Outlook No 106 (old structure)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'VARIABLE,UNIT_MEASURE',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REF_AREA',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MULT=0',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'REF_AREA=AU',
            type: 'DEFAULT',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.ECO:DSD_EO_O(1.2)',
      },
      {
        id: 'DF_EO_O',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.ECO:DF_EO_O(1.3)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.ECO:DSD_EO_O(1.3)',
          },
        ],
        version: '1.3',
        agencyID: 'OECD.ECO',
        isFinal: false,
        name: 'Economic Outlook No 106 (old structure)',
        names: {
          en: 'Economic Outlook No 106 (old structure)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'VARIABLE,UNIT_MEASURE',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REF_AREA',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MULT=0',
            type: 'NOT_DISPLAYED',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.ECO:DSD_EO_O(1.3)',
      },
      {
        id: 'NAMAIN_ALL',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_ALL(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_ALL',
        names: {
          en: 'NAMAIN_ALL',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_ALL',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_ALL(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_ALL',
        names: {
          en: 'NAMAIN_ALL',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_ALL',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_ALL(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_ALL - UoM 5',
        names: {
          en: 'NAMAIN_ALL - UoM 5',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'TIME_FORMAT,OBS_STATUS=A,CONF_STATUS=F',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'FREQ=Q,REF_AREA=AU,COUNTERPART_AREA=W0,endPeriod=2017-Q1',
            type: 'DEFAULT',
          },
          {
            title: 'STO,ACTIVITY,UNIT_MEASURE,TRANSFORMATION,PRICES,ADJUSTMENT',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TABLE_IDENTIFIER',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REF_SECTOR,INSTR_ASSET,EXPENDITURE,ACCOUNTING_ENTRY',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MEASURE,PRICES,ADJUSTMENT,TRANSFORMATION,REF_YEAR_PRICE,UNIT_MULT',
            type: 'UNIT_MEASURE_CONCEPTS',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0101_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0101_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0101_A',
        names: {
          en: 'NAMAIN_T0101_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0101_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0101_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'Table 0101 - Annual Gross value added at basic prices and gross domestic product at market prices',
        names: {
          en: 'Table 0101 - Annual Gross value added at basic prices and gross domestic product at market prices',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'FREQ,OBS_STATUS=A,CONF_STATUS=F,TABLE_IDENTIFIER,ADJUSTMENT',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'STO,ACTIVITY,ACCOUNTING_ENTRY',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'REF_AREA,PRICES',
            type: 'LAYOUT_ROW_SECTION',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0101_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0101_Q(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0101_Q',
        names: {
          en: 'NAMAIN_T0101_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0101_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0101_Q(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0101_Q',
        names: {
          en: 'NAMAIN_T0101_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0101_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0101_Q(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name:
          'Table 0101 - Quarterly Gross value added at basic prices and gross domestic product at market prices (UoM Case 3)',
        names: {
          en:
            'Table 0101 - Quarterly Gross value added at basic prices and gross domestic product at market prices (UoM Case 3)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'TIME_PERIOD,ADJUSTMENT',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REF_AREA',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'FREQ,TRANSFORMATION,TABLE_IDENTIFIER,TIME_FORMAT',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'UNIT_MEASURE,UNIT_MULT,PRICES,ADJUSTMENT',
            type: 'UNIT_MEASURE_CONCEPTS',
          },
          {
            title: 'STO,ACTIVITY,ACCOUNTING_ENTRY',
            type: 'LAYOUT_ROW',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0102_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0102_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0102_A',
        names: {
          en: 'NAMAIN_T0102_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0102_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0102_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0102_A',
        names: {
          en: 'NAMAIN_T0102_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0102_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0102_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'Table 0102 - Annual GDP identity from the expenditure side (UoM case 3 without UNIT_MULT)',
        names: {
          en: 'Table 0102 - Annual GDP identity from the expenditure side (UoM case 3 without UNIT_MULT)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'STO,ACTIVITY,ACCOUNTING_ENTRY',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD,PRICES',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REF_AREA',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'FREQ,TRANSFORMATION,TABLE_IDENTIFIER,OBS_STATUS=A,TIME_FORMAT',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'UNIT_MEASURE,PRICES,ADJUSTMENT',
            type: 'UNIT_MEASURE_CONCEPTS',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0102_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0102_Q(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0102_Q',
        names: {
          en: 'NAMAIN_T0102_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0102_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0102_Q(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0102_Q',
        names: {
          en: 'NAMAIN_T0102_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0102_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0102_Q(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'Table 0102 - Quarterly GDP identity from the expenditure side (UoM Case 5)',
        names: {
          en: 'Table 0102 - Quarterly GDP identity from the expenditure side (UoM Case 5)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'REF_AREA',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'FREQ,TRANSFORMATION,TABLE_IDENTIFIER,OBS_STATUS=A,TIME_FORMAT',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'UNIT_MEASURE,UNIT_MULT,PRICES,ADJUSTMENT',
            type: 'UNIT_MEASURE_CONCEPTS',
          },
          {
            title: 'COUNTERPART_AREA=W0',
            type: 'DEFAULT',
          },
          {
            title: 'STO,ACTIVITY,ACCOUNTING_ENTRY,PRICES',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD,ADJUSTMENT',
            type: 'LAYOUT_COLUMN',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0103_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0103_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0103_A',
        names: {
          en: 'NAMAIN_T0103_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0103_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0103_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0103_A',
        names: {
          en: 'NAMAIN_T0103_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0103_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0103_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'Table 0103 - Annual GDP identity from the income side',
        names: {
          en: 'Table 0103 - Annual GDP identity from the income side',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0103_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0103_Q(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0103_Q',
        names: {
          en: 'NAMAIN_T0103_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0103_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0103_Q(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0103_Q',
        names: {
          en: 'NAMAIN_T0103_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0103_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0103_Q(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'Table 0103 - Quarterly GDP identity from the income side (UoM case 2)',
        names: {
          en: 'Table 0103 - Quarterly GDP identity from the income side (UoM case 2)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'FREQ,TRANSFORMATION,TABLE_IDENTIFIER,OBS_STATUS=A,TIME_FORMAT',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'UNIT_MEASURE,UNIT_MULT,PRICES,ADJUSTMENT',
            type: 'UNIT_MEASURE_CONCEPTS',
          },
          {
            title: 'STO,ACTIVITY,ACCOUNTING_ENTRY,ADJUSTMENT',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'REF_AREA',
            type: 'LAYOUT_ROW_SECTION',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0107_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0107_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'Table 0107 - Annual Disposable income, saving, net lending / borrowing',
        names: {
          en: 'Table 0107 - Annual Disposable income, saving, net lending / borrowing',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0107_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0107_Q(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'Table 0107 - Quarterly Disposable income, saving, net lending / borrowing (UoM Case 3)',
        names: {
          en: 'Table 0107 - Quarterly Disposable income, saving, net lending / borrowing (UoM Case 3)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'COUNTERPART_AREA',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'FREQ,TRANSFORMATION,TABLE_IDENTIFIER,OBS_STATUS=A,TIME_FORMAT',
            type: 'NOT_DISPLAYED',
          },
          {
            title: 'UNIT_MEASURE,UNIT_MULT,PRICES,ADJUSTMENT',
            type: 'UNIT_MEASURE_CONCEPTS',
          },
          {
            title: 'ACCOUNTING_ENTRY,STO',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD,ADJUSTMENT',
            type: 'LAYOUT_COLUMN',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0110_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0110_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0110_A',
        names: {
          en: 'NAMAIN_T0110_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0110_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0110_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0110_A',
        names: {
          en: 'NAMAIN_T0110_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0110_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0110_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'Table 0110 - Annual Population and employment',
        names: {
          en: 'Table 0110 - Annual Population and employment',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0110_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0110_Q(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0110_Q',
        names: {
          en: 'NAMAIN_T0110_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0110_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0110_Q(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0110_Q',
        names: {
          en: 'NAMAIN_T0110_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0110_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0110_Q(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'Table 0110 - Quarterly Population and employment (UoM Case 4)',
        names: {
          en: 'Table 0110 - Quarterly Population and employment (UoM Case 4)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'STO,ACTIVITY',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'ADJUSTMENT',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MEASURE,UNIT_MULT,ADJUSTMENT',
            type: 'UNIT_MEASURE_CONCEPTS',
          },
          {
            title: 'FREQ,TRANSFORMATION,COUNTERPART_AREA,TABLE_IDENTIFIER,OBS_STATUS=A,TIM',
            type: 'NOT_DISPLAYED',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0111_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0111_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0111_A',
        names: {
          en: 'NAMAIN_T0111_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0111_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0111_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0111_A',
        names: {
          en: 'NAMAIN_T0111_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0111_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0111_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'Table 0111 - Annual Employment by industry',
        names: {
          en: 'Table 0111 - Annual Employment by industry',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0111_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0111_Q(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0111_Q',
        names: {
          en: 'NAMAIN_T0111_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0111_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0111_Q(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0111_Q',
        names: {
          en: 'NAMAIN_T0111_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0111_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0111_Q(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'Table 0111 - Quarterly Employment by industry (UoM Case 1)',
        names: {
          en: 'Table 0111 - Quarterly Employment by industry (UoM Case 1)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'ACTIVITY',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'STO',
            type: 'LAYOUT_ROW_SECTION',
          },
          {
            title: 'UNIT_MEASURE,UNIT_MULT',
            type: 'UNIT_MEASURE_CONCEPTS',
          },
          {
            title: 'FREQ,ADJUSTMENT,TRANSFORMATION,COUNTERPART_AREA,OBS_STATUS=A,TIME_FORM',
            type: 'NOT_DISPLAYED',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0117_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0117_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0117_A',
        names: {
          en: 'NAMAIN_T0117_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0117_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0117_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0117_A',
        names: {
          en: 'NAMAIN_T0117_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0117_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0117_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0117_A',
        names: {
          en: 'NAMAIN_T0117_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0117_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0117_Q(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0117_Q',
        names: {
          en: 'NAMAIN_T0117_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0117_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0117_Q(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0117_Q',
        names: {
          en: 'NAMAIN_T0117_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0117_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0117_Q(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0117_Q',
        names: {
          en: 'NAMAIN_T0117_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0120_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0120_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0120_A',
        names: {
          en: 'NAMAIN_T0120_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0120_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0120_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0120_A',
        names: {
          en: 'NAMAIN_T0120_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0120_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0120_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0120_A',
        names: {
          en: 'NAMAIN_T0120_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0120_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0120_Q(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0120_Q',
        names: {
          en: 'NAMAIN_T0120_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0120_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0120_Q(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0120_Q',
        names: {
          en: 'NAMAIN_T0120_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0120_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0120_Q(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0120_Q',
        names: {
          en: 'NAMAIN_T0120_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0121_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0121_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0121_A',
        names: {
          en: 'NAMAIN_T0121_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0121_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0121_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0121_A',
        names: {
          en: 'NAMAIN_T0121_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0121_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0121_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0121_A',
        names: {
          en: 'NAMAIN_T0121_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0121_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0121_Q(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0121_Q',
        names: {
          en: 'NAMAIN_T0121_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0121_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0121_Q(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0121_Q',
        names: {
          en: 'NAMAIN_T0121_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0121_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0121_Q(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0121_Q',
        names: {
          en: 'NAMAIN_T0121_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0301_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0301_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0301_A',
        names: {
          en: 'NAMAIN_T0301_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0301_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0301_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0301_A',
        names: {
          en: 'NAMAIN_T0301_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0301_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0301_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0301_A',
        names: {
          en: 'NAMAIN_T0301_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0302_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0302_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0302_A',
        names: {
          en: 'NAMAIN_T0302_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0302_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0302_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0302_A',
        names: {
          en: 'NAMAIN_T0302_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0302_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0302_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0302_A',
        names: {
          en: 'NAMAIN_T0302_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0303_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0303_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0303_A',
        names: {
          en: 'NAMAIN_T0303_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0303_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0303_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0303_A',
        names: {
          en: 'NAMAIN_T0303_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0303_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0303_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0303_A',
        names: {
          en: 'NAMAIN_T0303_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0501_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0501_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0501_A',
        names: {
          en: 'NAMAIN_T0501_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0501_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0501_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0501_A',
        names: {
          en: 'NAMAIN_T0501_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0501_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0501_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0501_A',
        names: {
          en: 'NAMAIN_T0501_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T0502_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0502_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0502_A',
        names: {
          en: 'NAMAIN_T0502_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T0502_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0502_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0502_A',
        names: {
          en: 'NAMAIN_T0502_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T0502_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T0502_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T0502_A',
        names: {
          en: 'NAMAIN_T0502_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T2000_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T2000_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T2000_A',
        names: {
          en: 'NAMAIN_T2000_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T2000_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T2000_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T2000_A',
        names: {
          en: 'NAMAIN_T2000_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T2000_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T2000_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T2000_A',
        names: {
          en: 'NAMAIN_T2000_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAMAIN_T2200_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T2200_A(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T2200_A',
        names: {
          en: 'NAMAIN_T2200_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(1.0)',
      },
      {
        id: 'NAMAIN_T2200_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T2200_A(1.1)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T2200_A',
        names: {
          en: 'NAMAIN_T2200_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NA_MAIN(1.1)',
      },
      {
        id: 'NAMAIN_T2200_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAMAIN_T2200_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAMAIN_T2200_A',
        names: {
          en: 'NAMAIN_T2200_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAMAIN(3.0)',
      },
      {
        id: 'NAPENS_ALL',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAPENS_ALL(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAPENS(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAPENS_ALL',
        names: {
          en: 'NAPENS_ALL',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAPENS(3.0)',
      },
      {
        id: 'NAPENS_T2900_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAPENS_T2900_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAPENS(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAPENS_T2900_A',
        names: {
          en: 'NAPENS_T2900_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAPENS(3.0)',
      },
      {
        id: 'NAPENS_T2901_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAPENS_T2901_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAPENS(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAPENS_T2901_A',
        names: {
          en: 'NAPENS_T2901_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAPENS(3.0)',
      },
      {
        id: 'NAPENS_T2902_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAPENS_T2902_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAPENS(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAPENS_T2902_A',
        names: {
          en: 'NAPENS_T2902_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAPENS(3.0)',
      },
      {
        id: 'NAREG_ALL',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAREG_ALL(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAREG(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NAREG_ALL',
        names: {
          en: 'NAREG_ALL',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAREG(3.0)',
      },
      {
        id: 'NAREG_T1001_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAREG_T1001_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAREG(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NA Regional Statistics',
        names: {
          en: 'NA Regional Statistics',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAREG(3.0)',
      },
      {
        id: 'NAREG_T1002_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAREG_T1002_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAREG(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NA Regional Statistics',
        names: {
          en: 'NA Regional Statistics',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAREG(3.0)',
      },
      {
        id: 'NAREG_T1200_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAREG_T1200_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAREG(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NA Regional Statistics',
        names: {
          en: 'NA Regional Statistics',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAREG(3.0)',
      },
      {
        id: 'NAREG_T1300_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NAREG_T1300_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAREG(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NA Regional Statistics',
        names: {
          en: 'NA Regional Statistics',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NAREG(3.0)',
      },
      {
        id: 'NASEC_ALL',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_ALL(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_ALL',
        names: {
          en: 'NASEC_ALL',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0610_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0610_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0610_A',
        names: {
          en: 'NASEC_T0610_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0611_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0611_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0611_A',
        names: {
          en: 'NASEC_T0611_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0612_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0612_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0612_A',
        names: {
          en: 'NASEC_T0612_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0620_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0620_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0620_A',
        names: {
          en: 'NASEC_T0620_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0621_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0621_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0621_A',
        names: {
          en: 'NASEC_T0621_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0622_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0622_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0622_A',
        names: {
          en: 'NASEC_T0622_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0625_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0625_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0625_A',
        names: {
          en: 'NASEC_T0625_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0710_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0710_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0710_A',
        names: {
          en: 'NASEC_T0710_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0720_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0720_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0720_A',
        names: {
          en: 'NASEC_T0720_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0725_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0725_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0725_A',
        names: {
          en: 'NASEC_T0725_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0800_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0800_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0800_A',
        names: {
          en: 'NASEC_T0800_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0801SA_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0801SA_Q(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0801SA_Q',
        names: {
          en: 'NASEC_T0801SA_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T0801_Q',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T0801_Q(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T0801_Q',
        names: {
          en: 'NASEC_T0801_Q',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASEC_T2600_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASEC_T2600_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASEC_T2600_A',
        names: {
          en: 'NASEC_T2600_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASEC(3.0)',
      },
      {
        id: 'NASU_1516ADD_5',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASU_1516ADD_5(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASU(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASU_1516ADD_5',
        names: {
          en: 'NASU_1516ADD_5',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASU(3.0)',
      },
      {
        id: 'NASU_1516_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASU_1516_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASU(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASU_1516_A',
        names: {
          en: 'NASU_1516_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASU(3.0)',
      },
      {
        id: 'NASU_163034_A',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASU_163034_A(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASU(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASU_163034_A',
        names: {
          en: 'NASU_163034_A',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASU(3.0)',
      },
      {
        id: 'NASU_16BP_5',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASU_16BP_5(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASU(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASU_16BP_5',
        names: {
          en: 'NASU_16BP_5',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASU(3.0)',
      },
      {
        id: 'NASU_1719IN_5',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASU_1719IN_5(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASU(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASU_1719IN_5',
        names: {
          en: 'NASU_1719IN_5',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASU(3.0)',
      },
      {
        id: 'NASU_1719PR_5',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.SDD:NASU_1719PR_5(3.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASU(3.0)',
          },
        ],
        version: '3.0',
        agencyID: 'OECD.SDD',
        isFinal: false,
        name: 'NASU_1719PR_5',
        names: {
          en: 'NASU_1719PR_5',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.SDD:NASU(3.0)',
      },
      {
        id: 'BISE_DF',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=TN1:BISE_DF(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TN1:TIPO_B_DSD(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'TN1',
        isFinal: true,
        name: 'Banco de Indicadores INEGI (BIINEGI)',
        names: {
          en: 'Banco de Indicadores INEGI (BIINEGI)',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TN1:TIPO_B_DSD(1.0)',
      },
      {
        id: 'CPI_DF',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=TN1:CPI_DF(1.0)',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TN1:DSD_CPI(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'TN1',
        isFinal: true,
        name: 'TN Consumer Prices Index',
        names: {
          en: 'TN Consumer Prices Index',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TN1:DSD_CPI(1.0)',
      },
    ],
    categorySchemes: [
      {
        id: 'OECDCS1',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=OECD:OECDCS1(1.0)',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'Topic',
        names: {
          en: 'Topic',
        },
        description: 'The main OECD category scheme. First cut.',
        descriptions: {
          en: 'The main OECD category scheme. First cut.',
        },
        isPartial: false,
        categories: [
          {
            id: 'ECO',
            name: 'Economy',
            names: {
              en: 'Economy',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).ECO',
              },
            ],
          },
          {
            id: 'ENV',
            name: 'Environment',
            names: {
              en: 'Environment',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).ENV',
              },
            ],
            categories: [
              {
                id: 'ENV_AC',
                name: 'Air and Climate',
                names: {
                  en: 'Air and Climate',
                },
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).ENV.ENV_AC',
                  },
                ],
              },
              {
                id: 'ENV_FO',
                name: 'Forest',
                names: {
                  en: 'Forest',
                },
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).ENV.ENV_FO',
                  },
                ],
              },
              {
                id: 'ENV_WAS',
                name: 'Waste',
                names: {
                  en: 'Waste',
                },
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).ENV.ENV_WAS',
                  },
                ],
              },
              {
                id: 'ENV_WAT',
                name: 'Water',
                names: {
                  en: 'Water',
                },
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).ENV.ENV_WAT',
                  },
                ],
              },
            ],
          },
          {
            id: 'SOC',
            name: 'Society',
            names: {
              en: 'Society',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).SOC',
              },
            ],
          },
          {
            id: 'EDU',
            name: 'Education',
            names: {
              en: 'Education',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).EDU',
              },
            ],
          },
          {
            id: 'NRG',
            name: 'Energy',
            names: {
              en: 'Energy',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).NRG',
              },
            ],
          },
          {
            id: 'GOV',
            name: 'Government',
            names: {
              en: 'Government',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).GOV',
              },
            ],
            categories: [
              {
                id: 'GOV_GG',
                name: 'General government',
                names: {
                  en: 'General government',
                },
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).GOV.GOV_GG',
                  },
                ],
              },
              {
                id: 'GOV_TAX',
                name: 'Taxation',
                names: {
                  en: 'Taxation',
                },
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).GOV.GOV_TAX',
                  },
                ],
              },
            ],
          },
          {
            id: 'HEA',
            name: 'Health',
            names: {
              en: 'Health',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).HEA',
              },
            ],
          },
          {
            id: 'INT',
            name: 'Innovation and Technology',
            names: {
              en: 'Innovation and Technology',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).INT',
              },
            ],
          },
          {
            id: 'EMP',
            name: 'Employment',
            names: {
              en: 'Employment',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).EMP',
              },
            ],
          },
          {
            id: 'IND',
            name: 'Industry',
            names: {
              en: 'Industry',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).IND',
              },
            ],
            categories: [
              {
                id: 'IND_TOUR',
                name: 'Tourism',
                names: {
                  en: 'Tourism',
                },
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).IND.IND_TOUR',
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  meta: {
    schema:
      'https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/structure-message/tools/schemas/1.0/sdmx-json-structure-schema.json',
    contentLanguages: ['en'],
    id: 'IDREF4825',
    prepared: '2020-10-15T13:59:01.1618964Z',
    test: false,
    sender: {
      id: 'Unknown',
    },
    receiver: [
      {
        id: 'Unknown',
      },
    ],
  },
};
