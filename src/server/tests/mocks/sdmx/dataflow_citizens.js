module.exports = ({ dataflowId = 'DF_GOV_SERVING_CITIZENS', version = '2.0', agencyId = 'OECD.GOV' }) => ({
  data: {
    dataflows: [
      {
        id: dataflowId,
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.GOV:DF_GOV_SERVING_CITIZENS(2.0)',
            type: 'dataflow',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.GOV:DSD_GOV_8_9(2.0)',
            type: 'datastructure',
          },
        ],
        version,
        agencyID: agencyId,
        isFinal: false,
        name: 'Serving citizens',
        names: {
          en: 'Serving citizens',
        },
        annotations: [
          {
            type: 'SEARCH_WEIGHT',
            title: '0',
            texts: {
              en: '16.9',
            },
          },
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
          {
            title: 'MEASURE,UNIT_MEASURE,SEX,AGE,EDUCATION_LEV',
            type: 'LAYOUT_ROW',
          },
          {
            title: 'TIME_PERIOD',
            type: 'LAYOUT_COLUMN',
          },
          {
            title: 'REF_AREA=FR,TIME_PERIOD_START=2015,TIME_PERIOD_END=2019',
            type: 'DEFAULT',
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.GOV:DSD_GOV_8_9(2.0)',
      },
    ],
    categorySchemes: [
      {
        id: 'OECDCS1',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=OECD:OECDCS1(1.0)',
            type: 'categoryscheme',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'Topic',
        names: {
          en: 'Topic',
        },
        description: 'The main OECD category scheme. First cut.',
        descriptions: {
          en: 'The main OECD category scheme. First cut.',
        },
        isPartial: true,
        categories: [
          {
            id: 'GOV',
            name: 'Government',
            names: {
              en: 'Government',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).GOV',
                type: 'category',
              },
            ],
            categories: [
              {
                id: 'GOV_GG',
                name: 'General government',
                names: {
                  en: 'General government',
                },
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).GOV.GOV_GG',
                    type: 'category',
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
    categorisations: [
      {
        id: 'CAT_DF_GOV_SERVING_CITIZENS',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Categorisation=OECD.GOV:CAT_DF_GOV_SERVING_CITIZENS(2.0)',
            type: 'categorisation',
          },
        ],
        version: '2.0',
        agencyID: 'OECD.GOV',
        isFinal: false,
        name: 'Category for DF_GOV_SERVING_CITIZENS',
        names: {
          en: 'Category for DF_GOV_SERVING_CITIZENS',
        },
        source: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.GOV:DF_GOV_SERVING_CITIZENS(2.0)',
        target: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=OECD:OECDCS1(1.0).GOV.GOV_GG',
      },
    ],
    conceptSchemes: [
      {
        id: 'CS_GOV_TEST',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=OECD.GOV:CS_GOV_TEST(1.1)',
            type: 'conceptscheme',
          },
        ],
        version: '1.1',
        agencyID: 'OECD.GOV',
        isFinal: false,
        name: 'GOV Concept Scheme',
        names: {
          en: 'GOV Concept Scheme',
        },
        isPartial: false,
        concepts: [
          {
            id: 'FREQ',
            name: 'Frequency',
            names: {
              en: 'Frequency',
            },
            description: 'Time interval at which observations occur over a given time period',
            descriptions: {
              en: 'Time interval at which observations occur over a given time period',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).FREQ',
                type: 'concept',
              },
            ],
          },
          {
            id: 'MEASURE',
            name: 'Measure',
            names: {
              en: 'Measure',
            },
            description:
              'Data element that represents a simple aggregation and which takes one or more vectors as arguments and returns a scalar.',
            descriptions: {
              en:
                'Data element that represents a simple aggregation and which takes one or more vectors as arguments and returns a scalar.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).MEASURE',
                type: 'concept',
              },
            ],
          },
          {
            id: 'REF_AREA',
            name: 'Reference area',
            names: {
              en: 'Reference area',
            },
            description: 'Country or geographic area to which the measured statistical phenomenon relates.',
            descriptions: {
              en: 'Country or geographic area to which the measured statistical phenomenon relates.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).REF_AREA',
                type: 'concept',
              },
            ],
          },
          {
            id: 'AGE',
            name: 'Age',
            names: {
              en: 'Age',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).AGE',
                type: 'concept',
              },
            ],
          },
          {
            id: 'SEX',
            name: 'Sex',
            names: {
              en: 'Sex',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).SEX',
                type: 'concept',
              },
            ],
          },
          {
            id: 'EDUCATION_LEV',
            name: 'Education level',
            names: {
              en: 'Education level',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).EDUCATION_LEV',
                type: 'concept',
              },
            ],
          },
          {
            id: 'CATEGORY',
            name: 'Field',
            names: {
              en: 'Field',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).CATEGORY',
                type: 'concept',
              },
            ],
          },
          {
            id: 'UNIT_MEASURE',
            name: 'Unit of measure',
            names: {
              en: 'Unit of measure',
            },
            description: 'Unit in which the data values are expressed.',
            descriptions: {
              en: 'Unit in which the data values are expressed.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).UNIT_MEASURE',
                type: 'concept',
              },
            ],
          },
          {
            id: 'TIME_PERIOD',
            name: 'Time period',
            names: {
              en: 'Time period',
            },
            description: 'Time span or point in time to which the observation actually refers',
            descriptions: {
              en: 'Time span or point in time to which the observation actually refers',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).TIME_PERIOD',
                type: 'concept',
              },
            ],
          },
          {
            id: 'OBS_VALUE',
            name: 'Observation value',
            names: {
              en: 'Observation value',
            },
            description: 'Observation value',
            descriptions: {
              en: 'Observation value',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).OBS_VALUE',
                type: 'concept',
              },
            ],
          },
          {
            id: 'UNIT_MULT',
            name: 'Unit multiplier',
            names: {
              en: 'Unit multiplier',
            },
            description:
              'Exponent in base 10 specified so that multiplying the observation numeric values by 10^UNIT_MULT gives a value expressed in the unit of measure',
            descriptions: {
              en:
                'Exponent in base 10 specified so that multiplying the observation numeric values by 10^UNIT_MULT gives a value expressed in the unit of measure',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).UNIT_MULT',
                type: 'concept',
              },
            ],
          },
          {
            id: 'OBS_STATUS',
            name: 'Observation status',
            names: {
              en: 'Observation status',
            },
            description: 'Information on the quality of a value or an unusual or missing value',
            descriptions: {
              en: 'Information on the quality of a value or an unusual or missing value',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).OBS_STATUS',
                type: 'concept',
              },
            ],
          },
        ],
      },
    ],
    codelists: [
      {
        id: 'CL_AGE',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.GOV:CL_AGE(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.GOV',
        isFinal: false,
        name: 'Codelist for age',
        names: {
          en: 'Codelist for age',
        },
        isPartial: true,
        codes: [
          {
            id: 'Y3',
            name: '3 years',
            names: {
              en: '3 years',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AGE(1.0).Y3',
                type: 'code',
              },
            ],
          },
          {
            id: 'Y4',
            name: '4 years',
            names: {
              en: '4 years',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AGE(1.0).Y4',
                type: 'code',
              },
            ],
          },
          {
            id: 'Y_GE15',
            name: '15 years and older',
            names: {
              en: '15 years and older',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AGE(1.0).Y_GE15',
                type: 'code',
              },
            ],
          },
          {
            id: 'Y18T24',
            name: 'From 18 to 24 years',
            names: {
              en: 'From 18 to 24 years',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AGE(1.0).Y18T24',
                type: 'code',
              },
            ],
          },
          {
            id: 'Y_LT25',
            name: 'Less than 25 years',
            names: {
              en: 'Less than 25 years',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AGE(1.0).Y_LT25',
                type: 'code',
              },
            ],
          },
          {
            id: 'Y_GE45',
            name: '45 years and older',
            names: {
              en: '45 years and older',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AGE(1.0).Y_GE45',
                type: 'code',
              },
            ],
          },
          {
            id: 'Y_GE65',
            name: '65 years and older',
            names: {
              en: '65 years and older',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AGE(1.0).Y_GE65',
                type: 'code',
              },
            ],
          },
          {
            id: '_T',
            name: 'Total',
            names: {
              en: 'Total',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AGE(1.0)._T',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_AREA',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.GOV:CL_AREA(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.GOV',
        isFinal: false,
        name: 'Code list for area codes',
        names: {
          en: 'Code list for area codes',
        },
        isPartial: true,
        codes: [
          {
            id: 'AT',
            name: 'Austria',
            names: {
              en: 'Austria',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).AT',
                type: 'code',
              },
            ],
          },
          {
            id: 'AU',
            name: 'Australia',
            names: {
              en: 'Australia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).AU',
                type: 'code',
              },
            ],
          },
          {
            id: 'BE',
            name: 'Belgium',
            names: {
              en: 'Belgium',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).BE',
                type: 'code',
              },
            ],
          },
          {
            id: 'BG',
            name: 'Bulgaria',
            names: {
              en: 'Bulgaria',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).BG',
                type: 'code',
              },
            ],
          },
          {
            id: 'BR',
            name: 'Brazil',
            names: {
              en: 'Brazil',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).BR',
                type: 'code',
              },
            ],
          },
          {
            id: 'CA',
            name: 'Canada',
            names: {
              en: 'Canada',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).CA',
                type: 'code',
              },
            ],
          },
          {
            id: 'CH',
            name: 'Switzerland',
            names: {
              en: 'Switzerland',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).CH',
                type: 'code',
              },
            ],
          },
          {
            id: 'CL',
            name: 'Chile',
            names: {
              en: 'Chile',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).CL',
                type: 'code',
              },
            ],
          },
          {
            id: 'CN',
            name: 'China',
            names: {
              en: 'China',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).CN',
                type: 'code',
              },
            ],
          },
          {
            id: 'CO',
            name: 'Colombia',
            names: {
              en: 'Colombia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).CO',
                type: 'code',
              },
            ],
          },
          {
            id: 'CR',
            name: 'Costa Rica',
            names: {
              en: 'Costa Rica',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).CR',
                type: 'code',
              },
            ],
          },
          {
            id: 'CY',
            name: 'Cyprus',
            names: {
              en: 'Cyprus',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).CY',
                type: 'code',
              },
            ],
          },
          {
            id: 'CZ',
            name: 'Czech Republic',
            names: {
              en: 'Czech Republic',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).CZ',
                type: 'code',
              },
            ],
          },
          {
            id: 'DE',
            name: 'Germany',
            names: {
              en: 'Germany',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).DE',
                type: 'code',
              },
            ],
          },
          {
            id: 'DK',
            name: 'Denmark',
            names: {
              en: 'Denmark',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).DK',
                type: 'code',
              },
            ],
          },
          {
            id: 'EE',
            name: 'Estonia',
            names: {
              en: 'Estonia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).EE',
                type: 'code',
              },
            ],
          },
          {
            id: 'ES',
            name: 'Spain',
            names: {
              en: 'Spain',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).ES',
                type: 'code',
              },
            ],
          },
          {
            id: 'FI',
            name: 'Finland',
            names: {
              en: 'Finland',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).FI',
                type: 'code',
              },
            ],
          },
          {
            id: 'FR',
            name: 'France',
            names: {
              en: 'France',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).FR',
                type: 'code',
              },
            ],
          },
          {
            id: 'GB',
            name: 'United Kingdom of Great Britain and Northern Ireland',
            names: {
              en: 'United Kingdom of Great Britain and Northern Ireland',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).GB',
                type: 'code',
              },
            ],
          },
          {
            id: 'GR',
            name: 'Greece',
            names: {
              en: 'Greece',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).GR',
                type: 'code',
              },
            ],
          },
          {
            id: 'HR',
            name: 'Croatia',
            names: {
              en: 'Croatia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).HR',
                type: 'code',
              },
            ],
          },
          {
            id: 'HU',
            name: 'Hungary',
            names: {
              en: 'Hungary',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).HU',
                type: 'code',
              },
            ],
          },
          {
            id: 'ID',
            name: 'Indonesia',
            names: {
              en: 'Indonesia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).ID',
                type: 'code',
              },
            ],
          },
          {
            id: 'IE',
            name: 'Ireland',
            names: {
              en: 'Ireland',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).IE',
                type: 'code',
              },
            ],
          },
          {
            id: 'IL',
            name: 'Israel',
            names: {
              en: 'Israel',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).IL',
                type: 'code',
              },
            ],
          },
          {
            id: 'IN',
            name: 'India',
            names: {
              en: 'India',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).IN',
                type: 'code',
              },
            ],
          },
          {
            id: 'IS',
            name: 'Iceland',
            names: {
              en: 'Iceland',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).IS',
                type: 'code',
              },
            ],
          },
          {
            id: 'IT',
            name: 'Italy',
            names: {
              en: 'Italy',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).IT',
                type: 'code',
              },
            ],
          },
          {
            id: 'JP',
            name: 'Japan',
            names: {
              en: 'Japan',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).JP',
                type: 'code',
              },
            ],
          },
          {
            id: 'KR',
            name: 'Korea, Republic of',
            names: {
              en: 'Korea, Republic of',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).KR',
                type: 'code',
              },
            ],
          },
          {
            id: 'LT',
            name: 'Lithuania',
            names: {
              en: 'Lithuania',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).LT',
                type: 'code',
              },
            ],
          },
          {
            id: 'LU',
            name: 'Luxembourg',
            names: {
              en: 'Luxembourg',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).LU',
                type: 'code',
              },
            ],
          },
          {
            id: 'LV',
            name: 'Latvia',
            names: {
              en: 'Latvia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).LV',
                type: 'code',
              },
            ],
          },
          {
            id: 'MT',
            name: 'Malta',
            names: {
              en: 'Malta',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).MT',
                type: 'code',
              },
            ],
          },
          {
            id: 'MX',
            name: 'Mexico',
            names: {
              en: 'Mexico',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).MX',
                type: 'code',
              },
            ],
          },
          {
            id: 'NL',
            name: 'Netherlands',
            names: {
              en: 'Netherlands',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).NL',
                type: 'code',
              },
            ],
          },
          {
            id: 'NO',
            name: 'Norway',
            names: {
              en: 'Norway',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).NO',
                type: 'code',
              },
            ],
          },
          {
            id: 'NZ',
            name: 'New Zealand',
            names: {
              en: 'New Zealand',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).NZ',
                type: 'code',
              },
            ],
          },
          {
            id: 'PL',
            name: 'Poland',
            names: {
              en: 'Poland',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).PL',
                type: 'code',
              },
            ],
          },
          {
            id: 'PT',
            name: 'Portugal',
            names: {
              en: 'Portugal',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).PT',
                type: 'code',
              },
            ],
          },
          {
            id: 'RO',
            name: 'Romania',
            names: {
              en: 'Romania',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).RO',
                type: 'code',
              },
            ],
          },
          {
            id: 'RU',
            name: 'Russian Federation',
            names: {
              en: 'Russian Federation',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).RU',
                type: 'code',
              },
            ],
          },
          {
            id: 'SE',
            name: 'Sweden',
            names: {
              en: 'Sweden',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).SE',
                type: 'code',
              },
            ],
          },
          {
            id: 'SI',
            name: 'Slovenia',
            names: {
              en: 'Slovenia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).SI',
                type: 'code',
              },
            ],
          },
          {
            id: 'SK',
            name: 'Slovakia',
            names: {
              en: 'Slovakia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).SK',
                type: 'code',
              },
            ],
          },
          {
            id: 'TR',
            name: 'Turkey',
            names: {
              en: 'Turkey',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).TR',
                type: 'code',
              },
            ],
          },
          {
            id: 'US',
            name: 'United States of America',
            names: {
              en: 'United States of America',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).US',
                type: 'code',
              },
            ],
          },
          {
            id: 'ZA',
            name: 'South Africa',
            names: {
              en: 'South Africa',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).ZA',
                type: 'code',
              },
            ],
          },
          {
            id: 'OECDAVG',
            name: 'OECD - Average',
            names: {
              en: 'OECD - Average',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).OECDAVG',
                type: 'code',
              },
            ],
          },
          {
            id: 'OECDEU',
            name: 'OECD - Europe',
            names: {
              en: 'OECD - Europe',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_AREA(1.0).OECDEU',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_CATEGORY',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.GOV:CL_CATEGORY(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.GOV',
        isFinal: false,
        name: 'Codelist for government functions',
        names: {
          en: 'Codelist for government functions',
        },
        isPartial: true,
        codes: [
          {
            id: 'ACC',
            name: 'Access',
            names: {
              en: 'Access',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_CATEGORY(1.0).ACC',
                type: 'code',
              },
            ],
          },
          {
            id: 'AFF',
            name: 'Affordability',
            names: {
              en: 'Affordability',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_CATEGORY(1.0).AFF',
                type: 'code',
              },
            ],
            parent: 'ACC',
          },
          {
            id: 'GEO',
            name: 'Geographic proximity',
            names: {
              en: 'Geographic proximity',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_CATEGORY(1.0).GEO',
                type: 'code',
              },
            ],
            parent: 'ACC',
          },
          {
            id: 'INF',
            name: 'Access to information',
            names: {
              en: 'Access to information',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_CATEGORY(1.0).INF',
                type: 'code',
              },
            ],
            parent: 'ACC',
          },
          {
            id: 'RES',
            name: 'Responsiveness',
            names: {
              en: 'Responsiveness',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_CATEGORY(1.0).RES',
                type: 'code',
              },
            ],
          },
          {
            id: 'COU',
            name: 'Courtesy and treatment',
            names: {
              en: 'Courtesy and treatment',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_CATEGORY(1.0).COU',
                type: 'code',
              },
            ],
            parent: 'RES',
          },
          {
            id: 'MOS',
            name: 'Match of services to special needs',
            names: {
              en: 'Match of services to special needs',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_CATEGORY(1.0).MOS',
                type: 'code',
              },
            ],
            parent: 'RES',
          },
          {
            id: 'TIM',
            name: 'Timeliness',
            names: {
              en: 'Timeliness',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_CATEGORY(1.0).TIM',
                type: 'code',
              },
            ],
            parent: 'RES',
          },
          {
            id: 'QUA',
            name: 'Quality',
            names: {
              en: 'Quality',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_CATEGORY(1.0).QUA',
                type: 'code',
              },
            ],
          },
          {
            id: 'EDS',
            name: 'Effective delivery of services and outcomes',
            names: {
              en: 'Effective delivery of services and outcomes',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_CATEGORY(1.0).EDS',
                type: 'code',
              },
            ],
            parent: 'QUA',
          },
          {
            id: 'CSD',
            name: 'Consistensy in service delivery and outcomes',
            names: {
              en: 'Consistensy in service delivery and outcomes',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_CATEGORY(1.0).CSD',
                type: 'code',
              },
            ],
            parent: 'QUA',
          },
          {
            id: 'SEC',
            name: 'Security/Safety',
            names: {
              en: 'Security/Safety',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_CATEGORY(1.0).SEC',
                type: 'code',
              },
            ],
            parent: 'QUA',
          },
        ],
      },
      {
        id: 'CL_EDUCATION_LEV',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.GOV:CL_EDUCATION_LEV(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.GOV',
        isFinal: false,
        name: 'Codelist for education level',
        names: {
          en: 'Codelist for education level',
        },
        isPartial: true,
        codes: [
          {
            id: 'ISCED11_02',
            name: 'Pre-primary education',
            names: {
              en: 'Pre-primary education',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_EDUCATION_LEV(1.0).ISCED11_02',
                type: 'code',
              },
            ],
          },
          {
            id: 'ISCED11_2',
            name: 'Lower secondary education',
            names: {
              en: 'Lower secondary education',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_EDUCATION_LEV(1.0).ISCED11_2',
                type: 'code',
              },
            ],
          },
          {
            id: 'ISCED11_5T8',
            name: 'Tertiary education',
            names: {
              en: 'Tertiary education',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_EDUCATION_LEV(1.0).ISCED11_5T8',
                type: 'code',
              },
            ],
          },
          {
            id: 'ISCED11_0_1',
            name: 'Early childhood and primary education',
            names: {
              en: 'Early childhood and primary education',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_EDUCATION_LEV(1.0).ISCED11_0_1',
                type: 'code',
              },
            ],
          },
          {
            id: 'ISCED11_1T4',
            name: 'Primary to post-secondary non-tertiary education',
            names: {
              en: 'Primary to post-secondary non-tertiary education',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_EDUCATION_LEV(1.0).ISCED11_1T4',
                type: 'code',
              },
            ],
          },
          {
            id: '_T',
            name: 'Total',
            names: {
              en: 'Total',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_EDUCATION_LEV(1.0)._T',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_MEASURE',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.GOV:CL_MEASURE(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.GOV',
        isFinal: false,
        name: 'Code list for measure',
        names: {
          en: 'Code list for measure',
        },
        isPartial: true,
        codes: [
          {
            id: 'CS_HC',
            name: 'Citizens satisfied with the healthcare system',
            names: {
              en: 'Citizens satisfied with the healthcare system',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).CS_HC',
                type: 'code',
              },
            ],
          },
          {
            id: 'CS_ES',
            name: 'Citizens satisfied with the education system',
            names: {
              en: 'Citizens satisfied with the education system',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).CS_ES',
                type: 'code',
              },
            ],
          },
          {
            id: 'CONF_JS',
            name: 'Citizens having confidence in the judicial system',
            names: {
              en: 'Citizens having confidence in the judicial system',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).CONF_JS',
                type: 'code',
              },
            ],
          },
          {
            id: 'CONF_LPS',
            name: 'Citizens having confidence in the local police',
            names: {
              en: 'Citizens having confidence in the local police',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).CONF_LPS',
                type: 'code',
              },
            ],
          },
          {
            id: 'AH_UNMETN_NA',
            name: 'Unmet care needs for medical examination by income level, national average',
            names: {
              en: 'Unmet care needs for medical examination by income level, national average',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AH_UNMETN_NA',
                type: 'code',
              },
            ],
          },
          {
            id: 'AH_UNMETN_LI',
            name: 'Unmet care needs for medical examination by income level, low income',
            names: {
              en: 'Unmet care needs for medical examination by income level, low income',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AH_UNMETN_LI',
                type: 'code',
              },
            ],
          },
          {
            id: 'AH_UNMETN_HI',
            name: 'Unmet care needs for medical examination by income level, high income',
            names: {
              en: 'Unmet care needs for medical examination by income level, high income',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AH_UNMETN_HI',
                type: 'code',
              },
            ],
          },
          {
            id: 'AH_OOPME',
            name: 'Out-of-pocket (OOP) medical expenditure',
            names: {
              en: 'Out-of-pocket (OOP) medical expenditure',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AH_OOPME',
                type: 'code',
              },
            ],
          },
          {
            id: 'AE_PE',
            name: 'Private expenditures on education after transfers',
            names: {
              en: 'Private expenditures on education after transfers',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AE_PE',
                type: 'code',
              },
            ],
          },
          {
            id: 'AE_ENR',
            name: 'Enrolment rate',
            names: {
              en: 'Enrolment rate',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AE_ENR',
                type: 'code',
              },
            ],
          },
          {
            id: 'AE_ER_T',
            name: 'First-time tertiary entry rates',
            names: {
              en: 'First-time tertiary entry rates',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AE_ER_T',
                type: 'code',
              },
            ],
          },
          {
            id: 'AE_ER_EXIS',
            name: 'First-time tertiary entry rates, excluding international students',
            names: {
              en: 'First-time tertiary entry rates, excluding international students',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AE_ER_EXIS',
                type: 'code',
              },
            ],
          },
          {
            id: 'AJ_I_AD',
            name: 'Individuals who took action to resolve their disputes over the past 2 years',
            names: {
              en: 'Individuals who took action to resolve their disputes over the past 2 years',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AJ_I_AD',
                type: 'code',
              },
            ],
          },
          {
            id: 'AJ_I_LA',
            name: 'Individuals who received legal assistance over the past 2 years',
            names: {
              en: 'Individuals who received legal assistance over the past 2 years',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AJ_I_LA',
                type: 'code',
              },
            ],
          },
          {
            id: 'AJ_TNALA_NIP',
            name:
              'Individuals who did not attempt to obtain legal assistance because they thought the problem was not difficult',
            names: {
              en:
                'Individuals who did not attempt to obtain legal assistance because they thought the problem was not difficult',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AJ_TNALA_NIP',
                type: 'code',
              },
            ],
          },
          {
            id: 'AJ_TNALA_AB',
            name: 'Individuals who did not attempt to obtain legal assistance because of the access barrier',
            names: {
              en: 'Individuals who did not attempt to obtain legal assistance because of the access barrier',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AJ_TNALA_AB',
                type: 'code',
              },
            ],
          },
          {
            id: 'AJ_TNALA_OT',
            name: 'Individuals who did not attempt to obtain legal assistance for other reasons',
            names: {
              en: 'Individuals who did not attempt to obtain legal assistance for other reasons',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AJ_TNALA_OT',
                type: 'code',
              },
            ],
          },
          {
            id: 'AJ_RNA_SS',
            name:
              'Individuals who did not take any action to resolve a dispute as they sought to find a solution on their own',
            names: {
              en:
                'Individuals who did not take any action to resolve a dispute as they sought to find a solution on their own',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AJ_RNA_SS',
                type: 'code',
              },
            ],
          },
          {
            id: 'AJ_RNA_NI',
            name:
              'Individuals who did not take any action to resolve a dispute as it was not important or not easy to solve',
            names: {
              en:
                'Individuals who did not take any action to resolve a dispute as it was not important or not easy to solve',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AJ_RNA_NI',
                type: 'code',
              },
            ],
          },
          {
            id: 'AJ_RNA_LP',
            name: 'Individuals who did not take any action to resolve a dispute as it was a lenghty process',
            names: {
              en: 'Individuals who did not take any action to resolve a dispute as it was a lenghty process',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).AJ_RNA_LP',
                type: 'code',
              },
            ],
          },
          {
            id: 'RH_PINVT_RD',
            name:
              'Individuals who felt that regular doctor involved them as much as they wanted in decisions about treatment or care',
            names: {
              en:
                'Individuals who felt that regular doctor involved them as much as they wanted in decisions about treatment or care',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RH_PINVT_RD',
                type: 'code',
              },
            ],
          },
          {
            id: 'RH_DS_2M',
            name: 'Individuals who waited for a doctor specialist appointment two months or longer',
            names: {
              en: 'Individuals who waited for a doctor specialist appointment two months or longer',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RH_DS_2M',
                type: 'code',
              },
            ],
          },
          {
            id: 'RH_HF_AH',
            name: 'Individuals who had hip structure surgery initiated after admission to hospital',
            names: {
              en: 'Individuals who had hip structure surgery initiated after admission to hospital',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RH_HF_AH',
                type: 'code',
              },
            ],
          },
          {
            id: 'RE_EL',
            name: 'Early leavers from education and training who are not currently working',
            names: {
              en: 'Early leavers from education and training who are not currently working',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RE_EL',
                type: 'code',
              },
            ],
          },
          {
            id: 'RE_SQTSN_LSE_SD',
            name:
              'Shortage of qualified teachers in teaching students with specific needs (socioeconomic disadvantage)',
            names: {
              en:
                'Shortage of qualified teachers in teaching students with specific needs (socioeconomic disadvantage)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RE_SQTSN_LSE_SD',
                type: 'code',
              },
            ],
          },
          {
            id: 'RE_SQTSN_LSE_MS',
            name: 'Shortage of qualified teachers in teaching students with specific needs (multicultural setting)',
            names: {
              en: 'Shortage of qualified teachers in teaching students with specific needs (multicultural setting)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RE_SQTSN_LSE_MS',
                type: 'code',
              },
            ],
          },
          {
            id: 'RE_SQTSN_LSE_SN',
            name: 'Shortage of qualified teachers in teaching students with specific needs (special needs)',
            names: {
              en: 'Shortage of qualified teachers in teaching students with specific needs (special needs)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RE_SQTSN_LSE_SN',
                type: 'code',
              },
            ],
          },
          {
            id: 'RE_DSL_FA_LSE',
            name: 'Decisions taken at the school levels in public education fully autonomously',
            names: {
              en: 'Decisions taken at the school levels in public education fully autonomously',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RE_DSL_FA_LSE',
                type: 'code',
              },
            ],
          },
          {
            id: 'RE_DSL_AC_LSE',
            name: 'Decisions taken at the school levels in public education after consultation with other bodies',
            names: {
              en: 'Decisions taken at the school levels in public education after consultation with other bodies',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RE_DSL_AC_LSE',
                type: 'code',
              },
            ],
          },
          {
            id: 'RE_DSL_FHA_LSE',
            name: 'Decisions taken at the school levels in public education within framework set by a higher authority',
            names: {
              en: 'Decisions taken at the school levels in public education within framework set by a higher authority',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RE_DSL_FHA_LSE',
                type: 'code',
              },
            ],
          },
          {
            id: 'RE_DSL_OTL_LSE',
            name: 'Decisions taken at other levels in public education after consulting the school',
            names: {
              en: 'Decisions taken at other levels in public education after consulting the school',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RE_DSL_OTL_LSE',
                type: 'code',
              },
            ],
          },
          {
            id: 'RE_DSL_OT_LSE',
            name: 'Decisions taken at the school levels in public education, other',
            names: {
              en: 'Decisions taken at the school levels in public education, other',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RE_DSL_OT_LSE',
                type: 'code',
              },
            ],
          },
          {
            id: 'RJ_T_CCA',
            name: 'Time needed to resolve civil, commercial, administrative and other cases',
            names: {
              en: 'Time needed to resolve civil, commercial, administrative and other cases',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RJ_T_CCA',
                type: 'code',
              },
            ],
          },
          {
            id: 'RJ_T_CC',
            name: 'Time needed to resolve litigious civil and commercial cases',
            names: {
              en: 'Time needed to resolve litigious civil and commercial cases',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RJ_T_CC',
                type: 'code',
              },
            ],
          },
          {
            id: 'RJ_T_A',
            name: 'Time needed to resolve administrative cases',
            names: {
              en: 'Time needed to resolve administrative cases',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).RJ_T_A',
                type: 'code',
              },
            ],
          },
          {
            id: 'QH_30M_AMI',
            name: 'Thirty-day mortality after admission to hospital for AMI',
            names: {
              en: 'Thirty-day mortality after admission to hospital for AMI',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).QH_30M_AMI',
                type: 'code',
              },
            ],
          },
          {
            id: 'QH_ADMRASTH',
            name: 'Asthma hospital admission in adults',
            names: {
              en: 'Asthma hospital admission in adults',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).QH_ADMRASTH',
                type: 'code',
              },
            ],
          },
          {
            id: 'QH_ADMRCOPD',
            name: 'COPD hospital admission in adults',
            names: {
              en: 'COPD hospital admission in adults',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).QH_ADMRCOPD',
                type: 'code',
              },
            ],
          },
          {
            id: 'QH_CHF',
            name: 'Congestive heart failure (CHF) hospital admission in adults',
            names: {
              en: 'Congestive heart failure (CHF) hospital admission in adults',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).QH_CHF',
                type: 'code',
              },
            ],
          },
          {
            id: 'PEE_PIRLS',
            name: 'Average Progress in International Reading Literacy Study (PIRLS) score',
            names: {
              en: 'Average Progress in International Reading Literacy Study (PIRLS) score',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).PEE_PIRLS',
                type: 'code',
              },
            ],
          },
          {
            id: 'PEE_PIRLS_SASHSP',
            name:
              'Students who attend schools where over 75% of the students enter with some reading and writing skills',
            names: {
              en:
                'Students who attend schools where over 75% of the students enter with some reading and writing skills',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).PEE_PIRLS_SASHSP',
                type: 'code',
              },
            ],
          },
          {
            id: 'PEE_PIRLS_SSHSP',
            name:
              'PIRLS score for highly skilled students who attend a school where over 75% of the students enter with some reading and writing skills',
            names: {
              en:
                'PIRLS score for highly skilled students who attend a school where over 75% of the students enter with some reading and writing skills',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).PEE_PIRLS_SSHSP',
                type: 'code',
              },
            ],
          },
          {
            id: 'PEE_PIRLS_HB',
            name: 'PIRLS score for students with high sense of belonging in school',
            names: {
              en: 'PIRLS score for students with high sense of belonging in school',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).PEE_PIRLS_HB',
                type: 'code',
              },
            ],
          },
          {
            id: 'PEE_PIRLS_SB',
            name: 'PIRLS score for students with some sense of belonging in school',
            names: {
              en: 'PIRLS score for students with some sense of belonging in school',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).PEE_PIRLS_SB',
                type: 'code',
              },
            ],
          },
          {
            id: 'PEE_PIRLS_LB',
            name: 'PIRLS score for students with little sense of belonging in school',
            names: {
              en: 'PIRLS score for students with little sense of belonging in school',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).PEE_PIRLS_LB',
                type: 'code',
              },
            ],
          },
          {
            id: 'PEE_PIRLS_PERHB',
            name: 'Students with high sense of belonging in school',
            names: {
              en: 'Students with high sense of belonging in school',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).PEE_PIRLS_PERHB',
                type: 'code',
              },
            ],
          },
          {
            id: 'EFJ_RLCJ_EE',
            name: 'Rule of law in Civil justice: Civil justice is effectively enforced',
            names: {
              en: 'Rule of law in Civil justice: Civil justice is effectively enforced',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).EFJ_RLCJ_EE',
                type: 'code',
              },
            ],
          },
          {
            id: 'EFJ_RLCJ_FGI',
            name: 'Rule of law in Civil justice: Civil justice is free of improper government influence',
            names: {
              en: 'Rule of law in Civil justice: Civil justice is free of improper government influence',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).EFJ_RLCJ_FGI',
                type: 'code',
              },
            ],
          },
          {
            id: 'EFJ_RLCJ_CATE',
            name: 'Rule of law in Civil justice: Criminal adjudication system is timely and effective',
            names: {
              en: 'Rule of law in Civil justice: Criminal adjudication system is timely and effective',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).EFJ_RLCJ_CATE',
                type: 'code',
              },
            ],
          },
          {
            id: 'EFJ_RLOS_PNRV',
            name: 'Rule of law in Order and security: People do not resort to violence to redress personal grievances',
            names: {
              en: 'Rule of law in Order and security: People do not resort to violence to redress personal grievances',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).EFJ_RLOS_PNRV',
                type: 'code',
              },
            ],
          },
          {
            id: 'EFJ_RLOS_CEC',
            name: 'Rule of law in Order and security: Crime is effectively controlled',
            names: {
              en: 'Rule of law in Order and security: Crime is effectively controlled',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_MEASURE(1.0).EFJ_RLOS_CEC',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_SEX',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.GOV:CL_SEX(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD.GOV',
        isFinal: false,
        name: 'Codelist for sex',
        names: {
          en: 'Codelist for sex',
        },
        isPartial: true,
        codes: [
          {
            id: 'F',
            name: 'Female',
            names: {
              en: 'Female',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_SEX(1.0).F',
                type: 'code',
              },
            ],
          },
          {
            id: 'M',
            name: 'Male',
            names: {
              en: 'Male',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_SEX(1.0).M',
                type: 'code',
              },
            ],
          },
          {
            id: '_T',
            name: 'Total',
            names: {
              en: 'Total',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD.GOV:CL_SEX(1.0)._T',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_UNIT_MEASURE',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD_GOV:CL_UNIT_MEASURE(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD_GOV',
        isFinal: false,
        name: 'Code list for unit of measure',
        names: {
          en: 'Code list for unit of measure',
        },
        isPartial: true,
        codes: [
          {
            id: 'PCT_POP',
            name: 'Percentage of population',
            names: {
              en: 'Percentage of population',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).PCT_POP',
                type: 'code',
              },
            ],
          },
          {
            id: 'PCT_POP_SUB',
            name: 'Percentage of population in the same subgroup',
            names: {
              en: 'Percentage of population in the same subgroup',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).PCT_POP_SUB',
                type: 'code',
              },
            ],
          },
          {
            id: 'PCT_HH_F_CONS',
            name: 'Percentage of final household consumption',
            names: {
              en: 'Percentage of final household consumption',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).PCT_HH_F_CONS',
                type: 'code',
              },
            ],
          },
          {
            id: 'PCT_EDU_EXP',
            name: 'Percentage of total education expenditures',
            names: {
              en: 'Percentage of total education expenditures',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).PCT_EDU_EXP',
                type: 'code',
              },
            ],
          },
          {
            id: 'PCT_PAT',
            name: 'Percentage of patients',
            names: {
              en: 'Percentage of patients',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).PCT_PAT',
                type: 'code',
              },
            ],
          },
          {
            id: 'PCT_SCH',
            name: 'Percentage of schools',
            names: {
              en: 'Percentage of schools',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).PCT_SCH',
                type: 'code',
              },
            ],
          },
          {
            id: 'PCT_STUD',
            name: 'Percentage of students',
            names: {
              en: 'Percentage of students',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).PCT_STUD',
                type: 'code',
              },
            ],
          },
          {
            id: 'PCT_DEC',
            name: 'Percentage of all decisions',
            names: {
              en: 'Percentage of all decisions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).PCT_DEC',
                type: 'code',
              },
            ],
          },
          {
            id: 'PCT_ADM',
            name: 'Percentage of admissions',
            names: {
              en: 'Percentage of admissions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).PCT_ADM',
                type: 'code',
              },
            ],
          },
          {
            id: 'NUMBER_100000_POP',
            name: 'Number per 100 000 population',
            names: {
              en: 'Number per 100 000 population',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).NUMBER_100000_POP',
                type: 'code',
              },
            ],
          },
          {
            id: 'POINTS',
            name: 'Points',
            names: {
              en: 'Points',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).POINTS',
                type: 'code',
              },
            ],
          },
          {
            id: 'INDEX',
            name: 'Index',
            names: {
              en: 'Index',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).INDEX',
                type: 'code',
              },
            ],
          },
          {
            id: 'DAYS',
            name: 'Days',
            names: {
              en: 'Days',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD_GOV:CL_UNIT_MEASURE(1.0).DAYS',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_FREQ',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_FREQ(2.0)',
            type: 'codelist',
          },
        ],
        version: '2.0',
        agencyID: 'SDMX',
        isFinal: true,
        name: 'Frequency',
        names: {
          en: 'Frequency',
        },
        description:
          'This code list provides a set of values indicating the "frequency" of the data (e.g. weekly, monthly, quarterly). The concept “frequency” may refer to various stages in the production process, e.g. data collection or data dissemination. For example, a time series could be disseminated at annual frequency but the underlying data are compiled monthly. The code list is applicable for all different uses of "frequency". This code list was formally adopted on 4 December 2013. More information about and supporting material for this code list and SDMX code lists in general (e.g. list of generic codes for expressing general concepts like "Total", "Unknown", etc.; syntaxes for the creation of further codes; general guidelines for the creation of SDMX code lists) can be found at this address: https://sdmx.org/?page_id=4345.',
        descriptions: {
          en:
            'This code list provides a set of values indicating the "frequency" of the data (e.g. weekly, monthly, quarterly). The concept “frequency” may refer to various stages in the production process, e.g. data collection or data dissemination. For example, a time series could be disseminated at annual frequency but the underlying data are compiled monthly. The code list is applicable for all different uses of "frequency". This code list was formally adopted on 4 December 2013. More information about and supporting material for this code list and SDMX code lists in general (e.g. list of generic codes for expressing general concepts like "Total", "Unknown", etc.; syntaxes for the creation of further codes; general guidelines for the creation of SDMX code lists) can be found at this address: https://sdmx.org/?page_id=4345.',
        },
        isPartial: true,
        codes: [
          {
            id: 'A',
            name: 'Annual',
            names: {
              en: 'Annual',
            },
            description: 'To be used for data collected or disseminated every year.',
            descriptions: {
              en: 'To be used for data collected or disseminated every year.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_FREQ(2.0).A',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_OBS_STATUS',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_OBS_STATUS(2.1)',
            type: 'codelist',
          },
        ],
        version: '2.1',
        agencyID: 'SDMX',
        isFinal: true,
        name: 'Observation status',
        names: {
          en: 'Observation status',
        },
        isPartial: false,
        codes: [
          {
            id: 'A',
            name: 'Normal value',
            names: {
              en: 'Normal value',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '1',
                texts: {
                  en: '1',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).A',
                type: 'code',
              },
            ],
          },
          {
            id: 'B',
            name: 'Time series break',
            names: {
              en: 'Time series break',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '2',
                texts: {
                  en: '2',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).B',
                type: 'code',
              },
            ],
          },
          {
            id: 'D',
            name: 'Definition differs',
            names: {
              en: 'Definition differs',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '3',
                texts: {
                  en: '3',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).D',
                type: 'code',
              },
            ],
          },
          {
            id: 'E',
            name: 'Estimated value',
            names: {
              en: 'Estimated value',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '4',
                texts: {
                  en: '4',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).E',
                type: 'code',
              },
            ],
          },
          {
            id: 'F',
            name: 'Forecast value',
            names: {
              en: 'Forecast value',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '5',
                texts: {
                  en: '5',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).F',
                type: 'code',
              },
            ],
          },
          {
            id: 'G',
            name: 'Experimental value',
            names: {
              en: 'Experimental value',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '6',
                texts: {
                  en: '6',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).G',
                type: 'code',
              },
            ],
          },
          {
            id: 'I',
            name: 'Imputed value (CCSA definition)',
            names: {
              en: 'Imputed value (CCSA definition)',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '7',
                texts: {
                  en: '7',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).I',
                type: 'code',
              },
            ],
          },
          {
            id: 'O',
            name: 'Missing value',
            names: {
              en: 'Missing value',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '8',
                texts: {
                  en: '8',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).O',
                type: 'code',
              },
            ],
          },
          {
            id: 'M',
            name: 'Missing value',
            names: {
              en: 'Missing value',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '9',
                texts: {
                  en: '9',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).M',
                type: 'code',
              },
            ],
          },
          {
            id: 'P',
            name: 'Provisional value',
            names: {
              en: 'Provisional value',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '10',
                texts: {
                  en: '10',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).P',
                type: 'code',
              },
            ],
          },
          {
            id: 'S',
            name: 'Strike and other special events',
            names: {
              en: 'Strike and other special events',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '11',
                texts: {
                  en: '11',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).S',
                type: 'code',
              },
            ],
          },
          {
            id: 'L',
            name: 'Missing value',
            names: {
              en: 'Missing value',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '12',
                texts: {
                  en: '12',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).L',
                type: 'code',
              },
            ],
          },
          {
            id: 'H',
            name: 'Missing value',
            names: {
              en: 'Missing value',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '13',
                texts: {
                  en: '13',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).H',
                type: 'code',
              },
            ],
          },
          {
            id: 'Q',
            name: 'Missing value',
            names: {
              en: 'Missing value',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '14',
                texts: {
                  en: '14',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).Q',
                type: 'code',
              },
            ],
          },
          {
            id: 'J',
            name: 'Derogation',
            names: {
              en: 'Derogation',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '15',
                texts: {
                  en: '15',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).J',
                type: 'code',
              },
            ],
          },
          {
            id: 'N',
            name: 'Not significant',
            names: {
              en: 'Not significant',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '16',
                texts: {
                  en: '16',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).N',
                type: 'code',
              },
            ],
          },
          {
            id: 'U',
            name: 'Low reliability',
            names: {
              en: 'Low reliability',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '17',
                texts: {
                  en: '17',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).U',
                type: 'code',
              },
            ],
          },
          {
            id: 'V',
            name: 'Unvalidated value',
            names: {
              en: 'Unvalidated value',
            },
            annotations: [
              {
                type: '@ORDER@',
                text: '18',
                texts: {
                  en: '18',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_OBS_STATUS(2.1).V',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_UNIT_MULT',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_UNIT_MULT(1.1)',
            type: 'codelist',
          },
        ],
        version: '1.1',
        agencyID: 'SDMX',
        isFinal: true,
        name: 'Unit Multiplier',
        names: {
          en: 'Unit Multiplier',
        },
        description:
          'This code list provides code values for indicating the magnitude in the units of measurement. More information about this code list and SDMX code lists in general (e.g. list of generic codes for expressing general concepts like "Total", "Unknown", etc.; syntaxes for the creation of further codes; general guidelines for the creation of SDMX code lists) can be found at this address: https://sdmx.org/?page_id=4345.',
        descriptions: {
          en:
            'This code list provides code values for indicating the magnitude in the units of measurement. More information about this code list and SDMX code lists in general (e.g. list of generic codes for expressing general concepts like "Total", "Unknown", etc.; syntaxes for the creation of further codes; general guidelines for the creation of SDMX code lists) can be found at this address: https://sdmx.org/?page_id=4345.',
        },
        isPartial: false,
        codes: [
          {
            id: '0',
            name: 'Units',
            names: {
              en: 'Units',
            },
            description: 'In scientific notation, expressed as ten raised to the power of zero (10^0)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of zero (10^0)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).0',
                type: 'code',
              },
            ],
          },
          {
            id: '1',
            name: 'Tens',
            names: {
              en: 'Tens',
            },
            description: 'In scientific notation, expressed as ten raised to the power of one (10^1)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of one (10^1)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).1',
                type: 'code',
              },
            ],
          },
          {
            id: '2',
            name: 'Hundreds',
            names: {
              en: 'Hundreds',
            },
            description: 'In scientific notation, expressed as ten raised to the power of two (10^2)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of two (10^2)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).2',
                type: 'code',
              },
            ],
          },
          {
            id: '3',
            name: 'Thousands',
            names: {
              en: 'Thousands',
            },
            description: 'In scientific notation, expressed as ten raised to the power of three (10^3)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of three (10^3)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).3',
                type: 'code',
              },
            ],
          },
          {
            id: '4',
            name: 'Tens of thousands',
            names: {
              en: 'Tens of thousands',
            },
            description: 'In scientific notation, expressed as ten raised to the power of four (10^4)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of four (10^4)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).4',
                type: 'code',
              },
            ],
          },
          {
            id: '5',
            name: 'Hundreds of thousands',
            names: {
              en: 'Hundreds of thousands',
            },
            description: 'In scientific notation, expressed as ten raised to the power of five(10^5)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of five(10^5)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).5',
                type: 'code',
              },
            ],
          },
          {
            id: '6',
            name: 'Millions',
            names: {
              en: 'Millions',
            },
            description: 'In scientific notation, expressed as ten raised to the power of six (10^6)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of six (10^6)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).6',
                type: 'code',
              },
            ],
          },
          {
            id: '7',
            name: 'Tens of millions',
            names: {
              en: 'Tens of millions',
            },
            description: 'In scientific notation, expressed as ten raised to the power of seven (10^7)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of seven (10^7)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).7',
                type: 'code',
              },
            ],
          },
          {
            id: '8',
            name: 'Hundreds of millions',
            names: {
              en: 'Hundreds of millions',
            },
            description: 'In scientific notation, expressed as ten raised to the power of eight (10^8)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of eight (10^8)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).8',
                type: 'code',
              },
            ],
          },
          {
            id: '9',
            name: 'Billions',
            names: {
              en: 'Billions',
            },
            description: 'In scientific notation, expressed as ten raised to the power of nine (10^9)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of nine (10^9)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).9',
                type: 'code',
              },
            ],
          },
          {
            id: '10',
            name: 'Tens of billions',
            names: {
              en: 'Tens of billions',
            },
            description: 'In scientific notation, expressed as ten raised to the power of ten (10^10)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of ten (10^10)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).10',
                type: 'code',
              },
            ],
          },
          {
            id: '11',
            name: 'Hundreds of billions',
            names: {
              en: 'Hundreds of billions',
            },
            description: 'In scientific notation, expressed as ten raised to the power of eleven (10^11)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of eleven (10^11)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).11',
                type: 'code',
              },
            ],
          },
          {
            id: '12',
            name: 'Trillions',
            names: {
              en: 'Trillions',
            },
            description: 'In scientific notation, expressed as ten raised to the power of twelve (10^12)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of twelve (10^12)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).12',
                type: 'code',
              },
            ],
          },
          {
            id: '13',
            name: 'Tens of trillions',
            names: {
              en: 'Tens of trillions',
            },
            description: 'In scientific notation, expressed as ten raised to the power of thirteen (10^13)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of thirteen (10^13)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).13',
                type: 'code',
              },
            ],
          },
          {
            id: '14',
            name: 'Hundreds of trillions',
            names: {
              en: 'Hundreds of trillions',
            },
            description: 'In scientific notation, expressed as ten raised to the power of fourteen (10^14)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of fourteen (10^14)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).14',
                type: 'code',
              },
            ],
          },
          {
            id: '15',
            name: 'Quadrillions',
            names: {
              en: 'Quadrillions',
            },
            description: 'In scientific notation, expressed as ten raised to the power of fifteen (10^15)',
            descriptions: {
              en: 'In scientific notation, expressed as ten raised to the power of fifteen (10^15)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=SDMX:CL_UNIT_MULT(1.1).15',
                type: 'code',
              },
            ],
          },
        ],
      },
    ],
    dataStructures: [
      {
        id: 'DSD_GOV_8_9',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD.GOV:DSD_GOV_8_9(2.0)',
            type: 'datastructure',
          },
        ],
        version: '2.0',
        agencyID: 'OECD.GOV',
        isFinal: false,
        name: 'GOV DSD',
        names: {
          en: 'GOV DSD',
        },
        description: 'DSD for GOV chapters 8 and 9',
        descriptions: {
          en: 'DSD for GOV chapters 8 and 9',
        },
        dataStructureComponents: {
          attributeList: {
            id: 'AttributeDescriptor',
            links: [
              {
                rel: 'self',
                urn:
                  'urn:sdmx:org.sdmx.infomodel.datastructure.AttributeDescriptor=OECD.GOV:DSD_GOV_8_9(2.0).AttributeDescriptor',
                type: 'attributedescriptor',
              },
            ],
            attributes: [
              {
                id: 'UNIT_MULT',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=OECD.GOV:DSD_GOV_8_9(2.0).UNIT_MULT',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity:
                  'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).UNIT_MULT',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_UNIT_MULT(1.1)',
                },
                assignmentStatus: 'Mandatory',
                attributeRelationship: {
                  primaryMeasure: 'OBS_VALUE',
                },
              },
              {
                id: 'OBS_STATUS',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=OECD.GOV:DSD_GOV_8_9(2.0).OBS_STATUS',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity:
                  'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).OBS_STATUS',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_OBS_STATUS(2.1)',
                  textFormat: {
                    textType: 'String',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: {
                  primaryMeasure: 'OBS_VALUE',
                },
              },
            ],
          },
          dimensionList: {
            id: 'DimensionDescriptor',
            links: [
              {
                rel: 'self',
                urn:
                  'urn:sdmx:org.sdmx.infomodel.datastructure.DimensionDescriptor=OECD.GOV:DSD_GOV_8_9(2.0).DimensionDescriptor',
                type: 'dimensiondescriptor',
              },
            ],
            dimensions: [
              {
                id: 'FREQ',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.GOV:DSD_GOV_8_9(2.0).FREQ',
                    type: 'dimension',
                  },
                ],
                position: 0,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).FREQ',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=SDMX:CL_FREQ(2.0)',
                  textFormat: {
                    textType: 'String',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
              },
              {
                id: 'MEASURE',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.GOV:DSD_GOV_8_9(2.0).MEASURE',
                    type: 'dimension',
                  },
                ],
                position: 1,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).MEASURE',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.GOV:CL_MEASURE(1.0)',
                  textFormat: {
                    textType: 'String',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
              },
              {
                id: 'REF_AREA',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.GOV:DSD_GOV_8_9(2.0).REF_AREA',
                    type: 'dimension',
                  },
                ],
                position: 2,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).REF_AREA',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.GOV:CL_AREA(1.0)',
                  textFormat: {
                    textType: 'String',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
              },
              {
                id: 'AGE',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.GOV:DSD_GOV_8_9(2.0).AGE',
                    type: 'dimension',
                  },
                ],
                position: 3,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).AGE',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.GOV:CL_AGE(1.0)',
                  textFormat: {
                    textType: 'String',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
              },
              {
                id: 'SEX',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.GOV:DSD_GOV_8_9(2.0).SEX',
                    type: 'dimension',
                  },
                ],
                position: 4,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).SEX',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.GOV:CL_SEX(1.0)',
                  textFormat: {
                    textType: 'String',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
              },
              {
                id: 'EDUCATION_LEV',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.GOV:DSD_GOV_8_9(2.0).EDUCATION_LEV',
                    type: 'dimension',
                  },
                ],
                position: 5,
                type: 'Dimension',
                conceptIdentity:
                  'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).EDUCATION_LEV',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.GOV:CL_EDUCATION_LEV(1.0)',
                  textFormat: {
                    textType: 'String',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
              },
              {
                id: 'CATEGORY',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.GOV:DSD_GOV_8_9(2.0).CATEGORY',
                    type: 'dimension',
                  },
                ],
                position: 6,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).CATEGORY',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD.GOV:CL_CATEGORY(1.0)',
                  textFormat: {
                    textType: 'String',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
              },
              {
                id: 'UNIT_MEASURE',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD.GOV:DSD_GOV_8_9(2.0).UNIT_MEASURE',
                    type: 'dimension',
                  },
                ],
                position: 7,
                type: 'Dimension',
                conceptIdentity:
                  'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).UNIT_MEASURE',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD_GOV:CL_UNIT_MEASURE(1.0)',
                  textFormat: {
                    textType: 'String',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
              },
            ],
            timeDimensions: [
              {
                id: 'TIME_PERIOD',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.TimeDimension=OECD.GOV:DSD_GOV_8_9(2.0).TIME_PERIOD',
                    type: 'timedimension',
                  },
                ],
                position: 8,
                type: 'TimeDimension',
                conceptIdentity:
                  'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).TIME_PERIOD',
                localRepresentation: {
                  textFormat: {
                    textType: 'DateTime',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
              },
            ],
          },
          measureList: {
            id: 'MeasureDescriptor',
            links: [
              {
                rel: 'self',
                urn:
                  'urn:sdmx:org.sdmx.infomodel.datastructure.MeasureDescriptor=OECD.GOV:DSD_GOV_8_9(2.0).MeasureDescriptor',
                type: 'measuredescriptor',
              },
            ],
            primaryMeasure: {
              id: 'OBS_VALUE',
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.PrimaryMeasure=OECD.GOV:DSD_GOV_8_9(2.0).OBS_VALUE',
                  type: 'primarymeasure',
                },
              ],
              conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD.GOV:CS_GOV_TEST(1.1).OBS_VALUE',
              localRepresentation: {
                textFormat: {
                  textType: 'Double',
                  isSequence: false,
                  isMultiLingual: false,
                },
              },
            },
          },
        },
      },
    ],
    contentConstraints: [
      {
        id: 'CR_A_DF_GOV_SERVING_CITIZENS',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.registry.ContentConstraint=OECD.GOV:CR_A_DF_GOV_SERVING_CITIZENS(2.0)',
            type: 'contentconstraint',
          },
        ],
        version: '2.0',
        agencyID: 'OECD.GOV',
        isFinal: false,
        name: 'Availability (A) for DF_GOV_SERVING_CITIZENS',
        names: {
          en: 'Availability (A) for DF_GOV_SERVING_CITIZENS',
        },
        annotations: [
          {
            title: 'A',
            type: 'ReleaseVersion',
          },
        ],
        type: 'Actual',
        constraintAttachment: {
          dataflows: ['urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.GOV:DF_GOV_SERVING_CITIZENS(2.0)'],
        },
        cubeRegions: [
          {
            isIncluded: true,
            keyValues: [
              {
                id: 'FREQ',
                values: ['A'],
              },
              {
                id: 'MEASURE',
                values: [
                  'AE_ENR',
                  'AE_ER_EXIS',
                  'AE_ER_T',
                  'AE_PE',
                  'AH_OOPME',
                  'AH_UNMETN_HI',
                  'AH_UNMETN_LI',
                  'AH_UNMETN_NA',
                  'AJ_I_AD',
                  'AJ_I_LA',
                  'AJ_RNA_LP',
                  'AJ_RNA_NI',
                  'AJ_RNA_SS',
                  'AJ_TNALA_AB',
                  'AJ_TNALA_NIP',
                  'AJ_TNALA_OT',
                  'CONF_JS',
                  'CONF_LPS',
                  'CS_ES',
                  'CS_HC',
                  'EFJ_RLCJ_CATE',
                  'EFJ_RLCJ_EE',
                  'EFJ_RLCJ_FGI',
                  'EFJ_RLOS_CEC',
                  'EFJ_RLOS_PNRV',
                  'PEE_PIRLS',
                  'PEE_PIRLS_HB',
                  'PEE_PIRLS_LB',
                  'PEE_PIRLS_PERHB',
                  'PEE_PIRLS_SASHSP',
                  'PEE_PIRLS_SB',
                  'PEE_PIRLS_SSHSP',
                  'QH_30M_AMI',
                  'QH_ADMRASTH',
                  'QH_ADMRCOPD',
                  'QH_CHF',
                  'RE_DSL_AC_LSE',
                  'RE_DSL_FA_LSE',
                  'RE_DSL_FHA_LSE',
                  'RE_DSL_OT_LSE',
                  'RE_DSL_OTL_LSE',
                  'RE_EL',
                  'RE_SQTSN_LSE_MS',
                  'RE_SQTSN_LSE_SD',
                  'RE_SQTSN_LSE_SN',
                  'RH_DS_2M',
                  'RH_HF_AH',
                  'RH_PINVT_RD',
                  'RJ_T_A',
                  'RJ_T_CC',
                  'RJ_T_CCA',
                ],
              },
              {
                id: 'REF_AREA',
                values: [
                  'AT',
                  'AU',
                  'BE',
                  'BR',
                  'CA',
                  'CH',
                  'CL',
                  'CN',
                  'CO',
                  'CR',
                  'CZ',
                  'DE',
                  'DK',
                  'EE',
                  'ES',
                  'FI',
                  'FR',
                  'GB',
                  'GR',
                  'HU',
                  'ID',
                  'IE',
                  'IL',
                  'IN',
                  'IS',
                  'IT',
                  'JP',
                  'KR',
                  'LT',
                  'LU',
                  'LV',
                  'MX',
                  'NL',
                  'NO',
                  'NZ',
                  'OECDAVG',
                  'OECDEU',
                  'PL',
                  'PT',
                  'RU',
                  'SE',
                  'SI',
                  'SK',
                  'TR',
                  'US',
                  'ZA',
                ],
              },
              {
                id: 'AGE',
                values: ['_T', 'Y_GE15', 'Y_GE45', 'Y_GE65', 'Y_LT25', 'Y18T24', 'Y3', 'Y4'],
              },
              {
                id: 'SEX',
                values: ['_T'],
              },
              {
                id: 'EDUCATION_LEV',
                values: ['_T', 'ISCED11_0_1', 'ISCED11_02', 'ISCED11_1T4', 'ISCED11_2', 'ISCED11_5T8'],
              },
              {
                id: 'CATEGORY',
                values: ['AFF', 'COU', 'CSD', 'EDS', 'INF', 'MOS', 'SEC', 'TIM'],
              },
              {
                id: 'UNIT_MEASURE',
                values: [
                  'DAYS',
                  'INDEX',
                  'NUMBER_100000_POP',
                  'PCT_ADM',
                  'PCT_DEC',
                  'PCT_EDU_EXP',
                  'PCT_HH_F_CONS',
                  'PCT_PAT',
                  'PCT_POP',
                  'PCT_POP_SUB',
                  'PCT_SCH',
                  'PCT_STUD',
                  'POINTS',
                ],
              },
              {
                id: 'TIME_PERIOD',
                timeRange: {
                  startPeriod: {
                    period: '2007-01-01T00:00:00Z',
                    isInclusive: true,
                  },
                  endPeriod: {
                    period: '2019-12-31T23:59:59Z',
                    isInclusive: true,
                  },
                },
              },
            ],
          },
        ],
      },
      {
        id: 'CR_DF_GOV_SERVING_CITIZENS',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.registry.ContentConstraint=OECD.GOV:CR_DF_GOV_SERVING_CITIZENS(2.0)',
            type: 'contentconstraint',
          },
        ],
        version: '2.0',
        agencyID: 'OECD.GOV',
        isFinal: false,
        name: 'CR_DF_GOV_SERVING_CITIZENS',
        names: {
          en: 'CR_DF_GOV_SERVING_CITIZENS',
        },
        type: 'Allowed',
        constraintAttachment: {
          dataflows: ['urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD.GOV:DF_GOV_SERVING_CITIZENS(2.0)'],
        },
        cubeRegions: [
          {
            isIncluded: true,
            keyValues: [
              {
                id: 'REF_AREA',
                values: [
                  'AT',
                  'AU',
                  'BE',
                  'BG',
                  'BR',
                  'CA',
                  'CH',
                  'CL',
                  'CN',
                  'CO',
                  'CR',
                  'CY',
                  'CZ',
                  'DE',
                  'DK',
                  'EE',
                  'ES',
                  'FI',
                  'FR',
                  'GB',
                  'GR',
                  'HR',
                  'HU',
                  'ID',
                  'IE',
                  'IL',
                  'IN',
                  'IS',
                  'IT',
                  'JP',
                  'KR',
                  'LT',
                  'LU',
                  'LV',
                  'MT',
                  'MX',
                  'NL',
                  'NO',
                  'NZ',
                  'OECDAVG',
                  'OECDEU',
                  'PL',
                  'PT',
                  'RO',
                  'RU',
                  'SE',
                  'SI',
                  'SK',
                  'TR',
                  'US',
                  'ZA',
                ],
              },
              {
                id: 'UNIT_MEASURE',
                values: [
                  'DAYS',
                  'INDEX',
                  'NUMBER_100000_POP',
                  'PCT_ADM',
                  'PCT_DEC',
                  'PCT_EDU_EXP',
                  'PCT_HH_F_CONS',
                  'PCT_PAT',
                  'PCT_POP',
                  'PCT_POP_SUB',
                  'PCT_SCH',
                  'PCT_STUD',
                  'POINTS',
                ],
              },
              {
                id: 'MEASURE',
                values: [
                  'AE_ENR',
                  'AE_ER_EXIS',
                  'AE_ER_T',
                  'AE_PE',
                  'AH_OOPME',
                  'AH_UNMETN_HI',
                  'AH_UNMETN_LI',
                  'AH_UNMETN_NA',
                  'AJ_I_AD',
                  'AJ_I_LA',
                  'AJ_RNA_LP',
                  'AJ_RNA_NI',
                  'AJ_RNA_SS',
                  'AJ_TNALA_AB',
                  'AJ_TNALA_NIP',
                  'AJ_TNALA_OT',
                  'CONF_JS',
                  'CONF_LPS',
                  'CS_ES',
                  'CS_HC',
                  'EFJ_RLCJ_CATE',
                  'EFJ_RLCJ_EE',
                  'EFJ_RLCJ_FGI',
                  'EFJ_RLOS_CEC',
                  'EFJ_RLOS_PNRV',
                  'PEE_PIRLS',
                  'PEE_PIRLS_HB',
                  'PEE_PIRLS_LB',
                  'PEE_PIRLS_PERHB',
                  'PEE_PIRLS_SASHSP',
                  'PEE_PIRLS_SB',
                  'PEE_PIRLS_SSHSP',
                  'QH_30M_AMI',
                  'QH_ADMRASTH',
                  'QH_ADMRCOPD',
                  'QH_CHF',
                  'RE_DSL_AC_LSE',
                  'RE_DSL_FA_LSE',
                  'RE_DSL_FHA_LSE',
                  'RE_DSL_OT_LSE',
                  'RE_DSL_OTL_LSE',
                  'RE_EL',
                  'RE_SQTSN_LSE_MS',
                  'RE_SQTSN_LSE_SD',
                  'RE_SQTSN_LSE_SN',
                  'RH_DS_2M',
                  'RH_HF_AH',
                  'RH_PINVT_RD',
                  'RJ_T_A',
                  'RJ_T_CC',
                  'RJ_T_CCA',
                ],
              },
              {
                id: 'SEX',
                values: ['_T', 'F', 'M'],
              },
              {
                id: 'AGE',
                values: ['_T', 'Y_GE15', 'Y_GE45', 'Y_GE65', 'Y_LT25', 'Y18T24', 'Y3', 'Y4'],
              },
              {
                id: 'EDUCATION_LEV',
                values: ['_T', 'ISCED11_0_1', 'ISCED11_02', 'ISCED11_1T4', 'ISCED11_2', 'ISCED11_5T8'],
              },
              {
                id: 'CATEGORY',
                values: ['ACC', 'AFF', 'COU', 'CSD', 'EDS', 'GEO', 'INF', 'MOS', 'QUA', 'RES', 'SEC', 'TIM'],
              },
              {
                id: 'FREQ',
                values: ['A'],
              },
            ],
          },
        ],
      },
    ],
  },
  meta: {
    schema:
      'https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/structure-message/tools/schemas/1.0/sdmx-json-structure-schema.json',
    'content-languages': ['en'],
    id: 'IDREF6422',
    prepared: '2020-06-15T14:27:39.2037717Z',
    test: false,
    sender: {
      id: 'Unknown',
    },
    receiver: [
      {
        id: 'Unknown',
      },
    ],
  },
})
