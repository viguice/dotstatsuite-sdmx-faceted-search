import { prop } from 'ramda'
import { createContext } from 'jeto'
import axios from 'axios'
import initHttp from '../init/http'
import initServices from '../services'
import initSolr from '../solr'
import initMongo from '../init/mongo'
import { manageSchema, tearContext, initConfig } from './utils'
import initConfigManager from '../init/configManager'

let CTX
const tc = () => CTX

const initContext = async () => {
  try {
    CTX = await initConfig(createContext)
      .then(initMongo)
      .then(initSolr)
      .then(async ctx => {
        await manageSchema(ctx().TENANT, ctx().solr)
        return ctx
      })
      .then(initConfigManager)
      .then(initServices)
      .then(initHttp)
  } catch (e) {
    console.error(e) // eslint-disable-line
    throw e
  }
}

beforeAll(initContext)
afterAll(tearContext(tc))

describe('API status service', () => {

  it('should get right status', async () => {
    const { http } = CTX()
    const res = await axios({ method: 'get', url: `${http.url}/healthcheck` }).then(prop('data'))
    expect(res.mongo).toEqual('OK')
    expect(res.totalMembers).toEqual(1)
  })
})
