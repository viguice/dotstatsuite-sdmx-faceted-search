import { createContext } from 'jeto'
import debug from '../debug.js'
import nock from 'nock'
import ConfigProvider from '../configProvider'
import { LANGS } from '../sdmx/utils'


const CONFIG_URL = 'http://configProvider'
const tenants = require('./tenants')
const SETTINGS = {
  spaces: {
    overwrite: {
      url: 'overwrite_url',
      headers: {
        header2: 'value22',
        header3: 'value33',
      },
    },
  },
}
const SYNONYMS = {
  "stats": "statistics", // synonyms only at index (sease)
  "statistics": "stats", // requires reversability
  "hi": ["hi", "hello", "morning"],
  "hello": ["hello", "hi", "morning"],
  "morning": ["morning", "hello", "hi"],
};
const STOPWORDS = ['a', 'an', 'earum'];

export const tearContext = cb => async () => {
  const CTX = cb()
  try {
    await CTX().mongo?.dropDatabase()
    CTX().http?.close()
    CTX().nsi?.close()
    await CTX().mongo?.close()
    await CTX().solr?.deleteCollection(CTX().TENANT.id)
  } catch (err) {
    console.error(err) // eslint-disable-line
    throw err
  }
}

export const initConfig = async (ctx = createContext({}), params = {}, options = {}) => {
  const server = { host: 'localhost' }
  const TENANTS = options.tenants || tenants
  const config = {
    configUrl: CONFIG_URL,
    gitHash: 'None',
    env: 'production',
    apiKey: 'secret',
    server,
    mongo: {
      url: process.env.CI ? 'mongodb://mongo:27017' : 'mongodb://localhost:27017',
      // dbName: `test-sfs-${uuid()}`,
      dbName: `test`,
    },
    solr: {
      protocol: 'http',
      host: process.env.CI ? 'solr' : '0.0.0.0',
      port: 8983,
      collection: 'test',
      logLevel: 0,
    },
  }

  const TENANT = Object.values(TENANTS)[0]
  nock(CONFIG_URL)
    .persist()
    .get(`/configs/tenants.json`)
    .reply(200, TENANTS)

  nock(CONFIG_URL)
    .persist()
    .get(`/configs/sfs.json`)
    .reply(200, params)

  nock(CONFIG_URL)
    .persist()
    .get(`/configs/${TENANT.id}/sfs/settings.json`)
    .reply(200, SETTINGS)

  const languages = LANGS.split(',')
  for (const l of languages) {
    nock(CONFIG_URL)
      .persist()
      .get(`/configs/${TENANT.id}/sfs/synonyms/${l}.json`)
      .reply(l === 'en' ? 200 : 404, SYNONYMS)

    nock(CONFIG_URL)
      .persist()
      .get(`/configs/${TENANT.id}/sfs/stopwords/${l}.json`)
      .reply(l === 'en' ? 200 : 404, STOPWORDS)
  }

  const configProvider = ConfigProvider(config)

  return ctx({ startTime: new Date(), config, configProvider, TENANT })
}

export const waitFor = async fn => {
  return new Promise(resolve => {
    const tryFn = async () => {
      try {
        await fn()
        resolve()
      } catch (e) {
        setImmediate(tryFn)
      }
    }
    tryFn()
  })
}


export const createCollection = async (tenant, solr) => {
  const managedTenants = await solr.getTenantCollections()
  if(!managedTenants.includes(tenant.id)){
    debug.info(`Creating ${tenant.id} collection...`)
    await solr.createCollection(tenant.id)
  }
}

export const manageSchema = async (tenant, solr) => {
  await createCollection(tenant, solr)
  const [, typeFields] = await solr.getDynamicAndTypeFields(tenant.id)
  if(!typeFields?.length){
    await solr.createSchema()
    await solr.reloadCollections()
  }
}

