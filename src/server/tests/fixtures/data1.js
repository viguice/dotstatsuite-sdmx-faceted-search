import R from 'ramda';

const fixture = {
  name: 'localized search',
  config: {
    defaultLocale: 'it',
  },
  data: [
    {
      id: 'SRC:IRS',
      dataflowId_s: 'IRS',
      datasourceId_s: 'SRC',
      type_s: 'dataflow',
      agencyId_s: 'ECB',
      version_s: '1.0',
      name_sfs_text_en: 'Interest rate statistics',
      sname_en_s: 'Interest rate statistics',
      name_sfs_text_it: 'NL Interest rate statistics',
      sname_it_s: 'NL Interest rate statistics',
      name_sfs_text_fr: 'Bonjour',
      sname_fr_s: 'Bonjour',
      description_sfs_text_en: 'hello an good dev',
    },
    {
      id: 'SRC:ABC',
      datasourceId_s: 'SRC',
      dataflowId_s: 'ABC',
      type_s: 'dataflow',
      agencyId_s: 'Agency1',
      version_s: '1.0',
      name_sfs_text_en: 'Name1',
      sname_en_s: 'Name1',
      name_sfs_text_it: 'NL Name1',
      sname_it_s: 'NL Name1',
      description_sfs_text_en: 'Facere aut expedita an quibusdam earum veniam',
    },
    {
      id: 'SRC:CDE',
      dataflowId_s: 'CDE',
      datasourceId_s: 'SRC',
      type_s: 'dataflow',
      agencyId_s: 'Agency2',
      version_s: '1.0',
      name_sfs_text_en: 'Name2',
      sname_en_s: 'Name2',
      description_sfs_text_en: 'morning',
      name_sfs_text_it: 'NL Name2',
      sname_it_s: 'NL Name2',
    },
  ],
  tests: [
    {
      name: 'should get IDS',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          search: 'hello rate',
          lang: 'en',
        },
      },
      results: [
        {
          fct: R.compose(R.pluck('id'), R.prop('dataflows')),
          data: ['SRC:IRS'],
        },
      ],
    },
    {
      name: 'should get localized names (it)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          lang: 'it',
        },
      },
      results: [
        {
          fct: R.compose(R.pluck('name'), R.prop('dataflows')),
          data: ['NL Interest rate statistics', 'NL Name1', 'NL Name2'],
        },
      ],
    },
    {
      name: 'should get default localized names (it)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {},
      },
      results: [
        {
          fct: R.compose(R.pluck('name'), R.prop('dataflows')),
          data: ['NL Interest rate statistics', 'NL Name1', 'NL Name2'],
        },
      ],
    },
    {
      name: 'should get localized names (en)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          lang: 'en',
        },
      },
      results: [
        {
          fct: R.compose(R.pluck('name'), R.prop('dataflows')),
          data: ['Interest rate statistics', 'Name1', 'Name2'],
        },
      ],
    },
    {
      name: 'should get localized names (fr)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          lang: 'fr',
        },
      },
      results: [
        {
          fct: R.compose(R.filter(R.identity), R.pluck('name'), R.prop('dataflows')),
          data: ['Bonjour'],
        },
      ],
    },
    {
      name: 'should get agencyId',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          lang: 'it',
        },
      },
      results: [
        {
          fct: R.compose(R.pluck('agencyId'), R.prop('dataflows')),
          data: ['ECB', 'Agency1', 'Agency2'],
        },
      ],
    },
    {
      name: 'should search (fr)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          search: 'Bonjour',
          lang: 'fr',
        },
      },
      results: [
        {
          fct: R.compose(R.filter(R.identity), R.pluck('name'), R.prop('dataflows')),
          data: ['Bonjour'],
        },
      ],
    },
    {
      name: 'should search with a typo catched by stemming (fr)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          search: 'Bonjoux',
          lang: 'fr',
        },
      },
      results: [
        {
          fct: R.compose(R.filter(R.identity), R.pluck('name'), R.prop('dataflows')),
          data: ['Bonjour'],
        },
      ],
    },
    {
      name: 'should search with too many typo, even for stemming (fr)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          search: 'Bonxjxour',
          lang: 'fr',
        },
      },
      results: [
        {
          fct: R.prop('dataflows'),
          data: [],
        },
      ],
    },
    {
      name: 'should multi search (en)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          search: 'good rate',
          lang: 'en',
        },
      },
      results: [
        {
          fct: R.compose(R.filter(R.identity), R.pluck('id'), R.prop('dataflows')),
          data: ['SRC:IRS'],
        },
      ],
    },
    {
      name: 'should find a dataflow through synonym (en)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          search: 'stats',
          lang: 'en',
        },
      },
      results: [
        {
          fct: R.compose(R.filter(R.identity), R.pluck('id'), R.prop('dataflows')),
          data: ['SRC:IRS'],
        },
      ],
    },
    {
      name: 'should find 2 dataflows through multi valued synonym (en)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          search: 'hello',
          lang: 'en',
        },
      },
      results: [
        {
          fct: R.compose(R.pluck('id'), R.prop('dataflows')),
          data: ['SRC:CDE', 'SRC:IRS'],
        },
      ],
    },
    //{
    //  name: 'should not find dataflow because stopwords (en)',
    //  request: {
    //    method: 'post',
    //    url: '/api/search',
    //    data: {
    //      search: 'an',
    //      lang: 'en',
    //    },
    //  },
    //  results: [
    //    {
    //      fct: R.compose(R.filter(R.identity), R.pluck('id'), R.prop('dataflows')),
    //      data: [],
    //    },
    //  ],
    //},
    // {
    //   name: 'should find dataflow with stopword (earum)',
    //   request: {
    //     method: 'post',
    //     url: '/api/search',
    //     data: {
    //       search: 'expedita earum quibusdam',
    //       lang: 'en',
    //     },
    //   },
    //   results: [
    //     {
    //       fct: R.compose(R.filter(R.identity), R.pluck('id'), R.prop('dataflows')),
    //       data: ['SRC:ABC'],
    //     },
    //   ],
    // },
    //{
    //  name: 'should find dataflow without stopword (earum)',
    //  request: {
    //    method: 'post',
    //    url: '/api/search',
    //    data: {
    //      search: 'quibusdam veniam',
    //      lang: 'en',
    //    },
    //  },
    //  results: [
    //    {
    //      fct: R.compose(R.filter(R.identity), R.pluck('id'), R.prop('dataflows')),
    //      data: ['SRC:ABC'],
    //    },
    //  ],
    //},
  ],
};

export default fixture;
