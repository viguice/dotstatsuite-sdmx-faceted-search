import axios from 'axios'
import nock from 'nock'
import { prop } from 'ramda'
import urljoin from 'url-join'
import { createContext } from 'jeto'
import initHttp from '../init/http'
import initServices from '../services'
import initSolr from '../solr'
import initIndexer from '../init/indexer'
import initReporter from '../init/reporter'
import initConfigManager from '../init/configManager'
import initParams from '../init/loadParams'
import fixture from './fixtures/data1'
import { manageSchema, tearContext, initConfig, waitFor } from './utils'
import initMongo from '../init/mongo'
import initModels from '../init/models'
import initLogger from '../init/logger'

const DF = require('./mocks/sdmx/dataflow_sample')

let TENANT
let CTX
const tc = () => CTX

const makeRequestFactory = (http, { apiKey }) => ({ method = 'post', url, data }) =>
  axios({ method, url: urljoin(http.url, url), data, headers: { 'x-tenant': TENANT.id, 'x-api-key': apiKey } }).then(
    prop('data'),
  )
let makeRequest

const params = () => ({
  defaultLocale: 'en',
})

const initContext = async () => {
  try {
    CTX = await initConfig(createContext, params())
      .then(ctx => {
        TENANT = ctx().TENANT
        return ctx
      })
      .then(initMongo)
      .then(initModels)
      .then(initReporter)
      .then(initLogger)
      .then(initSolr)
      .then(initConfigManager)
      .then(initParams)
      .then(initIndexer)
      .then(initServices)
      .then(initHttp)
    await manageSchema(TENANT, CTX().solr)
    const solrClient = CTX().solr.getClient(TENANT)
    await solrClient.deleteAll()
    await CTX().configManager.updateSchema(TENANT, CTX().config.fields)
    makeRequest = makeRequestFactory(CTX().http, CTX().config)
  } catch (e) {
    throw e
  }
}

beforeAll(initContext)
afterAll(tearContext(tc))

describe('Server | not indexed', () => {
  const email = 'EMAIL'

  it('should index one dataflow', async () => {
    const dataSpaceId = 'demo-stable'
    const space = TENANT.spaces[dataSpaceId]
    const ref = { agencyID: 'A', id: 'DF', version: '1.0' }
    const structure = DF(ref)

    nock(space.url)
      .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
      .reply(200, { data: {} })

    nock(space.url)
      .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
      .reply(200, { data: { dataflows: [ref] } })

    nock(space.url)
      .get('/dataflow/A/DF/1.0/?references=all&detail=referencepartial')
      .reply(200, structure)

    await makeRequest({
      method: 'POST',
      url: `/admin/dataflow?userEmail=${email}`,
      data: { dataSpaceId, ...ref },
    })
  })

  it('should search on dimension DIM2', async () => {
    const search = 'name:DF'
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    // console.log(JSON.stringify(res, null, 4))
    expect(res.dataflows[0].dimensions).toEqual(['Concept2', 'Time Period'])
    expect(res.dataflows[0].Concept2.length).toEqual(2)
  })
})
