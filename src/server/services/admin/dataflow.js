import { join, find, filter, pluck, toPairs, propOr, compose } from 'ramda'
import debug from '../../debug'
import { HTTPError } from '../../utils/errors'
import { fullID, getStructure } from '../../sdmx/utils'
import { checkTenant, getSolrClient, getSolrConfig } from '../utils'

const NAME = 'dataflow'

const getStatusCode = (local, report, loadings) => {
  let errors = 0
  let indexed = 0
  let fetched = 0
  const data = []

  // console.log(JSON.stringify(report, null, 4))
  for (const loading of loadings) {
    const x = report.loadings[loading.loadingId]
    data.push(x)
    for (const ds of Object.values(x)) {
      errors += ds.dataflows?.errors ?? 0
      indexed += ds.dataflows?.indexed ?? 0
      fetched += ds.dataflows?.fetched ?? 0
    }
  }

  let status
  if (indexed) status = errors || local.catSchemeCount > local.categorized ? 207 : 200
  else {
    // if (!local.categorized) status = 409
    if (!fetched) status = 404
    else status = 409
  }

  return [status, data]
}

export const service = {
  async delete({ dataSpaceId, spaceId, agencyId, id, version, userEmail }) {
    if (!spaceId) spaceId = dataSpaceId
    const { indexer } = this.globals()
    const { res, tenant } = this.locals
    const space = tenant.spaces?.[spaceId]
    const dID = fullID('dataflow')
    if (!space) throw new HTTPError(400, `Unknown space ${spaceId} for organisation ${tenant.id}`)

    debug.info(`deleting [dataflow] ${agencyId}:${id}(${version}) for organisation ${tenant.id} from space ${spaceId}`)
    const datasources = compose(
      pluck(0),
      filter(([datasourceId, { dataSpaceId, indexed }]) => dataSpaceId === spaceId && indexed), // eslint-disable-line
      toPairs,
      propOr([], 'datasources'),
    )(tenant)

    if (!datasources.length)
      throw new HTTPError(400, `No datasource found linked to space ${spaceId} for organisation ${tenant.id}`)

    const matches = await indexer.deleteOne(tenant, spaceId, datasources, agencyId, id, version, { userEmail })

    if (!matches.length) {
      const message = `${dID({ agencyId, id, version })} was not found`
      debug.warn(message)
      res.status(400).json({ message })
    } else {
      const message = `${dID({ agencyId, id, version })} deleted with datasources: [${join(', ', matches)}]`
      debug.info(message)
      res.status(200).json({ message })
    }
  },

  async post({ dataSpaceId, spaceId, agencyId, agencyID, id, version, userEmail }) {
    if (!agencyId) agencyId = agencyID
    if (!spaceId) spaceId = dataSpaceId

    if (!spaceId) throw new HTTPError(400, `'spaceId' param is required to post a dataflow`)
    if (!agencyId) throw new HTTPError(400, `'agencyId' param is required to post a dataflow`)
    if (!id) throw new HTTPError(400, `'id' param is required to post a dataflow`)
    if (!version) throw new HTTPError(400, `'version' param is required to post a dataflow`)

    const { report, indexer } = this.globals()
    const { res, tenant } = this.locals
    const space = tenant.spaces?.[spaceId]
    if (!space) throw new HTTPError(400, `Unknown space ${spaceId} for organisation ${tenant.id}`)
    debug.info(`[post] dataflow '${agencyId}:${id}(${version})' for organisation ${tenant.id} from space ${spaceId}`)
    const datasources = compose(
      filter(([datasourceId, { dataSpaceId, indexed }]) => dataSpaceId === spaceId && indexed), // eslint-disable-line
      toPairs,
      propOr([], 'datasources'),
    )(tenant)
    if (!datasources.length)
      throw new HTTPError(409, `No datasource found linked to space ${spaceId} for organisation ${tenant.id}`)
    const spaceUrl = space.url
    const dID = fullID('dataflow')
    debug.info(datasources, `Found ${datasources.length} datasource(s): [${join(', ', pluck(0, datasources))}]`)
    const loadings = []
    const localReport = { categorized: 0, noCatScheme: 0 }
    for (const [datasourceId, ds] of datasources) {
      ds.id = datasourceId
      localReport.catSchemeCount = ds.dataqueries.length
      for (const query of ds.dataqueries) {
        debug.info({ datasourceId, query }, `process data query`)
        try {
          const structure = await getStructure({
            spaceUrl,
            resourceType: 'categoryscheme',
            resourceId: query.categorySchemeId,
            version: query.version,
            agencyId: query.agencyId,
            references: 'dataflow',
            detail: 'full',
          })
          const dataflow = find(
            dataflow => dID(dataflow) === dID({ agencyId, id, version }),
            structure.data?.dataflows || [],
          )
          if (dataflow) {
            debug.info(dataflow, `found ${agencyId}:${id}(${version}) in ${datasourceId}`)
            const options = {
              dataflowId: dataflow.id,
              datasourceId,
              dataSpaceId: spaceId,
              agencyId,
              version,
              isExternalReference: dataflow.isExternalReference,
              links: dataflow.links,
              userEmail,
            }
            const loading = await indexer.loadOne(tenant, ds, options)
            loadings.push(loading)
            localReport.categorized++
          } else {
            debug.info(`cannot find dataflow ${agencyId}:${id}(${version}) in ${datasourceId}`)
          }
        } catch (err) {
          localReport.noCatScheme++
          debug.warn(`cannot get categoryscheme ${query.agencyId}:${query.categorySchemeId}(${query.version})`)
        }
      }
    }

    if (!loadings.length) {
      const message = `${dID({ agencyId, id, version })} do not match any datasources`
      debug.warn(message)
      res.status(409).json({ message })
    } else {
      const [status, data] = getStatusCode(localReport, report(), loadings)
      res.status(status).json(data)
    }
  },
}

const init = evtx => {
  evtx.use(NAME, service).service(service.name)
  evtx.service(NAME).before({
    delete: [checkTenant, getSolrConfig, getSolrClient],
    post: [checkTenant, getSolrConfig, getSolrClient],
  })
}
export default init
