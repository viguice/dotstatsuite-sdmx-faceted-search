import debug from '../../debug';
import { reduce } from 'ramda';
import evtX from 'evtx';
import initPing from './ping';
import initSearch from './search';
import initConfig from './config';
import { checkTenant, getSolrClient, getSolrConfig, getFacetOrders } from '../utils';

const allServices = [initPing, initSearch, initConfig];

const initServices = evtx => reduce((acc, service) => acc.configure(service), evtx, allServices);

export default async ctx => {
  const api = evtX(ctx)
    .configure(initServices)
    .before(checkTenant, getSolrConfig, getSolrClient, getFacetOrders);

  debug.info('API setup.');
  return ctx({ services: { ...ctx().services, api } });
};
