import { reduce } from 'ramda';
import { decode } from '../../utils';
import { doSearch } from '../../search';
import { checkLang } from '../utils';

const makeFacet = ({ type, localized }, { buckets = [] } = {}) => ({ type, localized, buckets });
const facetDefinition = (searchedFacets = {}, facets) =>
  reduce(
    (acc, facet) => {
      const name = decodeFacetName(facet);
      return { ...acc, [name]: makeFacet(facet, searchedFacets[name]) };
    },
    {},
    facets,
  );

const decodeFacetName = facet => {
  if (!facet.encoded) return facet.name;
  return decode(facet.name);
};

const NAME = 'config';

export const service = {
  async get({ lang }) {
    const { solrClient, solrConfig } = this.locals;
    const {
      defaultLocale,
      dataflows: { locales, facets, searchFields },
    } = solrConfig;
    const locale = lang || defaultLocale;

    return doSearch(null, '', null, lang, solrClient, solrConfig).then(({ facets: searchedFacets }) => ({
      locale,
      locales,
      defaultLocale,
      facets: facetDefinition(searchedFacets, facets),
    }));
  },

  post({ lang, facets: tenantFacets }) {
    const { solrClient, solrConfig } = this.locals;
    const {
      defaultLocale,
      dataflows: { locales, facets, searchFields },
    } = solrConfig;
    const locale = lang || defaultLocale;

    return doSearch(tenantFacets, '', null, lang, solrClient, solrConfig).then(({ facets: searchedFacets }) => ({
      locale,
      locales,
      defaultLocale,
      searchFields,
      facets: facetDefinition(searchedFacets, facets),
    }));
  },
}

export default evtx => {
  evtx.use(NAME, service);
  evtx.service(NAME).before({
    get: [checkLang],
    post: [checkLang],
  })
}
