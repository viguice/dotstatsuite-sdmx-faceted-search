const SERVICE_NAME = 'ping';

export const ping = {
  get() {
    return Promise.resolve({ ping: 'pong' });
  },
};

const init = evtx => {
  evtx.use(SERVICE_NAME, ping);
};

export default init;
