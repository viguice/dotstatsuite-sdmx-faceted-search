import initApi from './api';
import initAdmin from './admin';
import initHealthcheck from './healthcheck';

export default ctx =>
  initAdmin(ctx)
    .then(initApi)
    .then(initHealthcheck);
