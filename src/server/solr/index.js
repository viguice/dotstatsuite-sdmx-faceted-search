import { map, prop, dissoc, pathEq, lensPath, over, flip, set, lensProp, includes, difference, concat, reject, pipe, isEmpty } from 'ramda'
import got from 'got'
import debug from '../debug'
import { filterMemberCollections, fold, foldToAscii } from '../utils'
import { SFS_TEXT_LIST_EXT, LANGS, SFS_FIELD_PREFIX, SFS_TEXT_EXT } from '../sdmx/utils'

const DYNAMIC_FIELD_PREFIX = `*_${SFS_FIELD_PREFIX}`
const TYPE_FIELD_PREFIX=`${SFS_FIELD_PREFIX}`
const isStatusOk = pathEq(['responseHeader', 'status'], 0)

class Solr {
  constructor({ protocol, host, port, ...options }, tenants, getSynomyms, getStopwords) {
    this.protocol = protocol
    this.host = host
    this.port = port
    this.baseURL = `${protocol}://${host}:${port}`
    this.tenants = tenants
    this.options = options
    this.getSynomyms = getSynomyms;
    this.getStopwords = getStopwords;
  }

  getTenant(id){
    return this.tenants[id]
  }

  getClient(tenant) {
    return new Client(this, tenant)
  }

  post(url, options) {
    debug.trace(options, `solr:: POST: ${this.baseURL}/${url}`)
    return this.request(url, { ...options, method: 'post', prefixUrl: this.baseURL })
  }

  get(url) {
    return this.request(url, { method: 'get', prefixUrl: this.baseURL })
  }

  async request(url, options) {
    try {
      const res = await got(url, options).json()
      debug.trace(res)
      if (isStatusOk(res)) return res
      throw new Error(res.error.msg)
    } catch (err) {
      if (err?.response?.body) debug.error(JSON.parse(err.response.body), 'Solr Error')
      throw err
    }
  }

  async adminStatus() {
    const url = 'solr/admin/info/system?&wt=json'
    const status = await this.get(url)
    return dissoc('responseHeader', status)
  }

  async getTenantCollections() {
    const res = await this.status()
    const collections = filterMemberCollections(res, this.tenants)
    return Object.keys(collections)
  }

  status() {
    const url = 'solr/admin/collections?action=CLUSTERSTATUS&wt=json'
    return this.get(url).then(({ cluster }) => cluster.collections)
  }

  createCollection(tenantId) {
    const url = `solr/admin/collections?action=CREATE&name=${tenantId}&numShards=1&collection.configName=_default`
    return this.get(url)
  }

  deleteCollection(tenantId) {
    const url = `solr/admin/collections?action=DELETE&name=${tenantId}`
    return this.get(url)
  }

  reloadCollection(tenantId) {
    const url = `solr/admin/collections?action=RELOAD&name=${tenantId}`
    return this.get(url)
  }

  async createDynamicField(collection, field){
    const url = `solr/${collection}/schema?wt=json`
    const json = { 'add-dynamic-field': field }
    await this.post(url, { json })
  }

  async deleteDynamicField(collection, name){
    const url = `solr/${collection}/schema?wt=json`
    const json = { 'delete-dynamic-field': { name } }
    await this.post(url, { json })
  }

  async deleteFieldType(collection, name) {
    const url = `solr/${collection}/schema?wt=json`
    const json = { 'delete-field-type': { name } }
    await this.post(url, { json })
  }

  async createFieldType(collection, fieldType) {
    //debug.info({ fieldType }, `Creating field type`)
    debug.info(`Creating field type: ${fieldType.name}`)
    const url = `solr/${collection}/schema?wt=json`
    const json = { 'add-field-type': fieldType }
    await this.post(url, { json })
  }

  async getFieldType(collection, name){
    const url = `solr/${collection}/schema/fieldtypes/${name}?wt=json`
    return this.get(url);
  }

  getSchema(collection){
    const url = `solr/${collection}/admin/luke?show=schema&wt=json`
    return this.get(url)
  }

  getTenantLanguages(tenantId){
    const tenant = this.getTenant(tenantId)
    if(!tenant) return []
    if(!tenant.languages && tenant.defaultLanguage) return [tenant.defaultLanguage]
    return tenant.languages?.split(',') ?? []
  }

  async createSynonyms(collection, lang){
    debug.info(`Creating synonyms catalog for ${lang}`)
    // getSynomyms expects a tenant (collections are built upon tenants)
    const tenant = this.getTenant(collection)
    const synonyms = await this.getSynomyms(tenant, lang)
    if (!synonyms || isEmpty(synonyms)) return;
    const url = `solr/${collection}/schema/analysis/synonyms/${lang}`
    await this.post(url, { json: foldToAscii(synonyms) })
  }

  async createStopwords(collection, lang) {
    debug.info(`Creating stopwords catalog for ${lang}`)
    // getStopwords expects a tenant (collections are built upon tenants)
    const tenant = this.getTenant(collection)
    const stopwords = await this.getStopwords(tenant, lang)
    if (!stopwords || isEmpty(stopwords)) return;
    const url = `solr/${collection}/schema/analysis/stopwords/${lang}`
    await this.post(url, { json: map(fold, stopwords) })
  }

  async getDynamicAndTypeFields(collection){
    const res = await this.getSchema(collection)
    return  [
      (Object.keys(res?.schema?.dynamicFields) || []).filter(n => n.startsWith(DYNAMIC_FIELD_PREFIX)),
      (Object.keys(res?.schema?.types) || []).filter(n => n.startsWith(TYPE_FIELD_PREFIX))
    ]
  }

  async createSchema(){
    const collections = await this.getTenantCollections()
    let hsfsFields = {}
    let hsfsFieldTypes = {}
    for(const collection of collections){
      const [fieldNames, typeFieldNames] = await this.getDynamicAndTypeFields(collection)
      for(const name of fieldNames) hsfsFields[name] = [name, collection]
      for (const name of typeFieldNames) hsfsFieldTypes[name] = [name, collection]
    }
    const sfsFields = Object.values(hsfsFields)
    if(sfsFields.length)debug.info({ fields: map(prop(0), sfsFields) }, `Deleting existing dynamic fields...`)
    for(const [name, collection] of sfsFields) await this.deleteDynamicField(collection, name)

    const sfsFieldTypes = Object.values(hsfsFieldTypes)
    if (sfsFieldTypes.length) debug.info({ fields: map(prop(0), sfsFieldTypes) }, `Deleting existing field types...`)
    // When a Field type is deleted, associated managed catalogs (synonyms, stopwords) are also deleted
    for (const [name, collection] of sfsFieldTypes) await this.deleteFieldType(collection, name)

    const languages = LANGS.split(',')
    // Solr doesn't support km, there is no field type text_km
    const unsupportedLanguages = ['km'];

    debug.info(`Creating field types...`)
    for (const l of languages){
      const fieldTypeName = includes(l, unsupportedLanguages) ? 'text_general' : `text_${l}`;
      const res = await this.getFieldType(collections[0], fieldTypeName)
      let { fieldType } = res

      const FILTERS = [
        { 'class': 'solr.ASCIIFoldingFilterFactory' },
        //{ 'class': 'solr.ManagedStopFilterFactory', 'managed': l },
      ];
      const QFILTERS = [
        { 'class': 'solr.ManagedSynonymGraphFilterFactory', 'managed': l },
      ];
      const IFILTERS = [
        //{ 'class': 'solr.ManagedSynonymGraphFilterFactory', 'managed': l },
        //{ 'class': 'solr.FlattenGraphFilterFactory' }, // only @indexing
      ];
      const XFILTERS_CLASS = [
        'solr.SynonymGraphFilterFactory',
        'solr.StopFilterFactory',
      ];
      const rejectSolrDefaults = reject(
        filter => includes(filter.class, XFILTERS_CLASS)
      );
      if (prop('analyzer', fieldType)){
        fieldType = over(
          lensPath(['analyzer', 'filters']),
          pipe(
            rejectSolrDefaults,
            concat([...FILTERS, ...IFILTERS, ...QFILTERS]),
          ),
          fieldType,
        )
      } else {
        fieldType = over(
          lensPath(['indexAnalyzer', 'filters']),
          pipe(
            rejectSolrDefaults,
            concat([...FILTERS, ...IFILTERS]),
          ),
          fieldType,
        );
        fieldType = over(
          lensPath(['queryAnalyzer', 'filters']),
          pipe(
            rejectSolrDefaults,
            concat([...FILTERS, ...QFILTERS]),
          ),
          fieldType,
        );
      }

      fieldType = set(lensProp('name'), `${TYPE_FIELD_PREFIX}_text_${l}`, fieldType)
      await this.createFieldType(collections[0], fieldType)
      await this.createSynonyms(collections[0], l)
      //await this.createStopwords(collections[0], l)
    }

    debug.info(`Creating dynamic multi valued fields...`)
    for (const l of languages) {
      const fieldProps = { indexed: true, multiValued: true, stored: true, uninvertible: true }
      const makeDynamicField = field => ({ ...fieldProps, ...field })
      const field = { type: `${TYPE_FIELD_PREFIX}_text_${l}`, name: `*_${SFS_TEXT_LIST_EXT}_${l}` }
      //debug.info({ field: makeDynamicField(field) }, `Creating dynamic field...`)
      debug.info(`Creating dynamic field: ${field.name}`)
      await this.createDynamicField(collections[0], makeDynamicField(field))
    }

    debug.info(`Creating dynamic single valued fields...`)
    for (const l of languages) {
      const fieldProps = { indexed: true, tokenized: true, stored: true, uninvertible: true, multiValued: false }
      const makeDynamicField = field => ({ ...fieldProps, ...field })
      const field = { type: `${TYPE_FIELD_PREFIX}_text_${l}`, name: `*_${SFS_TEXT_EXT}_${l}` }
      //debug.info({ field: makeDynamicField(field) }, `Creating dynamic field...`)
      debug.info(`Creating dynamic field: ${field.name}`)
      await this.createDynamicField(collections[0], makeDynamicField(field))
    }
  }

  async manageCollections(){
    let managedTenants = await this.getTenantCollections()
    debug.info({ managedTenants }, `Existing tenant's collections:`)
    const newTenants = difference(Object.keys(this.tenants), managedTenants)
    if(!newTenants.length) {
      debug.info(`All tenants are already managed by solr`)
      return
    }
    debug.warn(`${JSON.stringify(newTenants)} tenant's collections are not yet managed by solr`)
    for(const tenantId of newTenants){
      debug.info(`Creating ${tenantId} collection...`)
      await this.createCollection(tenantId)
    }
    managedTenants = await this.getTenantCollections()
    debug.info(`Existing tenant's collections: ${JSON.stringify(managedTenants)} `)
  }

  async reloadCollections(){
    const managedTenants = await this.getTenantCollections()
    for (const tenantId of managedTenants) {
      debug.info(`Reloading ${tenantId} collection...`)
      await this.reloadCollection(tenantId)
    }
  }

  async manageSchema() {
    await this.manageCollections()
    await this.createSchema()
    await this.reloadCollections()
  }
}

class Client {
  constructor(solr, tenant) {
    this.solr = solr
    this.collection = tenant.id
  }

  deleteAll() {
    const { collection } = this
    const url = `solr/${collection}/update/json?commit=true`
    const json = { delete: { query: '*:*' } }
    return this.solr.post(url, { json })
  }

  deleteOne(id) {
    const { collection } = this
    const url = `solr/${collection}/update/json?commit=true`
    const json = { delete: { query: `id:"${id}"` } }
    return this.solr.post(url, { json })
  }

  add(data) {
    const { collection } = this
    const url = `solr/${collection}/update/json?commit=true`
    const json = { add: data }
    return this.solr.post(url, { json })
  }

  select(request) {
    const { collection } = this
    const url = `solr/${collection}/select?wt=json`
    return this.solr.post(url, { json: request })
  }
}

const init = async ctx => {
  const {
    config: { solr: solrConfig },
    configProvider,
  } = ctx()
  const tenants = await configProvider.getTenants()
  const solr = new Solr(
    solrConfig,
    tenants,
    configProvider.getSynomyms,
    configProvider.getStopwords
  )
  return ctx({ solr })
}

export default init
