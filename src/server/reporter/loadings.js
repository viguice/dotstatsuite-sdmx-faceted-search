import { pick, assocPath, path } from 'ramda'
import { ERROR } from './errors'
import debug from '../debug'
export const CATEGORY_SCHEME_LOADED = 'CATEGORY_SCHEME_LOADED'
export const DATAFLOW_LOADED = 'DATAFLOW_LOADED'
export const DATAFLOW_FETCHED = 'DATAFLOW_FETCHED'
export const DATAFLOW_REJECTED = 'DATAFLOW_REJECTED'
export const DATAFLOW_INDEXED = 'DATAFLOW_INDEXED'
export const DATAFLOWS_LOADED = 'DATAFLOWS_LOADED'
export const DATAFLOWS_DELETED = 'DATAFLOWS_DELETED'
export const DATAFLOW_TRANSPILED = 'DATAFLOW_TRANSPILED'
export const START_LOADING_DATASOURCE = 'START_LOADING_DATASOURCE'
export const START_INDEXING = 'START_INDEXING'
export const END_INDEXING = 'END_INDEXING'

const updateDelay = ({ startTime, stats = {} } = {}) => {
  const delay = getDelay(startTime)
  return {
    ...stats,
    delay,
  }
}

const updateLoadingStats = (
  { startTime, dataflows: { loaded = 0 } = {}, stats: { avg, min = Infinity, max = 0 } = {} } = {},
  { delay },
) => {
  const total = getDelay(startTime)
  return {
    min: delay < min ? delay : min,
    max: delay > max ? delay : max,
    avg: avg ? (avg * loaded + delay) / (loaded + 1) : delay,
    total,
  }
}
const getLoadingPath = ({ loading: { datasourceId, loadingId } = {} }) => [loadingId, datasourceId]
const getLoading = (state, action) => path(getLoadingPath(action), state)
const getDelay = startTime => new Date() - startTime
const inc = (key, state = {}) => ({ ...state, [key]: state[key] ? state[key] + 1 : 1 })

const reducer = (state, action) => {
  try {
    const loadingPath = getLoadingPath(action)
    const loading = getLoading(state, action)
    switch (action.type) {
      case START_LOADING_DATASOURCE:
        return assocPath([action.loading.loadingId, action.loading.datasourceId, 'startTime'], new Date(), state)

      case ERROR:
        return assocPath(
          loadingPath,
          {
            ...loading,
            dataflows: inc('errors', loading.dataflows),
            stats: updateDelay(loading),
            errors: [{ ...pick(['code', 'message'], action.error), time: new Date() }, ...(loading.errors || [])],
          },
          state,
        )

      case CATEGORY_SCHEME_LOADED:
        return assocPath(
          loadingPath,
          {
            ...loading,
            categoryschemes: inc('loaded', loading.categoryschemes),
            stats: updateDelay(loading),
          },
          state,
        )

      case DATAFLOWS_LOADED:
        return assocPath(
          loadingPath,
          {
            ...loading,
            dataflows: {
              ...loading.dataflows,
              selected: action.dataflows.length,
            },
            stats: updateDelay(loading),
          },
          state,
        )

      case DATAFLOW_LOADED:
        return assocPath(
          loadingPath,
          {
            ...loading,
            dataflows: inc('loaded', loading.dataflows),
            stats: updateLoadingStats(loading, action),
          },
          state,
        )

      case DATAFLOW_FETCHED:
        return assocPath(
          loadingPath,
          {
            ...loading,
            dataflows: inc('fetched', loading.dataflows),
            stats: updateLoadingStats(loading, action),
          },
          state,
        )

      case DATAFLOW_REJECTED:
        return assocPath(
          loadingPath,
          {
            ...loading,
            dataflows: inc('rejected', loading.dataflows),
            stats: updateLoadingStats(loading, action),
          },
          state,
        )

      case DATAFLOW_TRANSPILED:
        return assocPath(
          loadingPath,
          {
            ...loading,
            dataflows: inc('transpiled', loading.dataflows),
            stats: updateDelay(loading),
          },
          state,
        )

      case DATAFLOW_INDEXED:
        return assocPath(
          loadingPath,
          {
            ...loading,
            dataflows: inc('indexed', loading.dataflows),
            stats: updateDelay(loading),
          },
          state,
        )

      default:
        return state
    }
  } catch (e) {
    debug.error(e)
    return state
  }
}

export default reducer
