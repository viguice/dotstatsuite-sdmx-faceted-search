
export const stripBorm = input => input.charCodeAt(0) === 0xFEFF ? input.slice(1) : input;
