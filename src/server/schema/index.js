import { compose, filter, map, values, prop , includes, either } from 'ramda';
import { isHighlight, isOutable, isSearchable, isLocalized, isFacet } from './types';
import { TEXT_EXT, TEXT_LIST_EXT } from './ext'

const makeFacet = facet => ({
  name: facet.name,
  _name: facet._name,
  encoded: facet.encoded,
  type: facet.subType,
  op: facet.op,
  localized: facet.localized,
});
const makeFacets = compose(values, map(makeFacet), filter(isFacet));

export const makeLocalizedAttr = (ext, name, locale) => {
  return includes(ext, [TEXT_LIST_EXT, TEXT_EXT])
    ? `${name}_${ext}_${locale}`
    : `${name}_${locale}_${ext}`
}
// export const makeLocalizedAttr = (ext, name, locale) => `${name}_${locale}_${ext}`

const makeAttr = attr => {
  const res = { name: attr.name };
  if (isLocalized(attr)) res.field = lang => makeLocalizedAttr(attr.ext, attr.name, lang)
  else if (attr.ext) res.field = `${attr.name}_${attr.ext}`;
  else res.field = attr.name;
  if (isFacet(attr)) res.ID = attr.id;
  if (attr.weight) res.weight = attr.weight;
  return res;
};

const makeSchema = compose(values, map(makeAttr));

const makeHighlighting = schema => ({
  fields: compose(values, map(prop('name')), filter(isHighlight))(schema),
});

const makeOutable = compose(
  values,
  map(prop('name')),
  filter(either(isOutable, isFacet)),
  //filter(isOutable),
);
const makeSearchable = compose(values, map(prop('name')), filter(isSearchable));

const Schema = schema => ({
  facets: makeFacets(schema),
  schema: makeSchema(schema),
  highlighting: makeHighlighting(schema),
  outFields: makeOutable(schema),
  searchFields: makeSearchable(schema),
});

export default Schema;
