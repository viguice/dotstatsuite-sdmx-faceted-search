import { prop, propEq } from 'ramda';

export const FACET_TYPE = 'FACET';
export const ATTR_TYPE = 'ATTR';
export const ID_TYPE = 'ID';
export const PROP_TYPE = 'PROP';
export const CATCH_ALL_TYPE = 'CATCH_ALL';

export const isFacet = propEq('type', FACET_TYPE);
export const isCatchAll = propEq('type', CATCH_ALL_TYPE);
export const isLocalized = prop('localized');
export const isHighlight = prop('highlight');
export const isSearchable = prop('search');
export const isOutable = prop('out');
