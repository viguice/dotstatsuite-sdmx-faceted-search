export default class Model {
  constructor(name, collection) {
    this.collection = collection
    this.name = name
  }
}
