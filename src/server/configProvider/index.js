const got = require('got')
const { prop } = require('ramda')
const { HTTPError } = require('../utils/errors')
const debug = require('../debug')

export const getResourceUri = (uri, path) => `${uri}/configs${path}`
const GLOBAL_CONFIG_PATH = '/sfs.json'

export const getI18nUri = (tenantId, appId, lang) => `/${tenantId}/${appId}/i18n/${lang}.json`
export const getSynonymsUri = (tenantId, appId, lang) => `/${tenantId}/${appId}/synonyms/${lang}.json`
export const getStopwordsUri = (tenantId, appId, lang) => `/${tenantId}/${appId}/stopwords/${lang}.json`
export const getTenantsUri = () => '/tenants.json'
const getGlobalConfig = config => options => getResource(config)(GLOBAL_CONFIG_PATH, options)
const getSettings = config => (memberId, options) => getResource(config)(`/${memberId}/sfs/settings.json`, options)

const getResource = ({ configUrl }) => async (path, options = {}) => {
  try {
    return await got.get(getResourceUri(configUrl, path)).json()
  } catch (err) {
    if (err.response && err.response.statusCode === 404 && Object.prototype.hasOwnProperty.call(options, 'default'))
      return options.default
    if (err.response) debug.warn(err, `Cannot get ${getResourceUri(configUrl, path)}`)
    if (err.code === 'ECONNREFUSED') {
      debug.error(`Cannot connect to config server: ${configUrl}`)
      throw err
    }
    const status = prop('status', err.response)
    if (status) throw new HTTPError(status)
    throw err
  }
}

const getI18n = config => ({ id }, lang) => getResource(config)(getI18nUri(id, 'sfs', lang), { default: {} })
const getTenants = config => () => getResource(config)(getTenantsUri())
const getTenant = config => async id => {
  const tenant = prop(id, await getTenants(config)())
  if (!tenant) throw new HTTPError(404, `Unknown tenant '${id}' from config server`)
  return tenant
}
const getSynomyms = config => async ({ id }, lang) => getResource(config)(getSynonymsUri(id, 'sfs', lang), { default: {} })
const getStopwords = config => async ({ id }, lang) => getResource(config)(getStopwordsUri(id, 'sfs', lang), { default: [] })

const provider = config => ({
  getI18n: getI18n(config),
  getTenant: getTenant(config),
  getTenants: getTenants(config),
  getResource: getResource(config),
  getGlobalConfig: getGlobalConfig(config),
  getSettings: getSettings(config),
  getSynomyms: getSynomyms(config),
  getStopwords: getStopwords(config),
})

export default provider
