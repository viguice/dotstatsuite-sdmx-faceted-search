import {
  curry,
  prop,
  and,
  has,
  pipe,
  pathOr,
  match,
  when,
  isNil,
  always,
  slice,
  map,
  any,
  anyPass,
  complement,
} from 'ramda'
import got from 'got'
import urljoin from 'url-join'
import debug from '../debug'

export function SDMXError(message) {
  this.message = message
}
SDMXError.prototype = Object.create(Error.prototype)
SDMXError.prototype.constructor = SDMXError

const toNumber = v => {
  const [major = 0, minor = 0, patch = 0] = v.split('.')
  return major * 100 + minor * 10 + patch * 1
}

export const greaterThan = (v1, v2) => toNumber(v1) > toNumber(v2)

export const getParentId = (ancestors, id) => {
  const parentId = prop(id, ancestors)
  if (and(isNil(parentId), has(id, ancestors))) return parentId // root
  if (isNil(parentId)) return id
  return getParentId(ancestors, parentId)
}

export const LANGS = 'en,nl,fr,es,it,ar,km,de,th'

export const SFS_FIELD_PREFIX = 'sfs'
export const SFS_TEXT_EXT = `${SFS_FIELD_PREFIX}_text`
export const SFS_TEXT_LIST_EXT = `${SFS_FIELD_PREFIX}_text_list`

export const ACCEPT_HEADER = {
  structure: 'application/vnd.sdmx.structure+json;version=1.0;urn=true',
  data: 'application/vnd.sdmx.data+json;version=1.0.0-wd;urn=true',
}

export const getStructure = async (
  {
    spaceUrl,
    resourceType,
    agencyId = 'all',
    resourceId = 'all',
    version = 'all',
    references = 'none',
    detail = 'allstubs',
  },
  options = {},
) => {
  const url = urljoin(
    spaceUrl,
    resourceType,
    agencyId,
    resourceId,
    version,
    `?references=${references}`,
    `?detail=${detail}`,
  )
  const headers = { Accept: ACCEPT_HEADER.structure, 'Accept-Language': LANGS, ...options.headers }

  try {
    debug.info(`request ${url}`)
    return await got(url, { headers }).json()
  } catch (err) {
    err.url = url
    err.code = err.response?.statusCode || err.code
    throw err
  }
}

const parseNumber = when(complement(isNil), Number)

export const parseDataRange = response =>
  pipe(
    pathOr('', ['headers', 'content-range']),
    match(/values (\d+)-(\d+)\/(\d+)/),
    when(isNil, always([])),
    slice(1, 4),
    map(parseNumber),
    ([start, end, total]) => {
      if (any(anyPass([isNil, isNaN]))([start, end, total])) {
        return {}
      }
      const count = end - start + 1
      return { count, total }
    },
  )(response)

export const rlabel = ({ dataSpaceId = '', resourceType = '', agencyID, agencyId, id, version }) => {
  const res = []
  if (dataSpaceId) res.push(`${dataSpaceId}@`)
  if (resourceType) res.push(`${resourceType}/`)
  res.push(`${agencyID || agencyId}:${id}(${version})`)
  return res.join('')
}

export const rMake = ({ resourceType, agencyID, agencyId, id, version }) => ({
  resourceType,
  agencyID: agencyID || agencyId,
  id,
  version,
})

const parseURN = urn => urn.match(/^urn:sdmx:org\.sdmx\.infomodel\.(\w+)\.(\w+)=(.+):(.+)\((.+)\)(?:\.(.+))?/)

const SDMX_DATA_TYPES = []

const urnType = type => `sdmx.infomodel.${type.packageName}.${type.className}`

const hUrnTypes = SDMX_DATA_TYPES.reduce((acc, type) => ({ ...acc, [urnType(type)]: type }), {})

const getArtefactByUrnType = (packageName, className) => {
  const res = hUrnTypes[urnType({ packageName, className })]
  if (!res) throw new Error(`Unknown SDMX type [${packageName}, ${className}]`)
  return res
}

export const urn2FullRef = urn => {
  if (!urn) return
  const res = parseURN(urn)
  if (!res) throw new Error(`Malformed URN: ${urn}`)
  const [_, packageName, className, agencyID, id, version, itemId] = res // eslint-disable-line
  const artefact = getArtefactByUrnType(packageName, className)
  if (!artefact) throw new Error(`Unknow artefact for URN: ${urn}`)
  const resource = {
    resourceType: artefact.name,
    agencyID,
    id,
    version,
  }
  if (artefact.isItem && !itemId) throw new Error(`Inconsistent urn for an item: ${urn}`)
  if (itemId) resource.itemId = itemId
  return resource
}

export const urn2Ref = urn => {
  if (!urn) return
  const res = urn.match(/^urn:sdmx:org\.sdmx\.infomodel\.(\w*).(\w*)=(.*):(.*)\((.*)\)(?:\.(.*))?$/)
  if (!res) throw new Error(400, `Malformed urn: ${urn}`)
  const [, , , agencyID, id, version, itemId] = res
  return { agencyID, id, version, itemId }
}

export const fullID = curry(
  (resourceType, { agencyId, id, version, agencyID }) => `${resourceType}:${agencyId || agencyID}:${id}:${version}`,
)

export const ID = ({ agencyID, id, version, itemId } = {}) => {
  if (!agencyID) throw new Error('agencyID cannot be null')
  if (!id) throw new Error('id cannot be null')
  return itemId ? `${agencyID}:${id}:${version}:${itemId}` : `${agencyID}:${id}:${version}`
}
