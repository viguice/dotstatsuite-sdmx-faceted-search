import { urn2FullRef } from './utils'

const ComponentTypes = {
  dimension: 'dimension',
  timeDimension: 'timeDimension',
}

const getCompId = comp => {
  if (comp.id) return comp
  const ref = urn2FullRef(comp.conceptIdentity)
  comp.id = ref.itemId
  return comp
}

export const getTimeDimension = dataStructure => {
  const timeDimension =
    dataStructure?.dataStructureComponents?.dimensionList?.timeDimension ||
    dataStructure?.dataStructureComponents?.dimensionList?.timeDimensions?.[0]
  if (!timeDimension) return
  return { ...getCompId(timeDimension), type: ComponentTypes.timeDimension }
}

export const getDimensions = dataStructure => {
  let res = []
  const dimensions = dataStructure?.dataStructureComponents?.dimensionList?.dimensions
  if (dimensions?.length)
    res = [...res, ...dimensions.map(comp => ({ ...getCompId(comp), type: ComponentTypes.dimension }))]
  return res
}
