import Parser, { getRejectedDimensionsFromDataflow } from '../dataflow'
import DF from '../../tests/mocks/sdmx/dataflow_sample'

const defaultParserOptions = { datasourceId: 'TEST' }

describe('SDMX | NOT_INDEXED ', () => {
  it('should get non  indexed dimensions', () => {
    const dataflow = {
      annotations: [{ type: 'NOT_INDEXED', title: 'DIM1,DIM2=VALUE1+VALUE2' }],
    }
    const res = getRejectedDimensionsFromDataflow(dataflow)
    expect(res.DIM1).toEqual('ALL')
    expect(res.DIM2).toEqual(new Set(['VALUE1', 'VALUE2']))
  })

  it('should not indexed dimensions with NOT_INDEXED annotations', () => {
    const ref = { agencyID: 'A', id: 'DF', version: '1.0' }
    const structure = DF(ref)
    const parser = Parser(structure, defaultParserOptions)
    const res = parser.transpile(ref)
    // console.log(JSON.stringify(res, null, 4))
    expect(res.dimensions.map(x => x.id)).toEqual(['DIM2'])
    expect(res.dimensions.find(x => x.id === 'DIM2').children.map(v => [v.id, v.hasData])).toEqual([
      ['VALUE1', false],
      ['VALUE2', false],
      ['VALUE3', true],
      ['VALUE4', true],
    ])
  })
})
