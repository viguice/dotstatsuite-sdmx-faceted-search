import { isNil, isEmpty } from 'ramda'
import { parseISO, isValid } from 'date-fns'
import { ID, urn2Ref } from './utils.js'
import { getTimeDimension as extractTimeDimension, getDimensions as extractDimensions } from './datastructures.js'

export const getResourcesFromStructure = (structure, type) => structure?.data?.[type] ?? []

const getResourceFromStructure = (structure, fullRef = {}, type) => {
  const resources = getResourcesFromStructure(structure, fullRef.resourceType ?? type)
  // console.log(ID(fullRef), resources.map(ID))
  return resources.find(resource => ID(resource) === ID(fullRef))
}

export const makeId = (datasourceId, { id }) => `${datasourceId}:${id}`

const getCodeListValues = (notIndexedValues, constraints, codelists) => {
  const res = []
  for (const code of codelists.children) {
    const isIndexed = !isNotIndexed(code) && !notIndexedValues?.has(code.id)
    res.push({
      ...code,
      name: code.names,
      hasData: isIndexed && constraints?.has(code.id),
      children: getCodeListValues(notIndexedValues, constraints, code),
    })
  }
  return res
}

export const getRepresentation = (component, concepts) => {
  const concept = concepts[ID(urn2Ref(component.conceptIdentity))]
  const representation = component.localRepresentation ?? concept.coreRepresentation
  return representation
}

const getDimensions = (
  { rejectedDimensions = {}, dimensionValuesLimit = Infinity },
  { dataStructure, codelists = {}, concepts = {}, constraints = {} },
) => {
  if (!dataStructure) return []
  const res = []
  for (const dim of extractDimensions(dataStructure)) {
    const notIndexed = isNotIndexed(dim)
    if (notIndexed || !constraints[dim.id] || rejectedDimensions[dim.id] === 'ALL') continue
    const concept = concepts[ID(urn2Ref(dim.conceptIdentity))]
    if (!concept) throw new Error(`Cannot get concept "${dim.conceptIdentity}"`)
    const representation = getRepresentation(dim, concepts)
    let codeListValues = []
    if (representation?.enumeration) {
      const codelist = codelists[ID(urn2Ref(representation.enumeration))]
      codeListValues = getCodeListValues(rejectedDimensions[dim.id], constraints[dim.id], codelist)
      if (codeListValues.length > dimensionValuesLimit) continue
    }
    res.push({
      id: dim.id,
      name: concept.names,
      children: codeListValues,
      valuesCount: constraints[dim.id]?.size,
    })
  }
  return res
}

const getTimeDimensions = ({ dataStructure, concepts }) => {
  if (!dataStructure) return []
  const timeDimension = extractTimeDimension(dataStructure)
  if (!timeDimension) return []
  const concept = concepts[ID(urn2Ref(timeDimension.conceptIdentity))]
  return [{ id: timeDimension.id, name: concept.names }]
}

const isNotIndexed = obj => {
  const annos = getAnnotations(obj, { type: 'NOT_INDEXED' })
  return !!annos.length
}

export const getRejectedDimensionsFromDataflow = dataflow => {
  const annos = getAnnotations(dataflow, { type: 'NOT_INDEXED' })
  const res = {}
  for (const anno of annos) {
    const value = getAnnotationValue(anno)
    const dimTerms = value.split(',')
    for (const term of dimTerms) {
      const [dim, values] = term.split('=').map(x => x.trim())
      const id = dim.trim()
      if (values) {
        if (!res[id]) res[id] = new Set(values.split('+').map(x => x.trim()))
        else if (res[id] !== 'ALL') res[id] = new Set(...res[id], ...values.split('+').map(x => x.trim()))
      } else res[id] = 'ALL'
    }
  }
  return res
}

const makeDataflow = (
  { dimensionValuesLimit, datasourceId, externalUrl },
  structure,
  { codelists, concepts, categories, constraints },
  lastUpdated,
) => dataflow => {
  const rejectedDimensions = getRejectedDimensionsFromDataflow(dataflow)
  const hasNoContraints = isNil(constraints)
  const dataStructure = structure.data.dataStructures?.[0]
  const dimensions = getDimensions(
    { dimensionValuesLimit, rejectedDimensions },
    { dataStructure, codelists, concepts, categories, constraints, dataflow },
  )
  const timeDimensions = getTimeDimensions({ dataStructure, concepts, dataflow })

  const res = {
    fields: {
      id: makeId(datasourceId, dataflow),
      externalUrl,
      dataflowId: dataflow.id,
      version: dataflow.version,
      agencyId: dataflow.agencyID,
      datasourceId,
      name: dataflow.names,
      description: dataflow.descriptions,
      indexationDate: new Date(),
      lastUpdated,
    },
    dimensions,
    timeDimensions,
    categories,
    hasNoContraints,
    hasEmptyConstraints: isEmpty(constraints),
  }

  const orderAnnotations = getAnnotations(structure.data.dataflows[0], { type: 'SEARCH_WEIGHT' })
  if (orderAnnotations.length) {
    const order = orderAnnotations[0]
    if (order.title || order.title === 0) res.fields.gorder = order.title
    if (order.texts) res.fields.lorder = order.texts
  }

  return res
}

const isValidDateTimeRange = ({ validFrom, validTo } = {}, date = new Date()) => {
  if (validFrom && validTo) return new Date(validFrom) >= date && new Date(validTo) <= date
  if (validFrom) return new Date(validFrom) <= date
  if (validTo) return new Date(validTo) >= date
  return true
}

const getActualContentConstraints = structure => {
  const cc = getResourcesFromStructure(structure, 'contentConstraints')
  if (!cc?.length) return []
  let constraints
  const res = []
  for (const contentConstraint of cc) {
    if (contentConstraint.type !== 'Actual') continue
    if (!isValidDateTimeRange(contentConstraint, new Date())) continue
    res.push(contentConstraint)
    if (!constraints) constraints = {}
    for (const constraint of contentConstraint.cubeRegions || []) {
      if (!constraint.isIncluded) continue
      for (const { id, values } of constraint.keyValues || []) {
        constraints[id] = new Set([...(constraints[id] || []), ...(values || [])])
      }
    }
  }
  return [constraints, res]
}

const getConcepts = structure => {
  const res = {}
  for (const conceptScheme of structure.data.conceptSchemes || []) {
    for (const { id, names, coreRepresentation } of conceptScheme.concepts || []) {
      res[`${ID(conceptScheme)}:${id}`] = { names, coreRepresentation }
    }
  }
  return res
}

const getCodeLists = structure => {
  const res = {}
  for (const codelist of structure.data.codelists || []) {
    const roots = {}
    const nodes = {}
    for (const { annotations, id, names, parent } of codelist.codes || []) {
      const node = { id, annotations, names, children: [] }
      if (!nodes[id]) nodes[id] = node
      else nodes[id].annotations = annotations
      if (!parent) roots[id] = node
      else {
        if (nodes[parent]) nodes[parent].children.push(node)
        else {
          nodes[parent] = { id: parent, children: [node] }
        }
      }
    }
    res[ID(codelist)] = { names: codelist.names, children: Object.values(roots) }
  }
  return res
}

const getCategorySchemes = structure => {
  const getCats = (scheme, res = []) => {
    for (const cat of scheme) {
      if (cat.categories) {
        const rescat = { id: cat.id, name: cat.names, annotations: cat.annotations, children: [] }
        res.push(rescat)
        getCats(cat.categories, rescat.children)
      } else {
        res.push({ id: cat.id, name: cat.names, annotations: cat.annotations })
      }
    }
    return res
  }
  return getCats(structure.data?.categorySchemes || [])
}

const getAnnotations = (obj, { type, title } = {}) => {
  const res = []
  for (const anno of obj?.annotations || []) {
    if (type && anno.type !== type) continue
    if (title && anno.title !== title) continue
    res.push(anno)
  }
  return res
}

const getAnnotationValue = anno => {
  if (anno.title) return anno.title
  if (typeof anno.text === 'object') return Object.values(anno.text)[0]
  return anno.text
}

export const getLastUpdated = (cc, dataflow) => {
  if (isValidDateTimeRange(cc?.[0])) {
    let lastUpdated = cc?.[0]?.validFrom
    if (lastUpdated && isValid(parseISO(lastUpdated))) return parseISO(lastUpdated)
  }
  const annos = getAnnotations(dataflow, { type: 'LAST_UPDATE' })
  if (!annos.length) return new Date()
  const date = getAnnotationValue(annos[0])
  if (date && isValid(parseISO(date))) return parseISO(date)
  return new Date()
}

const transpile = options => (ref, structure) => {
  const dataflow = getResourceFromStructure(structure, ref, 'dataflows')
  if (!dataflow) throw new Error(`Cannot get dataflow ref "${JSON.stringify(ID(ref))}"!`)
  const [constraints, contentConstraints] = getActualContentConstraints(structure)
  const categories = getCategorySchemes(structure)
  const codelists = getCodeLists(structure)
  const concepts = getConcepts(structure)
  const lastUpdated = getLastUpdated(contentConstraints, dataflow)
  return makeDataflow(options, structure, { codelists, concepts, categories, constraints }, lastUpdated)(dataflow)
}

export default (input, options) => ({
  transpile: ref => transpile(options)(ref, input),
})
