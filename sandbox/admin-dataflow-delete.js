import axios from 'axios';
import { prop } from 'ramda';

// const url = 'http://sfs.staging.oecd.redpelicans.com';
// const key = 'c54GnjAfNaNF81R';
const url = 'http://localhost:7300';
const key = 'secret';

const run = () =>
  Promise.all([
    axios
      .delete(`${url}/admin/dataflow`, {
        data: { spaceId: 'qa:stable', agencyId: 'OECD', id: 'DF_TEST_MONTH', version: '1.0' },
        headers: { 'x-api-key': key, 'x-tenant': 'oecd' },
      })
      .then(prop('data')),
  ]);
run().then(console.log);
