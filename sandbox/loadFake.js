import faker from 'faker';
import axios from 'axios';
import R from 'ramda';
import fs from 'fs';

// const url = 'https://oecd.redpelicans.com/qa/sfs/admin/dataflows';
// const key = 'c54GnjAfNaNF81R';

const url = 'http://localhost:3007/admin/dataflows';
const key = 'secret';

const load = body => axios.post(url, body, { headers: { 'x-api-key': key } }).then(R.prop('data'));

const random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

const uniq = (faker, cache = {}) => {
  const getValue = () => {
    const value = faker().toUpperCase();
    return cache[value] ? getValue() : value;
  };
  const value = getValue();
  cache[value] = 1;
  return value;
};

const loadCategories = (schema, path = '', values = [], level = 0) => {
  if (!schema) return [];
  return schema.reduce((res, [cat, children]) => {
    const newPath = `${path}/${cat}`;
    const newValue = `${level}${newPath}/`;
    const newValues = [...values, newValue];
    return [...res, newValues, ...loadCategories(children, newPath, newValues, level + 1)];
  }, []);
};

const locale = lang => faker => `${faker()} (${lang.toUpperCase()})`;
const localeEN = locale('en');
const localeFR = locale('fr');
const CATEGORIES = [
  [
    'Business & Industrial',
    [
      ['Electrical & test Equipment'],
      ['Healthcare, Lab & Life Science'],
      [
        'Automation, Motor & Drives',
        [
          [
            'Control Systems & PLCs',
            [
              ['PLC Processors'],
              ['PLC Peripherical Modules'],
              ['HMI & Open Interface Panels'],
              ['PLC Cables'],
              ['PLC Chassis'],
            ],
          ],
          ['Drives & Motion Control', [['Motor Drives & Controls'], ['Pneumatic Cylinders']]],
          ['Electric Motors', [['Servo Motors'], ['Geamotors']]],
        ],
      ],
    ],
  ],
  ['Music', [['Records'], ['Cassettes'], ['CDs']]],
  [
    'Motors',
    [
      ['Automotive Tools & Supplies'],
      ['Parts & Accessories', [['Car & Truck Parts'], ['Manuals & Literature'], ['Motorcycle Parts']]],
    ],
  ],
  ['Coins & Paper Money', [['Paper Money: World'], ['Publication & Supplies'], ['Exonumia', [['World'], ['Medals']]]]],
];

const allCategories = loadCategories(CATEGORIES);
const pickDomainValues = domain => domain.filter((v, i) => random(0, 1));

const pickTreeValues = (values, res = {}) => {
  const children = pickDomainValues(values);
  return R.map(child => {
    if (R.is(String, child))
      return {
        id: child.toUpperCase(),
        name: { en: child },
      };
    const [value, children] = child;
    if (!children)
      return {
        id: value.toUpperCase(),
        name: { en: value },
      };
    return {
      id: value.toUpperCase(),
      name: { en: value },
      children: pickTreeValues(children, res),
    };
  }, children);
};

const COUNTRIES = R.uniq(R.times(() => `${faker.lorem.word()}-${faker.random.number()}`, 10));

const name = () => `${faker.hacker.adjective()} ${faker.commerce.productAdjective()} ${faker.commerce.productName()}`;

const genDataflows = () => {
  return {
    id: uniq(() => `${faker.random.word()}-${random(0, 100000)}`),
    name: { en: localeEN(name), fr: localeFR(name) },
    description: { en: localeEN(faker.lorem.sentence), fr: localeFR(faker.lorem.sentence) },
    dimensions: [
      {
        id: 'REF_AREA',
        name: { en: 'Reference area' },
        facet: 'list',
        children: R.map(x => ({ id: x.toUpperCase(), name: { en: x } }), pickDomainValues(COUNTRIES)),
      },
      {
        id: 'PRODUCT_TYPE',
        name: { en: 'Product type' },
        facet: 'tree',
        children: pickTreeValues(CATEGORIES),
      },
    ],
  };
};

R.reduce(
  acc => acc.then(() => load({ dataflows: R.times(genDataflows, 1) }).then(console.log)),
  Promise.resolve(),
  R.times(R.identity, 1),
).catch(console.log);

// fs.writeFile('/tmp/data2.js', JSON.stringify(res), err => {
//   if (err) return console.log(err);
//   console.log('/tmp/data2.js saved!');
// });
