# SDMX Faceted Search (aka sfs)

**Content**

- [Server](#server)
  - [Target](#target)
  - [Tenant model](#tenant-model)
  - [Stack](#stack)
  - [Principles](#principles)
  - [Changelog](#changelog)
  - [Install (source code)](#install-source-code)
  - [Server Config](#server-config)
    - [Deployment variables](#deployment-variables)
    - [Resource config](#resource-config)
  - [Solr](#solr)
    - [Install](#install)
    - [Setup](#setup)
      - [Script](#script)
      - [Manually](#manually)
  - [Run Server](#run-server)
    - [Context](#context)
    - [Development mode](#development-mode)
    - [Production mode](#production-mode)
  - [Health checks](#health-checks)
    - [ping](#ping)
    - [healthcheck](#healthcheck)
  - [Test](#test)
  - [Functional tests](#functional-tests)
- [API documentation](#api-documentation)
  - [Search API - HTTP Rest API to search for dataflows or get the content of the specific (home page) facets](#search-api---http-rest-api-to-search-for-dataflows-or-get-the-content-of-the-specific-home-page-facets)
  - [Admin API - HTTP Test API to insert, update or delete dataflows in the search index or to get logs](#admin-api---http-test-api-to-insert-update-or-delete-dataflows-in-the-search-index-or-to-get-logs)
    - [Dataflows](#dataflows)
    - [Dataflow](#dataflow)
    - [Logs](#logs)
    - [Others](#others)
  - [JS API - documentation of internal library for re-using/contributing developers](#js-api---documentation-of-internal-library-for-re-usingcontributing-developers)
- [Technical documentation for re-using/contributing developers](#technical-documentation-for-re-usingcontributing-developers)
  - [Dynamic config](#dynamic-config)
  - [Static config](#static-config)
    - [Schema definition](#schema-definition)
  - [Client queries syntax](#client-queries-syntax)
    - [Single term search](#single-term-search)
    - [Phrase term search](#phrase-term-search)
    - [Tagged search](#tagged-search)
    - [Negate](#negate)
  - [Highlighting results](#highlighting-results)
    - [Managing categories](#managing-categories)
  - [Use Case](#use-case)
  - [Notes on features](#notes-on-features)
    - [Stemming](#stemming)
    - [Search on facet values](#search-on-facet-values)
    - [Order annotations](#order-annotations)

## Server

### Target

`sfs` is a JSON http server that offer faceted search on SDMX dataflows. Search is enable on a configurable list of fields and faceted search on 2 types of facets: `list` and dataflow's `tree`. `sfs` use [solr's dynamic fields](https://cwiki.apache.org/confluence/display/solr/Dynamic+Fields) to index dataflows and an internal `schema` to request searches on abstract field names (`name` and not `name_en_s`). Localization is managed by the schema.

### Tenant model

`sfs` uses the `new tenant model`.
The top configuration level is the `organisation`, which allows assembling all `data spaces` (SDMX endpoints) and all Data Explorer and Data Lifecycle Manager experiences for a given organisational and brandable scope. The dataflows to be indexed within that `organisation` are found and validated through `data queries` listed in `data sources`. A `data source` defines SDMX CatagoryScheme(s) in one `data space`, in which the dataflows to be indexed have previously been categorised (using SDMX Categorisations). **Non-categorised dataflows are not indexed.**

An `organisation` (aka tenant) requires a SOLR collection (Note: There is no need to create a `core` anymore.):
`$ curl 'http://0.0.0.0:8983/solr/admin/collections?action=CREATE&name=test&numShards=1&collection.configName=_default'`
The `tenant` is used as the name of the collection, without this matching, the Data Explorer (DE) won't be able to request sfs properly (bad request since sfs won't be able to find the proper collection).

Here is a configuration example of an organisation:

```json
{
  "siscc": { // tenant
    "label": "SIS-CC",
    "spaces": {
      "qa-stable":{
        "url": "https://sxs-staging-siscc.dbmx.io/qa-stable/sdmx"
      },
      "demo-stable":{
        "label": "SIS-CC-demo-stable",
        "url": "https://sxs-staging-siscc.dbmx.io/demo-stable/sdmx"
      }
    },
    "datasources": {
      "qa-stable": {
        "dataSpaceId": "qa-stable",
        "indexed": true,
        "dataqueries": [
          { "version": "1.0", "categorySchemeId": "OECDCS1", "agencyId": "OECD" }
        ]
      },
      "demo-stable": {
        "dataSpaceId": "demo-stable",
        "indexed": true,
        "dataqueries": [
          { "version": "latest", "categorySchemeId": "OECDCS1", "agencyId": "OECD" }
        ]
      }
    },
  }
}
```

### Stack

`sfs` server is a [NodeJS](https://nodejs.org/en/) server. [ExpressJS](http://expressjs.com/) and [evtX](https://www.npmjs.com/package/evtx) libraries are used to manage http requests and define services around solr's search engine.

![sfs stack](./docs/sfs.png)

A `MongoDB` server instance is required to use `sfs`. It is used to create 1 database ('sfs') with 2 collections:
- 'jobs' in which the history of requests and their outcome is stored persistently, used for debugging and log reporting (e.g., in the DLM Logbook)
- 'configs' in which the dynamic schema/settings are stored, used while indexing and also to enrich results

**Example of MongoDB content created by `sfs`:**

job:  
```
{
    _id: 1652954168259,
    userEmail: null,
    submissionTime: ISODate("2022-05-19T09:56:08.261Z"),
    executionStart: ISODate("2022-05-19T09:56:08.261Z"),
    executionStatus: 'completed',
    tenant: 'oecd',
    action: 'deleteOne',
    dataSpaceIds: [ 'qa:stable' ],
    logs: [
      {
        executionStart: ISODate("2022-05-19T09:56:08.431Z"),
        message: 'dataflow ds:qa:stable:MILLED_RICE:TN1:1.0 deleted for organisation oecd',
        status: 'success',
        server: 'sfs',
        artefact: {
          resourceType: 'dataflow',
          agencyID: 'TN1',
          id: 'MILLED_RICE',
          version: '1.0'
        },
        dataSpaceId: 'qa:stable'
      }
    ],
    executionEnd: ISODate("2022-05-19T09:56:08.434Z")
  }
```

extract of configs:  
```
bHuYVRZAvzxEMZFrpUeULwRRiKcbEM: {
      type: 'FACET',
      name: 'bHuYVRZAvzxEMZFrpUeULwRRiKcbEM',
      _name: 'AGGREGATION_OPERATION',
      encoded: true,
      id: 'bHuYVRZAvzxEMZFrpUeULwRRiKcbEM',
      origin: 'concept',
      ext: 'ss',
      localized: true,
      search: false,
      op: 'AND',
      highlight: false
    },
```

### Principles

* Field's names are abstracted: client will not use field's names used by Solr, but abstracted ones. A field's name depends on it's Solr type and selected lang. ex: for `description` with lang='fr', Solr field's name will be `description_fr_t`. Server's setup will allow to define fields in such a way client (UI) will use generic names, in our ex, just `description`.
* Fields are typed, by their name construction (`description_<lang>_t`) and should mapped to solr's dynamc fields
* A static schema is defined in server's config
* A dynamic schema is derivated from dataflows loaded via /admin/dataflows, one can access to dynamic config with GET /admin/config?api-key=<secret>

### Changelog

see [Changelog](CHANGELOG.md)

### Install (source code)

- Install [nodejs](https://nodejs.org/en/)
- Clone repository `sdmx-facet-search`.

```
$ cd sdmx-facet-search
$ yarn
```

### Server Config

#### Deployment variables

Config files are located in .env

Config's data:

```
# host (String): sfs host ip or dns entry
SERVER_HOST
# port number
SERVER_PORT

# port number mongodb://mongo:27017
MONGODB_URL
# name of the database
MONGODB_DATABASE

# protocol Optional(String): defaults to http
SOLR_PROTOCOL

SOLR_HOST
SOLR_PORT

# URL to config server
CONFIG_URL

# apiKey (String): key to access /admin services
API_KEY

```

#### Resource config

At boot time, server get the resource `/configs/sfs.json` and merge it with `init/params.js`

Merge is done at 2 levels, root and `fields`

if params is:

```
export default {
  defaultLocale: 'en',
  fields: {
    ...
    field1: {
      type: ID_TYPE,
      out: true,
      search: false,
      name: 'field1',
    },
  }
}
```

and `sfs.json` is:

```
export default {
  prop1: 'val',
  fields: {
    ...
    field1: {
      weight: 4
    },
  }
}
```

Resulting config will be:

```json
{
  "defaultLocale": "en",
  "prop1": "val",
  "fields": {
    ...
    "field1": {
      "type": "ID_TYPE",
      "out": true,
      "search": false,
      "name": "field1",
      "weight": 4
    }
  }
}
```

### Solr

#### Install

It's out of the scope of this project to explain how to install / setup a solr server, [Apache Solr reference Guide](https://cwiki.apache.org/confluence/display/solr/Apache+Solr+Reference+Guide) may help!

Just a small recipe to get how we've done it for our development environment:

```
$ docker run -d --restart=always -p <IP>:8983:8983 --name solr solr8.7 -c
```

#### Setup

Solr use one collection per organisation (see new tenant model)

Collections must be created and setup before using SFS

Create and setup may be run via a SFS script or manually

##### Script

This script will:

* create, if collection does not exist, one collection per organisation
* delete all existing field types and dynamic fields starting with `*_sfs`
* create new field type per lang (see src/server/sdmx/utils#LANGS)
* create 2 new dynamic fields (single, multi valued) per lang
* update managed catalogs of stopwords and synonyms per lang from config service

Notes:
- in order to be compliant with Solr API, catalogs are JSON files and are located in config data (`configs/${tenant}/sfs/[stopwords|synonyms]/${language}.json`)
- synonyms are ASCII Folded by the script

Field types will be created following these rules:
- inherit from Solr `text_${language}` field type, except for KM (`text_general`)
- ASCII Folding on index and query
- Managed stopwords & synonyms on query
- deletion of unmanaged filters for stopwords & synonyms

Notes:
- some Solr field types (ie `text_fr`) doesn't have index and query differenciation, filters are applied to both automatically
- since synonyms are only applied during query, the initial value needs to be in the list of its synonyms (A -> B should be A -> [A,B])

Dynamic fields will be created following this template:
```
{
  name: '*_sfs_[text|text_list]_${language}',
  type: 'text_${language}',
  indexed: true,
  multiValued : true,
  stored : true,
}
```
To create organisation's collections and dynamic fields, run:
```
local_or_dev$ npx babel-node src/server/manageSchema
or
prod$ yarn dist:schema
```

##### Manually

All previous steps must be performed manually, it's up to the admin to setup dynamic fields - they must be compatible with the type displayed above - and create collections

eg, create a collection 'siscc':
```
$ curl 'http://0.0.0.0:8983/solr/admin/collections?action=CREATE&name=siscc&numShards=1&collection.configName=_default'
```
To create dynamic fields, see solR API documentation


### Run Server

#### Context

Running `solr` and mongodb servers must be reachable to run `sfs`.

```
$ docker run -d --restart=always -p 27017:27017 --name mongo mongo
$ docker run -d --restart=always -p 8983:8983 --name solr solr8.7 -c
```


#### Development mode

Fill .env file

then launch server:

```
$ yarn start:srv

  running config ...
  Solr is ready to serve U with member collections: [siscc]
  ready to index SDMX
  server started on http://0.0.0.0:7300
  Available members: [siscc]
  🚀 server started

```

check:

* running config
* available collections
* available members

#### Production mode

Fill .env file

```
$ yarn dist:srv
$ yarn dist:run
```

In both envs, params can be overwriten by shell variables

ex: we want to change SOLR's port:

```
$ SOLR_PORT=8888 yarn dist:run
```


### Health checks

2 services are available to supervise `sfs` server.

#### ping

```
$ curl http://localhost:3004/api/ping
{ ping: 'pong'}
```

#### healthcheck

```
$ curl http://localhost:3004/healthcheck
{
  ...
}
```

### Test

`solr` and `mongodb` servers defined in `testing` environment must be reachable to run tests.

Create a 'test' collection in solr:

```
$ curl 'http://0.0.0.0:8983/solr/admin/collections?action=CREATE&name=test&numShards=1&collection.configName=_default'

```

`sfs` is packaged with many tests at different layers of the stack:

To execute tests, run:
```
$ yarn test
```
To check coverage:
```
$ yarn coverage
```

### Functional tests

Tests are located under `tests` folders close to code to test.

Among tests, those under `src/server/tests` are functional ones, the goal is to test `sfs` as a black box.

`./fixtures/index.js` includes fixtures files that define tests run by `api.js`.

A fixture file is made of a schema definition, data to load and tests to run. See `data1.js` to get how to proceed and add new tests.

## API documentation

`sfs` offers 3 APIs:

* [**Search API** - HTTP Rest API to search for dataflows or get the content of the specific (home page) facets](#search-api---http-rest-api-to-search-for-dataflows-or-get-the-content-of-the-specific-home-page-facets)
* [**Admin API** - HTTP Test API to insert, update or delete dataflows in the search index or to get logs](#admin-api---http-test-api-to-insert-update-or-delete-dataflows-in-the-search-index-or-to-get-logs)
* [JS API - documentation of internal library for re-using/contributing developers](#js-api---documentation-of-internal-library-for-re-usingcontributing-developers)

### Search API - HTTP Rest API to search for dataflows or get the content of the specific (home page) facets

Search requests require the `organisation` id as parameter, otherwise a 400 Bad Request error will be returned.

Providing an `organisation` id can be done in 2 ways:
- as a header: `x-tenant=test`
- as a query param: `&tenant=test`

Search requests are always fully publically accessible.

`GET /api/config`: returns all available langs, the default one, facets definitions and values to dynamically build facets UIs.

`$ curl http://localhost:3004/api/config?tenant=siscc`
```json
{
  "langs": [ "fr", "en" ],
  "defaultLang": "en",
  "facets": {
    "category": {
      "type": "tree",
        "buckets": [
          {
            "val": "1|Music|Cassettes|",
            "count": 6
          },
          ... other values
        ]
    },
    "frequency": {
      "type": "list",
      "buckets": [
        {
          "val": "Monthly (M)",
          "count": 53
        }
      ]
    },
    "interest_rate_type": {
      "type": "list",
      "buckets": [
        {
          "val": "Bank interest rates (B)",
          "count": 52
        },
        ... other values
      ]
    }
  }
}
```

`POST /api/search`: Search API

**Body**

Name | Type | Default | Description
--- | :---: | :---: | ---
`lang` | String | 'en' | [lang](#lang) to be used
`search` | String | '' | search pattern on [searchFields](#searchFields)
`facets` | Object | null | facet's names / values (see below)
`start` | Integer | null | Result start index
`rows` | Integer | null | Result window size
`minCount` | Integer | null | specifies the minimum counts required for a facet field to be included in the response
`sort` | String | null |  arranges search results in either ascending (asc) or descending (desc) order (see https://lucene.apache.org/solr/guide/6_6/common-query-parameters.html#CommonQueryParameters-ThesortParameter)
`sortMode` | String | null |  ['name', 'date', 'score'] shortcuts equivalent to sort qp values ['name asc, indexationDate desc', 'indexationDate desc', 'score asc']
`fl` | Array | null | list of fields to filter, list can contain any kinf of fields (facet, prop, attr)


NB: `lang` can also be a http request's parameter
NB: default search '' will be converted by '*' server side
NB: `start' and `rows` allow pagination

**Response**

Name | Type | Description
--- | --- | ---
`dataflows` | Array | List of selected dataflows made of [outFields](#outFields)
`facets` | Object | facet search results (see below)
`start` | Integer | First dataflow index in result
`numFound` | Integer | Result cardinality

*Example*

`$ cat body.json`
```json
{
  "search": "*rate*",
  "facets": {
    "reference_area": ["Austria (AT)", "Belgium (BE)"]
  }
}
```
`$ curl http://localhost:3004/api/search  -H "Content-Type: application/json" -d @body.json`

*Response*
```json
{
    "dataflows": [
        {
            "id": "IRS",
            "agency": "ECB",
            "version": "1.0",
            "name": "Interest rate statistics"
        }
    ],
    "facets": {
        "count": 1,
        "reference_area": {
            "buckets": [
                {
                    "val": "Austria (AT)",
                    "count": 1
                }
            ]
        },
        "interest_rate_type": {
            "buckets": [
                {
                    "val": "Bank interest rates (B)",
                    "count": 1
                },
                {
                    "val": "Long-term interest rate for convergence purposes (L)",
                    "count": 1
                },
                {
                    "val": "Money market interest rates (M)",
                    "count": 1
                }
            ]
        },
        "frequency": {
            "buckets": [
                {
                    "val": "Daily (D)",
                    "count": 1
                },
                {
                    "val": "Monthly (M)",
                    "count": 1
                },
                {
                    "val": "Yearly (Y)",
                    "count": 1
                }
            ]
        }
    }
}
```


`GET /api/search`: Search API

Request used only to ease testing.

Accept only one param in query string `q` equivalent of `search` for post

```
$ curl "http://localhost:3004/api/search?q=Monthly"
```

### Admin API - HTTP Test API to insert, update or delete dataflows in the search index or to get logs

1) All admin requests require the `organisation` id as parameter, otherwise a 400 Bad Request error will be returned.

Providing an `organisation` id can be done in 2 ways:
- as a header: `x-tenant=test`
- as a query param: `&tenant=test`

2) The admin api is protected by an api key (see `src/server/params/[env] apiKey`)

Providing an api key can be done in 2 ways:
- as a header: `x-api-key=XXXXXX`
- as a query param: `&api-key=XXXXXX` (deprecated, for backward-compatibility only)

For greater security, all new usages should use the header parameter.

3) Admin requests accept a `userEmail` param with a user's email address (used by the Data Lifecycle Manager backend to log user actions)

Providing the `userEmail` parameter can be done in 2 ways:
- as a query param: `&userEmail=test@siscc.org`
- in a raw JSON-formatted request body:
  ```json
  {
    "userEmail": "test@siscc.org"
  }
  ```

#### Dataflows

`POST /admin/dataflows`: index all dataflows categorised in the defined CategoryScheme(s) for all `data sources` defined within the `organisation`

* accepts `mode` param with values:
** 'async' (default): requests loading and responds immediatly,
** 'sync': waits end of loading and returns report,
** 'stream': streams loading events

Providing the `mode` parameter can be done in 2 ways:
- as a query param: `&mode=async`
- in a raw JSON-formatted request body:
  ```json
  {
      "mode": "async"
  }
  ```

* returns
** for `mode=async`: the log id
** otherwise if an error occured: the error message
** otherwise: nothing

Internally, this method will execute:
* get tenant.datasources
* add dataflows to solr collection
* update existing dynamic sfs schema depending on dataflows added

Note: This method will insert or update the categorised dataflows. It will not delete any previously indexed dataflows that are not available anymore or are not categorised anymore. For that, use the `DELETE` method.

`DELETE /admin/dataflows`: drop all dataflows from Solr

Internally, this will execute:
* drop all dataflows from the solr collection

#### Dataflow

`POST /admin/dataflow {dataSpaceId, agencyId, id, version, userEmail }`: upsert dataflow { agencyId, id, version} from { tenant, dataSpaceId } for all existing datasources

This method will insert or update the given dataflow only if it was previously appropriately categorised.

`DELETE /admin/dataflow {dataSpaceId, agencyId, id, version, userEmail }`: delete dataflow { agencyId, id, version} from { tenant, dataSpaceId } for all existing datasources

This method will delete the given dataflow from the search index.

For all `dataflow` method calls, providing the { agencyId, id, version} and { dataSpaceId } parameters can be done:
- in a raw JSON-formatted request body:
  ```json
  {
    "dataSpaceId": "demo-design",
    "agencyId": "OECD.XYZ",
    "id": "DF_ABC",
    "version": "1.0",
  }
  ```

#### Logs

`GET or POST /admin/logs { dataSpaceId, action, submissionStart, submissionEnd, executionStatus, executionOutcome, artefact: object { resourceType, agencyID, id, version }}`

This method will return all logs according to the filtering request parameters.

The following specific query parameters are accepted:
* dataSpaceId: string
* action: [indexOne, indexAll, deleteAll, deleteOne]
* submissionStart: date
* submissionEnd: date
* executionStatus: [queued, inProgress, completed]
* executionOutcome: [success, warning, error]
* artefact: object { resourceType: [ dataflow, categoryscheme ], agencyID, id, version }

Providing the parameters can be done in 2 ways:
- as query parameters, with nested objects using the dot notation as defined in https://github.com/ljharb/qs
  Example: `&executionOutcome=error&artefact.resourceType=dataflow`
- in a raw JSON-formatted request body of a POST request:
  ```json
  {
    "dataSpaceId": "demo-design",
    "submissionStart": "2021-11-29T10:20:23.985Z",
    "submissionEnd": "2023-11-29T10:20:23.985Z",
    "executionStatus": "completed",
    "executionOutcome": "success",
    "artefact": {
        "resourceType": "dataflow",
        "agencyID": "OECD.XYZ",
        "id": "DF_ABC",
        "version": "1.0"
    }
  }
  ```

Example:

```
   const body = {
      "userEmail": "test@siscc.org",
      "dataSpaceId": "demo-stable",
      "submissionStart": "2017-01-01T00:00:00.000Z",
      "submissionEnd": "2023-01-01T00:00:00.000Z",
      "executionStatus": "completed",
      "executionOutcome": "error",
      "artefact": {
        "resourceType: "categoryscheme",
        "agencyID: "OECD",
        "id: "TEST",
        "version: "1.0"
      }
   }

   POST /admin/logs { body }
   [
        {
            "userEmail": "EMAIL",
            "submissionTime": "2022-05-04T10:06:24.189Z",
            "executionStart": "2022-05-04T10:06:24.189Z",
            "executionStatus": "completed",
            "tenant": "test",
            "action": "indexAll",
            "dataSourceIds": [
                "qa-stable",
                "demo-stable"
            ],
            "dataSpaceIds": [
                "qa-stable",
                "demo-stable"
            ],
            "logs": [
                {
                    "executionStart": "2022-05-04T10:06:24.243Z",
                    "message": "Cannot load http://qa-stable/categoryscheme/OECD/OECDCS1/1.0/?references=dataflow",
                    "logger": "loader.loadDataflowsFromQuery",
                    "status": "error",
                    "server": "sfs",
                    "exception": "Error: getaddrinfo EAI_AGAIN qa-stable\n    at GetAddrInfoReqWrap.onlookup [as oncomplete] (node:dns:71:26)",
                    "artefact": {
                        "resourceType": "categoryscheme",
                        "agencyID": "OECD",
                        "id": "OECDCS1",
                        "version": "1.0"
                    },
                    "dataSpaceId": "qa-stable"
                },
            ],
            "executionEnd": "2022-05-04T10:06:24.390Z",
            "outcome": "error",
            "id": 1651658784178
        }
    ]

```

`GET or POST /admin/logs { id }`

This method will return the specific log corresponding to the given request/log id.

Providing the `id` parameter can be done in 2 ways:
- as a GET query param: `&id=1651658784178`
- in a raw JSON-formatted request body of a POST request:
  ```json
  {
    "id": 1669717223969
  }
  ```

Example:

```
    GET /admin/logs&id=1651658784178
```

#### Others

`GET /admin/config?api-key=<secret>`: returns sfs dynamic config

`GET /admin/report?api-key=<secret>`: returns sfs in memory loadings statuses

`DELETE /admin/config`: delete dynamic config from SFS

### JS API - documentation of internal library for re-using/contributing developers

An internal library, a low level Javascript API, is used to help developers to manipulate solr's data through Solr's [JSON Request API](https://cwiki.apache.org/confluence/display/solr/JSON+Request+API), see `src/server/solr/index.js`.

```
  import initSolr from '../src/server/solr';
  import data from '../data/data.json'; // 1
  const config = { // 2
    solr: {
      host: 'localhost',
      port: 8983,
      verbose: true,
    }
  };

  initSolr({ config })
    .then(({ solr }) => solr.deleteAll() // 3
      .then(() =>  solr.add(data)) // 4
      .then(() => solr.select({ query: '*:*' })) // 5
```

This example is very close to a way to load data in a solr index.

1. JS structure to be indexed (see [Technical documentation for re-using/contributing developers](#technical-documentation-for-re-usingcontributing-developers) below)
2. Object similar to global sfs's config
3. `solr` is a Promise value returned by `initSolr`. It could be created also with a synchronous constructor.
3. `solr#deleteAll()` delete all data from the configured solr's collection, use it carefully!
4. `solr#add(data)` index data inside solr's collection. Use data without using any schema, so structure should be ready to be indexed as it.
5. `solr#select(data)` low level api to request searched on index. Do not use schema to transform query and response.

## Technical documentation for re-using/contributing developers

`sfs` can use a static config or a dynamic one derived from loaded dataflows.

### Dynamic config

`sfs` can derive its schema (see below) from dataflows loaded via POST /admin/dataflows.

We just have to supply fields definitions in static config.

```
{
  ...
  fields: {
    id: { // special case for ID, ID fiel is mandatory
      type: ID_TYPE,
      out: true,
    },
    names: { // field name, we must get the same in POST /admin/dataflows body
      type: ATTR_TYPE, // field with translations
      name: 'name', // targetedName
      ext: TEXTFIELD_EXT, // solr dynamic field extension
      search: true, // allow user requests to search on this field
      out: true, // will be returned by /api/search requests
      highlight: true, // will be highlighted
      localized: true, // is localized
      weight: 2, // field will be boosted by 2
    },
}
```

### Static config

If a developers wanted to load data into SOR without using the Admin API, a new schema would need to be defined.

The schema is configurable in `server/params` folder and will be used by API's services.

A schema is required:

* to help interface code to build the input structure
* to instruct Solr how to index/search data
* to analyze and answer to API requests
* to manage the localization of some fields

#### Schema definition

The schema is read at server's boot time in config files under `dataflows` key. Only the Search and the Admin APIs are aware of the schema, but not the JS API.

Name | Type  | Description
--- | --- | ---
`outFields` | Array | List of fields returned for each dataflow found
`searchFields` | Array | List of fields to search on
`schema` | Array of { `name`: String, `field`: (String or Function) } | Mapping Object `name` to index `field`
`facets` | Array of { `name`: String, `type`: ['list' or 'tree'], `op`: ['OR' or 'AND']} | Facet definition


`schema` is a mapping between indexed fields and abstract names used in `outFields`, `searchFields` and `facets.name` entries. `schema.field` can be String or a Function, latter will receive a `lang` parameter.

**Example**

To manage 3 languages ['en', 'cz', 'fr'] for the field `name`, an indexed dataflow looks like:

```
{
  id: 'IRS',
  name_en_s: 'Interest rate statistics',
  name_fr_s: 'Statistiques des taux d'intérêt',
  name_cz_s: 'Statistika úrokových sazeb',
}

```
The related schema would be like this:

```
dataflows: {
  outFields: ['id, 'name'],
  searchFields: ['name'],
  schema: [
    {
      name: 'name',
      field: lang => `name_${lang}_s`,
    }
  ]
}
```

On a search request:

```
$ cat body.json
{
  "lang": "fr",
  "search": "*taux*",
}
$ curl http://localhost:3004/api/search  -H "Content-Type: application/json" -d @body.json
```

We will get:

```
{
    "dataflows": [
        {
            "id": "IRS",
            "name": "Statistiques des taux d'intérêt"
        }
    ],
    "facets": {
        ...
    }
}
```

Indexed field names are never directly returned by `sfs`, but only corresponding names defined by schema depending on `lang` parameter.

Fields in this sample use [solr's dynamic fields](https://cwiki.apache.org/confluence/display/solr/Dynamic+Fields) definition, but it's up to you thanks to the schema to implement your own type's strategy.

We have a similar use case for facets. Let's say we would like to have a faceted search on field `reference_area` without localization. This time `reference_area` is a multi string field type named `reference_area_ss`.

Schema will be defined as:

```
dataflows: {
  outFields: ['id, 'name'],
  searchFields: ['name'],
  schema: [
    {
      name: 'name',
      field: lang => `name_${lang}_s`,
    },
    {
      name: 'reference_area',
      field: 'reference_area_ss',
    },
  ],
  facets: [
    {
      name: 'reference_area',
      type: 'list'
      op: 'OR',
    }
  ]
}
```

NB:

* `op` define the logical operator to use in case of a multiselection for one facet.
* `type` help to split facets in 2 worlds: 'list' and 'tree'. Latter implies to deal with hierarchical facets search, so we need to identify facets of that type.

### Client queries syntax

A search query can be expressed on one or many single terms, on phrase for all searchable fields.

Instead of searching on all searchable fields, it's possible to precise the field to search in.

All individual searches can be negated.


#### Single term search

```
{
    search: "one two three"
}
```

Server will execute for all searchable fields (here ['field1', 'field2']):

```
   +(<field1>:*one* <field2>:*one*) +(<field1>:*two* <field2>:*two*) +(<field1>:*three* <field2>:*three*)
```

#### Phrase term search

To match on an exact string, we must use single or double quotes in search field:

```
{
    search: "\"Et blanditiis quasi reprehenderit"\"
}
```


#### Tagged search

Previous searches were trying to match on all searchable fields, We can precise a field using a tag like:

```
{
    search: "name:\"Et blanditiis quasi reprehenderit"\"
    // or
    search: "description:blandi name:one"
}
```

#### Negate

We can negate all search terms by adding '-' in front of the term:

```
{
    search: "-interest ID564 -name:\"Et blanditiis"\"
}
```

### Highlighting results

see Dynamic config > highlight


#### Managing categories

Searching on `caterogy` fields implies to deal with hierarchical facets search, a concept which can mean different things to different people depending on data.
`sfs` tries to be more generic as possible and implements a basic approach that works well for most use cases by encoding the facet fields at index time in a specific manner.

Let's have an example:

```
Dataflow#1: C1 > C11
Dataflow#2: C1 > C11 > C111
Dataflow#3: C1 > C12, C2

```

In this example, we have dataflows associated with multiple categories, like Dataflow#3.

We must perform some index time processing on this flattened data in order to create the tokens needed for a hierarchical approach. When we index the data we create specially formatted fields that encode the depth information for each node that appears as part of the path, and include the hierarchy separated by a common separator (“depth/first level term/second level term/etc”). We also add additional terms for every ancestors in the original data.

Indexed fields

```
Dataflow#1: { cat: ['0|C1/', '1|C1|C11|'] }
Dataflow#2: { cat: ['0|C1/', '1|C1|C11|', '2|C1|C11|C111|'] }
Dataflow#3: { cat: ['0|C1/', '1|C1|C12|', '0|C2|'] }
```

if we define `cat` in our schema as a `tree` type facet like this:

```
dataflows: {
  outFields: ['id'],
  schema: [
    {
      name: 'cat',
      field: cat_ss,
    },
  ],
  facets: [
    {
      name: 'cat',
      type: 'tree'
    }
  ]
}
```

We can query like this:

```
$ cat body.json
{
  "facets": {
    "cat": ["0|C2|"]
  }
}
$ curl http://localhost:3004/api/search  -H "Content-Type: application/json" -d @body.json
```
And get:

```
{
    "dataflows": [
        {
            "id": "3",
        }
    ],
    "facets": {
        "count": 1,
        "cat": {
            "buckets": [
                {
                    "val": "0|C1|",
                    "count": 3
                },
                {
                    "val": "1|C1|C11|",
                    "count": 2
                },
                {
                    "val": "0|C2|",
                    "count": 1
                },
                {
                    "val": "1|C1|C12|",
                    "count": 1
                },
                {
                    "val": "2|C1|C11|C111|",
                    "count": 1
                }
            ]
        },
      }
  }
```

### Use case

* Start with an empty mongo DB and an empty solr collection
* Load dataflows with POST /admin/dataflows (mind api key)
`sfs` will insert dataflows within solr and build a quite new dynamic config
* Check new schema with GET /admin/config
Your SPA is ready to use `sfs`
* First it needs to call /api/config, to get locales, i18n, searchFields and facets
* Then search, search and search with POST /api/search


### Notes on features


#### Stemming

Before 'stemming', all searches were executed with surrounded wilcards:
* search name:'toto' was translated within solr as `name_en_txt:*toto*`
* sfs was using dynamic fields `*_en_txt`

With stemming, wilcards are removed and searches are executed on new dynamic fields `*_sfs_text_${lang}` linked to bundled solr types setup for stemming `text_${lang}`.

Solr dynamic fields must be created before using sfs (see Setup section).
SFS available languages are hardcoded, to add a new one, extend `src/server/sdmx/utils.js#LANGS'
SFS Admin must ensure that text_* fields exist for all languages defined by LANGS constant.


#### Search on facet values

To allow search on facets values factet's fields were duplicated:
* one with _ss extension to compute facet values
* one with _txt extension to allow search

#### Order annotations

When a dataflow has an order annotation:
```
{ type: 'SEARCH_WEIGHT', title: { 1 }, text: { en: 2, fr: 3 } }
```
SFS will create a document with 3 fields:
```
  gorder_i: 1
  lorder_en_i: 2
  lorder_fr_i: 3

```
if a search({ locale: 'fr' } match this document, document's score will equal solr score * lorder_fr_i

the generale score fct equals document.score * boost, where:

```js
{!boost b="def(def(${lorder_<locale>_i}, gorder_i), 1)"}
```
That's all folks.
