## [Unreleased]

- remove i18n: could help DE to migrate to 2.0


## 2.0.0 (2019-12-12)

v2.0.0 is a major release that add, review and enhance features.

Requires node's version >= 13.3

### Datasource Definition Evolved


Datasource structure has changed to:

```
{
  "SIS-CC-stable": {
    "url": "http://nsi-stable-siscc.redpelicans.com/rest",
    "indexed": true,
    "queries": [
      {
        "version": "1.0",
        "categorySchemeId": "OECDCS1",
        "agencyId": "OECD"
      }
    ],
  },
}

```

NB: 

* 'SIS-CC-stable' is the datasourceId
* `version` must be made of Number[.Number[.Number]]

### Document ID


`document ID` is the identifier of indexed dataflow documents in SOLR. It's now build with `${datasourceId}:${dataflow.id}`


### Facet's origin

Add a new property on facet's definitions: `origin` with domain values : ['datasource', 'category', 'concept'].

It may help `DE` to manipulate a facet:

* 'datasource' is not hierarchical
* 'category' are hierarchical by only made of leaves
* 'concept' are fully hierarchical


### Index facet's names instead of ID

Some facet's names use white spaces. We cannot use spaces within field name in SFS and SOLR, so spaces are reaplaced by '_'. It's under client (DE) responsability to remove underscores.

ex:

```
{
  id: 'TSEST',
  name: 'Ajustment Type',
  names: {
    en: 'Ajustment Type',
  },
  description: 'The statistical method applied to time series data.',
}

```

will create a facet:

```
      Ajustment_Type: {
        type: 'FACET',
        name: 'Ajustment_Type',
        id: 'AJUSTMENT_TYPE',
        subType: 'tree',
        search: true,
        ext: 'ss',
        localized: true,
        op: 'AND',
        origin: 'concept',
      },

```

### Highlight facet values

All facet's values are now searchable, and used for highlighting.

So a search like 'Region:Weighted', will return:

```
highlighting:
  { 'DS1:DF_CPI':
    { 
      Region: [ '<em>0|Weighted average of eight capital cities#50#</em>' ] 
    } 
  } 
```

It's under `DE` responsability to render correctly highlighted snippet.


### Allow updating or deleting individual dataflows in the search index

* DELETE /admin/dataflow { datasourceId, dataflowId } allow to delete an indexed dataflow
* PATH /admin/dataflow { datasourceId, dataflowId, agencyId, version } allow to update an indexed dataflow

2 scripts were added in `./sandbox` to show how to use new rest services. 

launch:

```
  $ npx babel-node  sandbox/admin-dataflow-patch.js
```

* admin-dataflow-delete.js
* admin-dataflow-patch.js

### Exclude by params some categorySchemes to create a facet

SFS manages a new config property to exclude a catagory scheme of all indexations:

```
// cat src/server/params/default.js
{ 
  defaultLocale,
  excludedCategorySchemeFacets: ['CAT1, 'CAT2'],
  fields,
}
```

A `dataflow` will not be indexed if `includes(dataflow.id, config.excludedCategorySchemeFacets)`


### Allow using externally defined dataflows

Below definition will request `https://registry.sdmx.org/ws/rest/dataflow/ECB.DISS/DF_EXAMPLE/1.0` to get `DF_EXAMPLE` definition:

```
{
	"data": {
		"dataflows": [
			{
				"id": "DF_EXAMPLE",
				"agencyID": "ECB.DISS",
				"version": "1.0",
				"name": "Dataflow name",
				"names": {
					"en": "Dataflow name"
				},
				"isExternalReference": true,
				"links": [
					{
						"href": "https://registry.sdmx.org/ws/rest/dataflow/ECB.DISS/DF_EXAMPLE/1.0",
						"rel": "dataflow",
						"urn": "urn:sdmx:org.sdmx.infomodel.datastructure.dataflow=ECB.DISS:DF_EXAMPLE(1.0)"
					}
				]
			}
		]
	}
}
```

###  Rename datasource_id

Rename datasource_id as datasourceId

### Outfields

* add `id` field as an outfield

#### :rocket: New Feature

- [#37](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search/issues/37) Allow updating or deleting individual dataflows in the search index

#### :boom: Breaking Change

- [#41](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search/issues/41) Redefine Document ID and Datasource definition
- [#47](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search/issues/47) Index facet's names instead of ID and remove i18n
- [#48](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search/issues/48) All facets are hierarchical

#### :bug: Bug Fix

- [#26](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search/issues/26) Report is sometimes unreadable and buggy

#### :nail_care: Enhancement

- [#38](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search/issues/38) Allow using externally defined dataflows
- [#42](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search/issues/42) Create facets for all category scheme of a dataflow
- [#43](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search/issues/43) Exclude by params some categorySchemes to create a facet
- [#44](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search/issues/44) Highlight facet values

#### Committers

- Eric Basley ⚡️ ([@eric-basley](https://gitlab.com/eric-basley))


