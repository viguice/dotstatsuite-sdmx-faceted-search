FROM node:16-alpine

ARG GIT_HASH

RUN mkdir -p /sfs
WORKDIR /sfs

RUN echo $GIT_HASH

COPY node_modules /sfs/node_modules
COPY dist /sfs/dist
COPY scripts /sfs/scripts
COPY data /sfs/data
COPY .eslint* package.json yarn.lock /sfs/

ENV GIT_HASH=$GIT_HASH

EXPOSE 80
CMD yarn dist:run
